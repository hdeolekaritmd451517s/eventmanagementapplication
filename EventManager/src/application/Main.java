package application;


import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.util.Duration;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

/**
 * This class is the main file of the project. It calls the MainController class
 * which acts as a superclass for administrator's and organiser's controller
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0 build 1.0
 * @since December 2, 2016
 *
 */
public class Main extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        try {
        	// launching the application through MainController.
			MainController mainController = new MainController();
			mainController.setPrimaryStage(primaryStage);
			mainController.launchApplication();

			//new Scene(root,400,410);
		} catch (Exception e) {
			// tray notification if system could not be launched.
			TrayNotification tray = new TrayNotification();
			tray.setNotificationType(NotificationType.ERROR);
			tray.setTitle("System Failure");
			tray.setMessage("Could not  launch the application\n"+e.getMessage());
			tray.setAnimationType(AnimationType.SLIDE);
			tray.showAndDismiss(Duration.millis(4000));
			e.printStackTrace();
		}
    }
}