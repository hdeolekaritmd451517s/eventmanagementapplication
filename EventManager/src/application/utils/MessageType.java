package application.utils;

/**
 * This interface contains type of message to be sent.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 *
 */
public interface MessageType {
	// user registration
	public static final int USER_SIGNUP_MESSAGE= 1;
	// user event registration.
	public static final int USER_EVENT_REGISTRATION = 2;
}
