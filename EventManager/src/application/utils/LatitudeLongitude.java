package application.utils;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;

/**
 *
 * This class will get the latitude and longitude values based on  address.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class LatitudeLongitude
{

	/**
	 * This method returns the latitude and longitudes of the event location.
	 * @param address Event address to be located on the gmap.
	 * @return latitude and longitude coordinates.
	 * @throws Exception if address cannot be processed.
	 */
	public String[] getLatLongPositions(String address) throws Exception
	{
		try {
			int responseCode = 0;
			String api = "http://maps.googleapis.com/maps/api/geocode/xml?address=" +
			URLEncoder.encode(address, "UTF-8") + "&sensor=true";
			URL url = new URL(api);
			System.out.println("Tracking location using geocode:"+url);
			HttpURLConnection httpConnection = (HttpURLConnection)url.openConnection();
			httpConnection.connect();
			responseCode = httpConnection.getResponseCode();
			if(responseCode == 200)
			{
				// location was found, now plot the location on new document
				DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();;
				Document document = builder.parse(httpConnection.getInputStream());
				XPathFactory eventXPathFactory = XPathFactory.newInstance();
				XPath newLocationXPath = eventXPathFactory.newXPath();
				XPathExpression xPathExpression = newLocationXPath.compile("/GeocodeResponse/status");
				String status = (String)xPathExpression.evaluate(document, XPathConstants.STRING);
				// if successfully plotted, get the lat and longs.
				if(status.equals("OK"))
				{
					xPathExpression = newLocationXPath.compile("//geometry/location/lat");
					String latitude = (String)xPathExpression.evaluate(document, XPathConstants.STRING);
					xPathExpression = newLocationXPath.compile("//geometry/location/lng");
					String longitude = (String)xPathExpression.evaluate(document, XPathConstants.STRING);
					return new String[] {latitude, longitude};
				}
				else
				{
					// else throw the exception
					throw new Exception("Error from the API - response status: "+status);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void main(String[] args) {
		try  {
			String latLongs[] = new LatitudeLongitude().getLatLongPositions("b/5 New TriveniGawadWadiVirarEastMaharashtraIndia");
			System.out.println("Tracked lat: "+latLongs[0]+" long:"+latLongs[1]);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}