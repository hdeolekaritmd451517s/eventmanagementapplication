package application.utils;

/**
 * This class contains all the validation methods for most of the input fields.
 * @author Harshal
 */
public class InputValidator {
	/*
	 * 			^			#start of the line
 			[_A-Za-z0-9-\\+]+	#  must start with string in the bracket [ ], must contains one or more (+)
  			(				#   start of group #1
    			\\.[_A-Za-z0-9-]+	#     follow by a dot "." and string in the bracket [ ], must contains one or more (+)
  				)*			#   end of group #1, this group is optional (*)
    			@			#     must contains a "@" symbol
    		[A-Za-z0-9-]+      #       follow by string in the bracket [ ], must contains one or more (+)
      		(			#         start of group #2 - first level TLD checking
       			\\.[A-Za-z0-9]+  #           follow by a dot "." and string in the bracket [ ], must contains one or more (+)
      		)*		#         end of group #2, this group is optional (*)
      (			#         start of group #3 - second level TLD checking
       \\.[A-Za-z]{2,}  #           follow by a dot "." and string in the bracket [ ], with minimum length of 2
      )			#         end of group #3
			$			#end of the line
	 *
	 * */
	public static final String EMAIL_PATTERN =
			"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
					+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	public static final String NAME_PATTERN= "[a-zA-Z]+";

	public static final String EVENT_NAME_PATTERN = "[_!@,\\sa-zA-Z0-9\\-\\.]+";

	/*
	 * US ZIP code (U.S. postal code) allow both the five-digit and nine-digit (called ZIP+4) formats.
	 * A valid postal code should match 12345 and 12345-6789, but not 1234, 123456, 123456789, or 1234-56789.
	 * */
	public static final String POSTAL_CODE_PATTERN ="^[0-9]{5}(?:-[0-9]{4})?$";
	/*(?=.*[a-z])      : This matches the presence of at least one lowercase letter.
	(?=.*d)          : This matches the presence of at least one digit i.e. 0-9.
	(?=.*[@#$%]) : This matches the presence of at least one special character.
	((?=.*[A-Z])     : This matches the presence of at least one capital letter.
	{6,16}             : This limits the length of password from minimum 6 letters to maximum 16 letters.*/
	public static final String PASSWORD_PATTERN= "((?=.*[a-z])(?=.*\\d)(?=.*[A-Z])(?=.*[@#$%!]).{6,16})";

	/*Digits only. Also, one or more*/
	public static final String NUMBERS_ONLY_PATTERN = "\\d+";

	/*
	 * It matches 1 or more digits, followed by optional: comma or full stop, followed by 1 or 2 digits.
	 * */
	public static final String TICKET_PRICE_PATTERN = "[0-9]{1,4}([,.][0-9]{1,2})?";

	public static final String test = "^[a-z0-9_-]{3,15}";

	public static boolean validatePhoneNumber(String phoneNo) {
		//validate phone numbers of format "1234567890"
		if (phoneNo.matches("\\d{10}")) return true;
		//validating phone number with -, . or spaces
		else if(phoneNo.matches("\\d{3}[-\\.\\s]\\d{3}[-\\.\\s]\\d{4}")) return true;
		//validating phone number with extension length from 3 to 5
		else if(phoneNo.matches("\\d{3}-\\d{3}-\\d{4}\\s(x|(ext))\\d{3,5}")) return true;
		//validating phone number where area code is in braces ()
		else if(phoneNo.matches("\\(\\d{3}\\)-\\d{3}-\\d{4}")) return true;
		//return false if nothing matches the input
		else return false;
	}

	/**
	 * Validates names
	 * @param name
	 * @return true if name is valid
	 */
	public static boolean validateName(String name){
		return name.matches(NAME_PATTERN);
	}

	/**
	 * Validates email
	 * @param email
	 * @return true if email is valid, else, false
	 */
	public static boolean validateEmail(String email){
		return email.matches(EMAIL_PATTERN);
	}

	public static boolean validatePassword(String password){
		return password.matches(PASSWORD_PATTERN);
	}
	/**
	 *Validates the confirm password
	 * @param password
	 * @param confirmPassword
	 * @return 1 if confirm password doesn't match the password policy, 2 if it doesn't match the password
	 */
	public static int validateConfirmPassword(String password, String confirmPassword){
		int result = 0;
		System.out.println("password:"+password+" confpwd:"+confirmPassword);
		if(!(confirmPassword.matches(PASSWORD_PATTERN))){
			result =  1;
			return result;
		}
		if(!(password.equals(confirmPassword)))
			result =  2;
		return result;
	}

	/**
	 * Validates event name
	 * @param eventName
	 * @return true if name is valid
	 */
	public static boolean validateEventName(String eventName) {
		return eventName.matches(EVENT_NAME_PATTERN);
	}

	/**
	 * Validates postal code
	 * @param postalCode
	 * @return if postal code is valid
	 */
	public static boolean validatePostalCode(String postalCode) {
		return postalCode.matches(POSTAL_CODE_PATTERN);
	}

	/**
	 * Validates Integer fields
	 * @param numbers
	 * @return true if number is valid
	 */
	public static boolean validateNumber(String numbers) {
		return numbers.matches(NUMBERS_ONLY_PATTERN);
	}

	public static boolean validatePrice(String text) {
		return text.matches(TICKET_PRICE_PATTERN);
	}

	public static boolean finalTest(String text) {
		return text.matches(test);
	}

	public static void main(String[] args) {
		System.out.println(finalTest("tom-thumbtomthumb"));
	}
}
