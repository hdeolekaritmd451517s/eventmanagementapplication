package application.utils;


import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import com.iit.eventmanagment.model.BookEvent;
import com.iit.eventmanagment.model.Users;
import com.iit.eventmanagment.model.db.Configuration;

/**
 * This class is used to send appropriate emails to the users.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class SendHTMLEmail {

    // Recipient's email ID needs to be mentioned.
    String to;
    MimeMessage message;
    private static Properties properties = System.getProperties();

    public SendHTMLEmail() throws AddressException, MessagingException {

        // Setup mail server
        properties.setProperty("mail.transport.protocol", "smtp");
        System.out.println("mail.smtp.host:"+Configuration.get(Configuration.MAIL_SMTP_HOST));
        properties.put("mail.smtp.host", Configuration.get(Configuration.MAIL_SMTP_HOST));
        System.out.println("mail.smtp.socketFactory.port:"+Configuration.get(Configuration.MAIL_SMTP_SOCKETFACTORY_PORT));
        properties.put("mail.smtp.socketFactory.port", Configuration.get(Configuration.MAIL_SMTP_SOCKETFACTORY_PORT));
        System.out.println("mail.smtp.socketFactory.port:"+Configuration.get(Configuration.MAIL_SMTP_SOCKETFACTORY_CLASS));
        properties.put("mail.smtp.socketFactory.port", Configuration.get(Configuration.MAIL_SMTP_SOCKETFACTORY_CLASS));
        System.out.println("mail.smtp.auth:"+Configuration.get(Configuration.MAIL_SMTP_AUTH));
        properties.put("mail.smtp.auth", Configuration.get(Configuration.MAIL_SMTP_AUTH));
        System.out.println("mail.smtp.port:"+Configuration.get(Configuration.MAIL_SMTP_PORT));
        properties.put("mail.smtp.port", Configuration.get(Configuration.MAIL_SMTP_PORT));
        System.out.println("mail.smtp.starttls.enable"+Configuration.get(Configuration.MAIL_SMTP_STARTTLS_FLAG));
        properties.put("mail.smtp.starttls.enable", Configuration.get(Configuration.MAIL_SMTP_STARTTLS_FLAG));

        // creates a valid session by authenticating user.
        Session session = Session.getDefaultInstance(properties, new javax.mail.Authenticator() {
  			protected PasswordAuthentication getPasswordAuthentication() {
  				System.out.println("username:"+Configuration.get(Configuration.MAIL_SMTP_USER)+" password:"+Configuration.get(Configuration.MAIL_SMTP_USER_PASS));
  				return new PasswordAuthentication(Configuration.get(Configuration.MAIL_SMTP_USER), Configuration.get(Configuration.MAIL_SMTP_USER_PASS));
  			}
  		});
        message = new MimeMessage(session);
        message.setFrom(new InternetAddress(Configuration.get(Configuration.MAIL_SMTP_USER)));
	}

	/**
	 * This method sends an email.
	 * @param to email recipient.
	 * @param emailMessage email body/message to be sent.
	 * @param messageType type of email, e.g. user registration, event regisr
	 * @throws MessagingException
	 */
	public void sendMessage(String to, String emailMessage, int messageType) throws MessagingException {
		try {
			switch (messageType) {
			case 1:
				message.setSubject("Event Manager: Registration Successful!");
				break;
			case 2:
				message.setSubject("Event Manager: Registration for Event!!");
				break;
			default:
				break;
			}

			// Set  the header.
			message.setRecipient(Message.RecipientType.TO, new InternetAddress(to));

			// sets content type
			message.setContent(emailMessage, "text/html");

			// Send message
			Transport.send(message);
			System.out.println("Sent message successfully....");
		} catch (MessagingException mex) {
			mex.printStackTrace();
		}
	}

	/**
	 * This method returns the email html message body.
	 * @param obj Object used for creating email.
	 * @param messageType
	 * @return
	 */
	public String getMessage(Object obj, int messageType){
		String message = "";
		switch(messageType){
		case 1:
			Users newUser = (Users) obj;
			message = "<h3>Dear,"+newUser.getFirstName()+"</h3><br>"
					+ "<h4>Congratualations! you have been successfully registered with Event Management System.<br>"
					+ "Username:"+newUser.getEmail()+"<br>Password:"+newUser.getPassword()+"</h4>";


			message="<BODY BGCOLOR='FFFFFF'>"
					+ "<HR><H3>Login Successful!!!</H3>"
					+ "<h3>Dear,"+newUser.getFirstName()+"</h3><br>"
					+ "<H3>Welcome on-board</H4>"
					+ "<p>Your login credentials are validated</p>"
					+ "<p><em><B>Enjoy our Event Manager application using your credentials:</B></em></p><br>"
					+ "username : "+newUser.getEmail()+"</br>"
					+ "<p>password : "+newUser.getPassword()+"</p>"
					+ "Send an email to <a href='mailto:d@gmail.com'>d@gmail.com</a>. requesting to become resource <P>"
					+ " <B><I>*Event manager helps you to surf the events, book the tickets for the events and choose the events based on location</I></B></P>"
					+ "<HR><br>Regards,</br><br>Event Managing team</br><br>3300 S Federal St,Chicago, IL - 60616 | (510)-640-6361 </br></HR></BODY>";

			break;
		case 2:
			BookEvent newBooking = (BookEvent) obj;
			message="<BODY BGCOLOR=''><HR>"+
			"<H1><font color =Black>Booking Successful!!!</font></H1>"+
			"<H2><font color =Black>You have your seats confirmed at the Event</font></H2>"+
			"<p><font color =Black>Please carry your Identity proof</font></p>"+
			"<br><font color =Black><em><B>Booking summary</B></em></font></br>"+
			"<br><font color =Black> Registered Person:test@example.com</font></br>"+
			"<font color =Black>number of seats : 1</font>"+

			"<br><font color =Black>Send an email to <a href=mailto:d@gmail.com>"+
			"d@gmail.com</a>. for any queries</font></br>"+
			"<P><font color =Black> <B><I>*Event manager helps you to surf the events, book the tickets for the events and choose the events based on location</I></B></font></P>"+
			"<HR>"+
			"<br><font color =Black>Regards,</font></br>"+
			"<br><font color =Black>Event Managing team</font></br>"+
			"<br><font color =Black>3300 S Federal St,Chicago, IL - 60616 | (510)-640-6361 </font></br>"+
			"</HR>"+
			"</BODY>";
			break;
		}
		return message;
	}
}