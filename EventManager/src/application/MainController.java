package application;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import com.iit.eventmanagment.controller.LocateEventController;
import com.iit.eventmanagment.controller.LoginController;
import com.iit.eventmanagment.model.Event;
import com.iit.eventmanagment.model.Users;
import com.iit.eventmanagment.service.EventService;
import com.iit.eventmanagment.service.impl.EventServiceImpl;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.RequiredFieldValidator;
import com.lynden.gmapsfx.GoogleMapView;
import application.utils.InputValidator;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

/**
 * This class acts as a superclass for Admin and Organiser Controller.
 * It handles actions such as creating, editing and deleting event.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0 build 1.0
 * @since December 2, 2016
 */
public class MainController implements Initializable{
	@FXML
	public JFXButton addEventButton;
	@FXML
	public JFXTextField eventNameTextField;
	@FXML
	public JFXTextField eventDescriptionTextField;
	@FXML
	public JFXTextField eventAddressTextField;
	@FXML
	public JFXTextField eventCityTextField;
	@FXML
	public JFXTextField eventStateTextField;
	@FXML
	public JFXTextField eventCountryTextField;
	@FXML
	public JFXTextField eventPostalCodeTextField;
	@FXML
	public JFXTextField eventEmailTextField;
	@FXML
	public JFXTextField eventPhoneTextField;
	@FXML
	public JFXDatePicker eventStartDatePicker;
	@FXML
	public JFXDatePicker eventStartTimePicker;
	@FXML
	public JFXDatePicker eventEndDatePicker;
	@FXML
	public JFXDatePicker eventEndTimePicker;
	@FXML
	public JFXTextField eventImageTextField;
	@FXML
	public JFXTextField eventNoOfSeatsTextField;
	@FXML
	public JFXButton createEventButton;
	@FXML
	public JFXButton updateEventButton;
	@FXML
	public JFXButton deleteEventButton;
	@FXML
	public JFXButton viewEventButton;
	@FXML
	public JFXButton editEventButton;
	@FXML
	public DatePicker fromDatePicker;
	@FXML
	public DatePicker toDatePicker;
	@FXML
	public JFXTextField searchEventTextField;
	@FXML
	public GridPane eventGridPane;
	@FXML
	public TableView<Event> eventTableView;
	@FXML
	public TableColumn<Event, Integer> eventIDTableColumn;
	@FXML
	public TableColumn<Event, String> eventNameTableColumn;
	@FXML
	public TableColumn<Event, String> eventLocationTableColumn;
	@FXML
	public TableColumn<Event, LocalDateTime> eventStartDateTimeTableColumn;
	@FXML
	public TableColumn<Event, LocalDateTime> eventEndDateTimeTableColumn;
	@FXML
	public TableColumn<Event, String> eventEmailTableColumn;
	@FXML
	public TableColumn<Event, String> eventPhoneTableColumn;
	@FXML
	public TableColumn<Event, Integer> eventSeatAvailTableColumn;
	@FXML
	public TableColumn<Event, String> eventStatusTableColumn;
	public LoginController loginController;
	public Users loggedInUser;
	// observed list for event table.
	public ObservableList<Event> eventDataList = FXCollections.observableArrayList();
	public Stage currentStage;
	public Event currentEvent;
	// setting validators for the required fields.
	RequiredFieldValidator eventNameValidator = new RequiredFieldValidator();
	RequiredFieldValidator eventDescriptionValidator = new RequiredFieldValidator();
	RequiredFieldValidator eventAddressValidator = new RequiredFieldValidator();
	RequiredFieldValidator eventCityValidator = new RequiredFieldValidator();
	RequiredFieldValidator eventStateValidator = new RequiredFieldValidator();
	RequiredFieldValidator eventCountryValidator = new RequiredFieldValidator();
	RequiredFieldValidator eventPostalValidator = new RequiredFieldValidator();
	RequiredFieldValidator eventEmailValidator = new RequiredFieldValidator();
	RequiredFieldValidator eventPhonesValidator = new RequiredFieldValidator();
	RequiredFieldValidator eventNoOfSeatsValidator = new RequiredFieldValidator();
	RequiredFieldValidator eventImageValidator = new RequiredFieldValidator();


	public MainController() {
	}

	/**
	 * Sets the stage which is loaded and logged in user.
	 * @param primaryStage Stage to be shown
	 * @param loggedInUser Logged in user
	 */
	public MainController(Stage primaryStage, Users loggedInUser) {
		this.currentStage = primaryStage;
		this.loggedInUser  = loggedInUser;
	}


	/**
	 * Sets the loaded stage.
	 * @return
	 */
	public Stage getPrimaryStage() {
		return currentStage;
	}
	public Users getLoggedInUser() {
		return loggedInUser;
	}

	public void setLoggedInUser(Users loggedInUser) {
		this.loggedInUser = loggedInUser;
	}

	public void setPrimaryStage(Stage primaryStage) {
		this.currentStage = primaryStage;
	}

	public LoginController getLoginController() {
		return loginController;
	}

	public void setLoginController(LoginController loginController) {
		this.loginController = loginController;
	}

	public void launchApplication() throws IOException{
		displayLoginWindow();
	}

	/**
	 * Displays login window, called when login is successful.
	 * @throws IOException
	 */
	public void displayLoginWindow() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getClassLoader().getResource("\\com\\iit\\eventmanagment\\view\\login.fxml"));
		loader.load();
		Parent root = loader.getRoot();
		Scene scene = new Scene(root);
		scene.getStylesheets().add(getClass().getClassLoader().getResource("\\application\\application.css").toExternalForm());
		this.currentStage.setScene(scene);
		LoginController loginController = loader.<LoginController>getController();
		loginController.setPrimaryStage(this.currentStage );
		this.currentStage.show();
	}


	/**
	 * Action invoked on add event button click. It enables the create button and disables the update and delete button.
	 * @param event
	 */
	@FXML
	void onAddEventClick(ActionEvent event) {
		eventGridPane.setDisable(false);
		createEventButton.setDisable(false);
		updateEventButton.setDisable(true);
		deleteEventButton.setDisable(true);
		// clears all the textfield for new inputs
		clearEvent();
	}

	/**
	 * This method creates new event. Shows appropriate alerts if validation fails.
	 * @param event
	 */
	@FXML
	void onCreateEventClick(ActionEvent event) {
		try {
			Alert alert1 = new Alert(AlertType.INFORMATION);
			alert1.setTitle("Event Manager");
			alert1.setHeaderText("Event Manager");
			//validates if user has input all the required fields.
			if(!(eventNameTextField.validate()&&eventDescriptionTextField.validate()&&eventAddressTextField.validate()&&eventCityTextField.validate()
					&&eventStateTextField.validate()&&eventCountryTextField.validate()&&eventPostalCodeTextField.validate()&&eventEmailTextField.validate()
					&&eventPhoneTextField.validate()&&eventNoOfSeatsTextField.validate())){
				alert1.setHeaderText("Event Manager");
				alert1.setContentText("Please fill the required fields.");
				alert1.showAndWait();
				// validates if date and times are selected
			} else if((eventStartDatePicker.getValue()==null) ||("null".equals(eventStartTimePicker.getTime()))||
					(eventEndDatePicker.getValue()==null)||("null".equals(eventEndTimePicker.getTime()))){
				alert1.setHeaderText("Event Manager");
				alert1.setContentText("Please fill the proper date and time fields.");
				alert1.showAndWait();
			}else{
				boolean isValid = true;
				// validates all the input fields using regex.
				if(!InputValidator.validateEventName(eventNameTextField.getText())){
					isValid = false;
					alert1.setContentText("Please enter valid event name.");
					alert1.showAndWait();
				} else if(!InputValidator.validateName(eventCityTextField.getText())){
					isValid = false;
					alert1.setContentText("Please enter valid city name.");
					alert1.showAndWait();
				}else if(!InputValidator.validateName(eventStateTextField.getText())){
					isValid = false;
					alert1.setContentText("Please enter valid state name.");
					alert1.showAndWait();
				}else if(!InputValidator.validateName(eventCountryTextField.getText())){
					isValid = false;
					alert1.setContentText("Please enter valid country name.");
					alert1.showAndWait();
				} else if(!InputValidator.validatePostalCode(eventPostalCodeTextField.getText())){
					isValid = false;
					alert1.setContentText("Please enter valid postal code.");
					alert1.showAndWait();
				}else if(!InputValidator.validateEmail(eventEmailTextField.getText())){
					isValid = false;
					alert1.setContentText("Please enter valid email.");
					alert1.showAndWait();
				}else if(!InputValidator.validatePhoneNumber(eventPhoneTextField.getText())){
					isValid = false;
					alert1.setContentText("Please enter valid phonenumber.");
					alert1.showAndWait();
				}else if(!InputValidator.validateNumber(eventNoOfSeatsTextField.getText())){
					isValid = false;
					alert1.setContentText("Please enter valid no of seats.");
					alert1.showAndWait();
				}
				// if input is valid, creates the event.
				if(isValid){
					EventService eventService = new EventServiceImpl();
					Event newEvent = getNewEvent();
					newEvent = eventService.addEvent(newEvent, loggedInUser);
					if(newEvent != null){
						eventDataList.add(newEvent);
						clearEvent();
					} else{
						// else shows error.
						Alert alert = new Alert(AlertType.ERROR);
						alert.setTitle("Event");
						alert.setHeaderText("Create Event");
						alert.setContentText("Message: Event could not be saved!");
						alert.showAndWait();
					}
					eventGridPane.setDisable(true);
					createEventButton.setDisable(true);
					updateEventButton.setDisable(true);
					deleteEventButton.setDisable(true);
				}
			}
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Event");
			alert.setHeaderText("Oopps, error saving event!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}

	/**
	 * Creates a new object of Event class using inputs from user
	 * @return Event class objects.
	 */
	private Event getNewEvent() {
		Event newEditedEvent = new Event(eventNameTextField.getText(), eventDescriptionTextField.getText(), eventAddressTextField.getText(),
				eventCityTextField.getText(), eventStateTextField.getText(), eventCountryTextField.getText(), eventPostalCodeTextField.getText(),
				eventEmailTextField.getText(), eventPhoneTextField.getText(), eventStartDatePicker.getValue(), eventStartTimePicker.getTime(),
				eventEndDatePicker.getValue(), eventEndTimePicker.getTime(), Integer.parseInt(eventNoOfSeatsTextField.getText()), eventImageTextField.getText().getBytes());
		return newEditedEvent;
	}

	/**
	 * Deletes the selected event  from table.
	 * @param event
	 */
	@FXML
	void onDeleteEventClick(ActionEvent event) {
		try {
			EventService eventService = new EventServiceImpl();
			Event eventTobeDeleted = getEditedEvent();
			boolean result = eventService.deleteEvent(eventTobeDeleted);
			if(result){
				int selectedIndex = eventTableView.getSelectionModel().getSelectedIndex();
				// removes the selected event from the tableview.
				if (selectedIndex >= 0) {
					//eventTableView.getItems().remove(selectedIndex);
					eventDataList.remove(selectedIndex);
				} else {
					// Nothing selected.
					Alert alert = new Alert(AlertType.WARNING);
					alert.setTitle("Delete Location");
					alert.setHeaderText("Oopps, error deleting location!");
					alert.setContentText("Please select the location.");
					alert.showAndWait();
				}
			}
			// after deleting event, disables the input grid, delete and update button.
			updateEventButton.setDisable(true);
			deleteEventButton.setDisable(true);
			eventGridPane.setDisable(true);
			clearEvent();
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Update Location");
			alert.setHeaderText("Oopps, error updating location!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}

	/**
	 * Returns the newly edited events.
	 * @return
	 */
	private Event getEditedEvent() {
		Event newEditedEvent = new Event(currentEvent.getId(), eventNameTextField.getText(), eventDescriptionTextField.getText(), eventAddressTextField.getText(),
				eventCityTextField.getText(), eventStateTextField.getText(), eventCountryTextField.getText(), eventPostalCodeTextField.getText(),
				eventEmailTextField.getText(), eventPhoneTextField.getText(), eventStartDatePicker.getValue(), eventStartTimePicker.getTime(),
				eventEndDatePicker.getValue(), eventEndTimePicker.getTime(), Integer.parseInt(eventNoOfSeatsTextField.getText()), eventImageTextField.getText().getBytes());
		return newEditedEvent;
	}


	/**
	 * Called on edit button click. Disables creating of event, enables updating and deleting of event.
	 * @param event -
	 */
	@FXML
	void onEditEventClick(ActionEvent event) {
		currentEvent = eventTableView.getSelectionModel().getSelectedItem();
		if(editEvent(currentEvent)){
			createEventButton.setDisable(true);
			updateEventButton.setDisable(false);
			deleteEventButton.setDisable(false);
		}
	}

	/**
	 * Sets the event selected by the user to the input grid.
	 * @param currentEvent event selected by user from the tableview
	 * @return
	 */
	private boolean editEvent(Event currentEvent) {
		boolean result = true;
		if(currentEvent!=null){
			eventGridPane.setDisable(false);
			eventNameTextField.setText(currentEvent.getName());
			eventDescriptionTextField.setText(currentEvent.getDescription());
			eventAddressTextField.setText(currentEvent.getEventAddress());
			eventCityTextField.setText(currentEvent.getEventCity());
			eventStateTextField.setText(currentEvent.getEventState());
			eventCountryTextField.setText(currentEvent.getEventCountry());
			eventPostalCodeTextField.setText(currentEvent.getEventPostalCode());
			eventEmailTextField.setText(currentEvent.getEventEmail());
			eventPhoneTextField.setText(currentEvent.getEventPhone());
			eventStartDatePicker.setValue(currentEvent.getEventStartDate());
			eventStartTimePicker.setTime(currentEvent.getEventStartTime());
			eventEndDatePicker.setValue(currentEvent.getEventEndDate());
			eventEndTimePicker.setTime(currentEvent.getEventEndTime());
			eventNoOfSeatsTextField.setText(String.valueOf(currentEvent.getNoOfSeats()));
			eventImageTextField.setText(currentEvent.getEventImage().toString());
			result = true;
		} else{
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("EVENT");
			alert.setHeaderText("EDIT EVENT");
			alert.setContentText("Message: Please select the field first!");
			alert.showAndWait();
			result = false;
		}
		return result;
	}


	/**
	 * Updates the edited event in the database and the table.
	 * @param event
	 */
	@FXML
	void onUpdateEventClick(ActionEvent event) {
		Event newEditedEvent;
		try {
			EventService eventService = new EventServiceImpl();
			newEditedEvent = getEditedEvent();
			boolean result = eventService.updateEvent(newEditedEvent);
			if(result){
				updateTableRecord(newEditedEvent);
			} else {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Location");
				alert.setHeaderText("Update Location");
				alert.setContentText("Message: No records were updated");
				alert.showAndWait();
			}
			updateEventButton.setDisable(true);
			deleteEventButton.setDisable(true);
			eventGridPane.setDisable(true);
			clearEvent();
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Update Location");
			alert.setHeaderText("Oopps, error updating location!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}

	/**
	 * Registers required field validators and change listeners with the textfields.
	 */
	private void registerListeners() {
		eventNameTextField.getValidators().add(eventNameValidator);
		eventDescriptionTextField.getValidators().add(eventDescriptionValidator);
		eventAddressTextField.getValidators().add(eventAddressValidator);
		eventCityTextField.getValidators().add(eventCityValidator);
		eventStateTextField.getValidators().add(eventStateValidator);
		eventCountryTextField.getValidators().add(eventCountryValidator);
		eventPostalCodeTextField.getValidators().add(eventPostalValidator);
		eventEmailTextField.getValidators().add(eventEmailValidator);
		eventPhoneTextField.getValidators().add(eventPhonesValidator);
		eventEmailTextField.getValidators().add(eventEmailValidator);
		eventNoOfSeatsTextField.getValidators().add(eventNoOfSeatsValidator);

		eventNoOfSeatsTextField.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(!newValue){
					eventNoOfSeatsValidator.setMessage("field is required");
					eventNoOfSeatsTextField.validate();
				}
			}
		});


		eventNameTextField.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(!newValue){
					eventNameValidator.setMessage("field is required");
					eventNameTextField.validate();
				}
			}
		});

		eventDescriptionTextField.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(!newValue){
					eventDescriptionValidator.setMessage("field is required");
					eventDescriptionTextField.validate();
				}
			}
		});

		eventAddressTextField.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(!newValue){
					eventAddressValidator.setMessage("field is required");
					eventAddressTextField.validate();
				}
			}
		});

		eventCityTextField.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(!newValue){
					eventCityValidator.setMessage("field is required");
					eventCityTextField.validate();
				}
			}
		});

		eventStateTextField.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(!newValue){
					eventStateValidator.setMessage("field is required");
					eventStateTextField.validate();
				}
			}
		});

		eventCountryTextField.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(!newValue){
					eventCountryValidator.setMessage("field is required");
					eventCountryTextField.validate();
				}
			}
		});

		eventPostalCodeTextField.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(!newValue){
					eventPostalValidator.setMessage("field is required");
					eventPostalCodeTextField.validate();
				}
			}
		});

		eventEmailTextField.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(!newValue){
					eventEmailValidator.setMessage("field is required");
					eventEmailTextField.validate();
				}
			}
		});

		eventPhoneTextField.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(!newValue){
					eventPhonesValidator.setMessage("field is required");
					eventPhoneTextField.validate();
				}
			}
		});
	}

	/**
	 * Updates the current edited event in the table
	 * @param newEditedEvent
	 */
	private void updateTableRecord(Event newEditedEvent) {
		currentEvent.setId(newEditedEvent.getId());
		currentEvent.setName(newEditedEvent.getName());
		currentEvent.setDescription(newEditedEvent.getDescription());
		currentEvent.setEventAddress(newEditedEvent.getEventAddress());
		currentEvent.setEventLocation(newEditedEvent.getEventAddress()+","+"\n"+newEditedEvent.getEventCity()+"-"+newEditedEvent.getEventPostalCode()
		+",\n"+newEditedEvent.getEventState()+","+newEditedEvent.getEventCountry());
		currentEvent.setEventCity(newEditedEvent.getEventCity());
		currentEvent.setEventState(newEditedEvent.getEventState());
		currentEvent.setEventCountry(newEditedEvent.getEventCountry());
		currentEvent.setEventPostalCode(newEditedEvent.getEventPostalCode());
		currentEvent.setEventEmail(newEditedEvent.getEventEmail());
		currentEvent.setEventPhone(newEditedEvent.getEventPhone());
		currentEvent.setEventStartDate(newEditedEvent.getEventStartDate());
		currentEvent.setEventStartTime(newEditedEvent.getEventStartTime());
		currentEvent.setEventEndDate(newEditedEvent.getEventEndDate());
		currentEvent.setEventEndTime(newEditedEvent.getEventEndTime());
		currentEvent.setEventImage(newEditedEvent.getEventImage());
		currentEvent.setNoOfSeats(newEditedEvent.getNoOfSeats());
	}

	/**
	 * This method locates the event on google map.
	 * @param e
	 */
	@FXML
	void onViewEventClick(ActionEvent e) {
		try {
			Event eventToTrack = eventTableView.getSelectionModel().getSelectedItem();
			if(eventToTrack != null){
				Stage locateEventStage = new Stage();
				// controller to manage gmap.
				LocateEventController locateEventController = new LocateEventController(locateEventStage, loggedInUser, eventToTrack);
				// initializes the gmap view.
				GoogleMapView  mapView = new GoogleMapView();
				mapView.addMapInializedListener(locateEventController);
				locateEventController.setMapView(mapView);
				Scene scene = new Scene(mapView);
				locateEventStage.setTitle("JavaFX and Google Maps");
				locateEventStage.setScene(scene);
				locateEventStage.show();
			}else{
				// notifies if user has not selected any event from the table
				TrayNotification tray = new TrayNotification();
				tray.setNotificationType(NotificationType.INFORMATION);
				tray.setTitle("View Event");
				tray.setMessage("Please select the event first!");
				tray.setAnimationType(AnimationType.SLIDE);
				tray.showAndDismiss(Duration.millis(2000));
			}
		} catch (Exception ex) {
			TrayNotification tray = new TrayNotification();
			tray.setNotificationType(NotificationType.ERROR);
			tray.setTitle("View Event");
			tray.setMessage("Oopps, error loading event");
			tray.setAnimationType(AnimationType.SLIDE);
			tray.showAndDismiss(Duration.millis(2000));
			ex.printStackTrace();
		}
	}

	/**
	 * This method loads the tables with all the events based on the user logged in.
	 */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		try {
			System.out.println("------->MainController initializing...........");
			eventIDTableColumn.setCellValueFactory(cellData -> cellData.getValue().eventIDProperty().asObject());
			eventNameTableColumn.setCellValueFactory(cellData -> cellData.getValue().eventNameProperty());
			eventLocationTableColumn.setCellValueFactory(cellData -> cellData.getValue().getEventLocationProperty());
			eventStartDateTimeTableColumn.setCellValueFactory(cellData -> cellData.getValue().getEventStartDateTimeProperty());
			eventEndDateTimeTableColumn.setCellValueFactory(cellData -> cellData.getValue().getEventEndDateTimeProperty());
			eventEmailTableColumn.setCellValueFactory(cellData -> cellData.getValue().eventEmailProperty());
			eventPhoneTableColumn.setCellValueFactory(cellData -> cellData.getValue().eventPhoneProperty());
			eventSeatAvailTableColumn.setCellValueFactory(cellData -> cellData.getValue().noOfSeatsIntegerProperty().asObject());
			eventStatusTableColumn.setCellValueFactory(cellData -> cellData.getValue().eventStatusProperty());
			EventService eventService = new EventServiceImpl();
			List<Event> eventList = null;
			switch (loggedInUser.getRole().getId()) {
			case 1:
			case 3:
				// displays  all the events for admin, (role id 1) and students(role id 3)
				eventList = eventService.viewAllEvent();
				break;
			case 2:
				// displays all the events created by the orgnizer (role id 2)
				eventList = eventService.viewAllEventByUserId(loggedInUser);
				break;
			default:
				System.out.println("Role not found for this user: Could not load events");
				break;
			}

			if(eventList != null){
				eventDataList.addAll(eventList);
				eventTableView.setItems(eventDataList);
			}else{
				System.out.println("no events found");
			}
			registerListeners();
			// adding observable list to the filterlist
			FilteredList<Event> eventFilteredData = new FilteredList<>(eventDataList, e -> true);
			 // setting changed  listener on key released event.
			searchEventTextField.setOnKeyReleased(e -> {
				searchEventTextField.textProperty().addListener((observableValue, oldValue, newValue) -> {
					eventFilteredData.setPredicate((Predicate<? super Event>) event -> {
						// for event character entered, match data in following columns:
						//(id, name, city, state, postalcode, phone, email)
						String lowerCaseFilter = newValue.toLowerCase();
						if(newValue == null || newValue.isEmpty())
							return true;
						else if(String.valueOf(event.getId()).contains(lowerCaseFilter))
							return true;
						else if(event.getName().toLowerCase().contains(lowerCaseFilter))
							return true;
						else if(event.getEventCity().toLowerCase().contains(lowerCaseFilter))
							return true;
						else if(event.getEventState().toLowerCase().contains(lowerCaseFilter))
							return true;
						else if(event.getEventPostalCode().toLowerCase().contains(lowerCaseFilter))
							return true;
						else if(event.getEventPhone().toLowerCase().contains(lowerCaseFilter))
							return true;
						else if(event.getEventEmail().toLowerCase().contains(lowerCaseFilter))
							return true;
						return false;
					});
				});
			});
			// sets the filtered data to the column.
			eventTableView.setItems(eventFilteredData);
		} catch (Exception e) {
			e.printStackTrace();
			TrayNotification tray = new TrayNotification();
			tray.setNotificationType(NotificationType.ERROR);
			tray.setTitle("System Failure");
			tray.setMessage("Oopps, error loading resources. Please contact system admin!");
			tray.setAnimationType(AnimationType.SLIDE);
			tray.showAndDismiss(Duration.millis(2000));
		}
	}

	/**
	 * Clears the input fields.
	 */
	protected void clearEvent() {
		if(eventNameTextField.getText() != null)
			eventNameTextField.clear();
		if(eventDescriptionTextField.getText() != null)
			eventDescriptionTextField.clear();
		if(eventAddressTextField.getText() != null)
			eventAddressTextField.clear();
		if(eventCityTextField.getText() != null)
			eventCityTextField.clear();
		if(eventStateTextField.getText() != null)
			eventStateTextField.clear();
		if(eventCountryTextField.getText() != null)
			eventCountryTextField.clear();
		if(eventPostalCodeTextField.getText() != null)
			eventPostalCodeTextField.clear();
		if(eventEmailTextField.getText() != null)
			eventEmailTextField.clear();
		if(eventPhoneTextField.getText() != null)
			eventPhoneTextField.clear();
		if(eventStartDatePicker.getValue() != null){
			eventStartDatePicker.getEditor().clear();
			eventStartDatePicker.setValue(null);
		}
		if(eventStartTimePicker.getTime() != null){
			eventStartTimePicker.getEditor().clear();
			eventStartTimePicker.setTime(null);
		}
		if(eventEndDatePicker.getValue() != null){
			eventEndDatePicker.getEditor().clear();
			eventEndDatePicker.setValue(null);
		}
		if(eventEndTimePicker.getTime() != null){
			eventEndTimePicker.getEditor().clear();
			eventEndTimePicker.setTime(null);
		}
		if(eventNoOfSeatsTextField.getText() != null)
			eventNoOfSeatsTextField.clear();
		if(eventImageTextField.getText() != null)
			eventImageTextField.clear();
	}
}
