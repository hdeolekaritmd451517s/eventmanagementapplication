package com.iit.eventmanagment.controller;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.List;
import java.util.ResourceBundle;
import com.iit.eventmanagment.model.BookEvent;
import com.iit.eventmanagment.model.Users;
import com.iit.eventmanagment.service.BookingService;
import com.iit.eventmanagment.service.impl.BookingServiceImpl;
import com.jfoenix.controls.JFXButton;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.util.Duration;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

/**
 * This class handles user booking view. It shows the id, name of the event booked by used,
 * booking date and time(not the event). It allows user to cancel the booking.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class MyBookingController implements Initializable {
    @FXML
    private TableView<BookEvent> bookingTableView;

    @FXML
    private TableColumn<BookEvent, Integer> bookingIDTableColumn;

    @FXML
    private TableColumn<BookEvent, String> eventNameTableColumn;

    @FXML
    private TableColumn<BookEvent, LocalDateTime> bookingDateTimeTableColumn;

    @FXML
    private JFXButton cancelBookingButton;

    private Users loggedInUser;
    // observable event booking list for booking table.
    private ObservableList<BookEvent> bookingDataList = FXCollections.observableArrayList();

    public MyBookingController(Users loggedInUser) {
		this.loggedInUser = loggedInUser;
	}


	/**
	 * Cancels selected event booking.
	 * @param event
	 */
	@FXML
    void onCancelBookingClick(ActionEvent event) {
    	try {
    		BookEvent selectBooking = bookingTableView.getSelectionModel().getSelectedItem();
    		int selectedItemIndex = bookingTableView.getSelectionModel().getSelectedIndex();
        	//If booking is selected, cancel it by deleting it from the database(not ideal, could have just marked as cancelled.)
    		if(selectBooking!=null){
        		BookingService bookingService = new BookingServiceImpl();
            	boolean result = bookingService.deleteBookingById(selectBooking);
            	// if successfully deleted, remove it from the tableview
    			if(result){
    				if (selectedItemIndex >= 0) {
    					bookingTableView.getItems().remove(selectedItemIndex);
    				} else {
    					// Nothing selected.
    					Alert alert = new Alert(AlertType.WARNING);
    					alert.setTitle("Event Manager");
    					alert.setHeaderText("Cancel Events");
    					alert.setContentText("Something went wrong, contact event organiser!");
    					alert.showAndWait();
    				}
    			}
        	}else{
        		 TrayNotification tray = new TrayNotification();
                 tray.setNotificationType(NotificationType.SUCCESS);
                 tray.setTitle("Event Manager");
                 tray.setMessage("Dear "+loggedInUser.getFirstName()+",\nPlease select the booking to be cancelled");
                 tray.setAnimationType(AnimationType.SLIDE);
                 tray.showAndDismiss(Duration.millis(1500));
        	}
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Event Manager");
			alert.setHeaderText("Cancel Booking");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
    }
	/**
	 * Initializes the event booking tableview with all the bookings.
	 */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		try {
			bookingIDTableColumn.setCellValueFactory(cellData -> cellData.getValue().idProperty().asObject());
			eventNameTableColumn.setCellValueFactory(cellData -> cellData.getValue().eventNameProperty());
			bookingDateTimeTableColumn.setCellValueFactory(cellData -> cellData.getValue().bookingDateTimeProperty());
			BookingService bookingService = new BookingServiceImpl();
			List<BookEvent> bookedEventList = bookingService.viewAllBookingById(loggedInUser);
			if(bookedEventList != null){
				bookingDataList.addAll(bookedEventList);
				bookingTableView.setItems(bookingDataList);
			}
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Event Manager");
			alert.setHeaderText("Error occured while loading your bookings");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}
}
