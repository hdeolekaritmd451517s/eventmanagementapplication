package com.iit.eventmanagment.controller;

import java.io.IOException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Predicate;

import com.iit.eventmanagment.model.BookEvent;
import com.iit.eventmanagment.model.Event;
import com.iit.eventmanagment.model.Users;
import com.iit.eventmanagment.service.BookingService;
import com.iit.eventmanagment.service.EventService;
import com.iit.eventmanagment.service.impl.BookingServiceImpl;
import com.iit.eventmanagment.service.impl.EventServiceImpl;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import com.lynden.gmapsfx.GoogleMapView;

import application.utils.MessageType;
import application.utils.SendHTMLEmail;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;


/**
 * This class handles the student view. It allows student to register for the events.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class StudentController implements Initializable {

	@FXML
	private AnchorPane organiserAnchorPane;

	@FXML
	private MenuItem profilePanelMenuItem;

	@FXML
	private MenuItem myEventPanelMenuItem;

	@FXML
	private VBox registrationDetailsVBox;

	@FXML
	private Label eventNameLabel;

	@FXML
	private Label eventLocationLabel;

	@FXML
	private Label eventDateTimeLabel;

	@FXML
	private VBox bookingStatusVBox;

	@FXML
	private Label bookingDateTime;

	@FXML
	private JFXButton confirmBookingButton;

	@FXML
	private JFXButton cancelBookingButton;
/*
	@FXML
	private JFXButton downloadReceiptButton;
*/
	@FXML
	private JFXButton registerEventButton;

	@FXML
	private JFXButton viewEventButton;

	@FXML
	private JFXTextField searchEventTextField;

	@FXML
	private JFXButton refreshEventButton;

	@FXML
	private TableView<Event> eventTableView;

	@FXML
	private TableColumn<Event, Integer> eventIDTableColumn;

	@FXML
	private TableColumn<Event, String> eventNameTableColumn;

	@FXML
	private TableColumn<Event, String> eventLocationTableColumn;

	@FXML
	private TableColumn<Event, LocalDateTime> eventStartDateTimeTableColumn;

	@FXML
	private TableColumn<Event, LocalDateTime> eventEndDateTimeTableColumn;

	@FXML
	private TableColumn<Event, String> eventEmailTableColumn;

	@FXML
	private TableColumn<Event, String> eventPhoneTableColumn;

	@FXML
	private TableColumn<Event, Integer> eventSeatAvailTableColumn;

	@FXML
	private CheckBox eventReminderCheckBox;
	private Stage studentStage;
	private LoginController loginController;
	private Users loggedInUser;
	private ObservableList<Event> eventDataList = FXCollections.observableArrayList();

	private Event currentEvent;

	/**
	 * Sets the student stage and loggedIn user
	 * @param studentStage
	 * @param loggedInUser
	 */
	public StudentController(Stage studentStage, Users loggedInUser) {
		setStudentStage(studentStage);
		setLoggedInUser(loggedInUser);
	}

	public Stage getStudentStage() {
		return studentStage;
	}

	public void setStudentStage(Stage studentStage) {
		this.studentStage = studentStage;
	}

	public LoginController getLoginController() {
		return loginController;
	}

	public void setLoginController(LoginController loginController) {
		this.loginController = loginController;
	}

	public Users getLoggedInUser() {
		return loggedInUser;
	}

	public void setLoggedInUser(Users loggedInUser) {
		this.loggedInUser = loggedInUser;
	}

	public ObservableList<Event> getEventDataList() {
		return eventDataList;
	}

	public void setEventDataList(ObservableList<Event> eventDataList) {
		this.eventDataList = eventDataList;
	}

	/**
	 * Clears the registration cart.
	 * @param event
	 */
	@FXML
	void onCancelBookingClick(ActionEvent event) {
		clearBookingPanel();
	}

	/**
	 * Updates the event table views.
	 * @param event
	 */
	@FXML
	void onRefreshEventClick(ActionEvent event) {
		try {
			refreshList();
		} catch (Exception e) {
			TrayNotification tray = new TrayNotification();
            tray.setNotificationType(NotificationType.ERROR);
            tray.setTitle("Refresh Failure");
            tray.setMessage("Could not refresh the events");
            tray.setAnimationType(AnimationType.SLIDE);
            tray.showAndDismiss(Duration.millis(3000));
            e.printStackTrace();
		}
	}



	/**
	 * Books this event for the user.
	 * @param event
	 */
	@FXML
	void onConfirmBookingClick(ActionEvent event) {
		try {
			BookingService eventService = new BookingServiceImpl();
			BookEvent newBookEvent = new BookEvent();
			newBookEvent.setUserId(loggedInUser.getId());
			newBookEvent.setEventId(currentEvent.getId());
			newBookEvent.setReminder(eventReminderCheckBox.isSelected());
			newBookEvent = eventService.addBooking(loggedInUser, newBookEvent);
			if(newBookEvent != null){
				System.out.println(newBookEvent.getBookingDateTime().toString());
				bookingDateTime.setText("Booking Date and Time:\n"+newBookEvent.getBookingDateTime().toString());
				//downloadReceiptButton.setDisable(false);
				SendHTMLEmail sendEmail = new SendHTMLEmail();
				String message = sendEmail.getMessage(newBookEvent, MessageType.USER_EVENT_REGISTRATION);
				sendEmail.sendMessage(loggedInUser.getEmail(),message, MessageType.USER_EVENT_REGISTRATION);
				TrayNotification tray = new TrayNotification();
	            tray.setNotificationType(NotificationType.SUCCESS);
	            tray.setTitle("Registration Successfull");
	            tray.setMessage("See you at the event, Good Day!");
	            tray.setAnimationType(AnimationType.SLIDE);
	            tray.showAndDismiss(Duration.millis(3000));
			} else{
				Alert alert = new Alert(AlertType.WARNING);
				alert.setTitle("Event Manager");
				alert.setHeaderText("Confirm  Event");
				alert.setContentText("Message: Event could not be confirmed!");
				alert.showAndWait();
			}
			registrationDetailsVBox.opacityProperty().set(0);
			confirmBookingButton.setDisable(true);
			cancelBookingButton.setDisable(true);
			System.out.println(bookingStatusVBox.getOpacity());
			bookingStatusVBox.opacityProperty().set(1);

			new Thread(new Runnable(){
				@Override
				 public void run() {
					try {
						Thread.sleep(5000);
						clearBookingPanel();
					} catch (InterruptedException e) {
						//e.printStackTrace();
					}
				}}).start();
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Event Manager");
			alert.setHeaderText("Oopps, error while booking event!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}

	/**
	 * Clears the cart.
	 */
	private void clearBookingPanel() {
		eventNameLabel.setText("");
		eventLocationLabel.setText("");
		eventDateTimeLabel.setText("");
		bookingDateTime.setText("");
		bookingStatusVBox.setOpacity(0);
		registrationDetailsVBox.setOpacity(0);
		confirmBookingButton.setDisable(true);
		cancelBookingButton.setDisable(true);
		//downloadReceiptButton.setDisable(true);
	}


	/**
	 * Displays all the bookings of the user.
	 * @param event
	 */
	@FXML
	void onMyEventPanelClick(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getClassLoader().getResource("\\com\\iit\\eventmanagment\\view\\my_bookings.fxml"));
			Stage bookingStage = new Stage();
			bookingStage.setTitle("My Bookings");
			MyBookingController myBookingController = new MyBookingController(loggedInUser);
			loader.setController(myBookingController);
			Parent root;
			root = loader.load();
			bookingStage.setScene(new Scene(root));
			bookingStage.show();
		} catch (IOException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Resource");
			alert.setHeaderText("Oopps, error loading resources!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
			e.printStackTrace();
		}
	}

	/**
	 * Displays student profile. Allows updating of profile
	 * @param event
	 */
	@FXML
	void onProfilePanelClick(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getClassLoader().getResource("\\com\\iit\\eventmanagment\\view\\my_profile.fxml"));
			loader.load();
			Parent root = loader.getRoot();
			Stage myProfileStage = new Stage();
			myProfileStage.setTitle("My Profle");
			myProfileStage.setScene(new Scene(root));
			MyProfileController myProfileController = loader.<MyProfileController>getController();
			myProfileController.setLoggedInUser(this.loggedInUser);
			myProfileController.displayProfile();
			myProfileStage.show();
		} catch (IOException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("My Profle");
			alert.setHeaderText("Oopps, View Profile!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}

	/**
	 * Adds selected event to the cart and enables confirm button to confirm the booking.
	 * @param event
	 */
	@FXML
	void onRegisterEventClick(ActionEvent event) {
		currentEvent = eventTableView.getSelectionModel().getSelectedItem();
		if(registerEvent(currentEvent)){
			confirmBookingButton.setDisable(false);
			cancelBookingButton.setDisable(false);
			//downloadReceiptButton.setDisable(true);
		}
	}

	/**Adds the selected event to the cart.
	 * @param currentEvent
	 * @return true if selected event was added to the cart successfully
	 */
	private boolean registerEvent(Event currentEvent) {
		boolean result = true;
		if(currentEvent!=null){
			registrationDetailsVBox.setOpacity(1);
			eventNameLabel.setText(currentEvent.getName());
			eventLocationLabel.setText(currentEvent.getEventAddress()+"\n"+currentEvent.getEventCity()+"-"+currentEvent.getEventPostalCode()+
					"\n"+currentEvent.getEventState()+", "+currentEvent.getEventCountry());
			SimpleDateFormat startDateTimeFormat = new SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm z");
			SimpleDateFormat endDateTimeFormat = new SimpleDateFormat("yyyy.MM.dd G 'till' HH:mm z");
			String startDateTime = startDateTimeFormat.format(Timestamp.valueOf(LocalDateTime.of(currentEvent.getEventStartDate(), currentEvent.getEventStartTime())));
			String lastDateTime = endDateTimeFormat.format(Timestamp.valueOf(LocalDateTime.of(currentEvent.getEventEndDate(), currentEvent.getEventEndTime())));
			eventDateTimeLabel.setText(startDateTime+"\n"+lastDateTime);
			result = true;
		} else{
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Event Manager");
			alert.setHeaderText(" Register Event");
			alert.setContentText("Message: Please select the event first!");
			alert.showAndWait();
			result = false;
		}
		return result;
	}

	/**
	 * Locates the event on the screen.
	 * @param event
	 */
	@FXML
	void onViewEventClick(ActionEvent event) {
		try {
			Event eventToTrack = eventTableView.getSelectionModel().getSelectedItem();
			if(eventToTrack != null){
				Stage locateEventStage = new Stage();
				// controller to manage gmap.
				LocateEventController locateEventController = new LocateEventController(locateEventStage, loggedInUser, eventToTrack);
				// initializes the gmap view.
				GoogleMapView  mapView = new GoogleMapView();
				mapView.addMapInializedListener(locateEventController);
				locateEventController.setMapView(mapView);
				Scene scene = new Scene(mapView);
				locateEventStage.setTitle("JavaFX and Google Maps");
				locateEventStage.setScene(scene);
				locateEventStage.show();
			}else{
				// notifies if user has not selected any event from the table
				TrayNotification tray = new TrayNotification();
				tray.setNotificationType(NotificationType.INFORMATION);
				tray.setTitle("View Event");
				tray.setMessage("Please select the event first!");
				tray.setAnimationType(AnimationType.SLIDE);
				tray.showAndDismiss(Duration.millis(2000));
			}
		} catch (Exception ex) {
			TrayNotification tray = new TrayNotification();
			tray.setNotificationType(NotificationType.ERROR);
			tray.setTitle("View Event");
			tray.setMessage("Oopps, error loading event");
			tray.setAnimationType(AnimationType.SLIDE);
			tray.showAndDismiss(Duration.millis(2000));
			ex.printStackTrace();
		}
	}

	/**
	 * Refreshes the events in the table
	 * @throws Exception if tableview could not be refreshed
	 */
	private void refreshList() throws Exception{
		EventService eventService = new EventServiceImpl();
		List<Event> eventList = eventService.viewAllEvent();
		Iterator it = eventDataList.iterator();
		while (it.hasNext()) {
			it.next();
			it.remove();
		}
		eventDataList.addAll(eventList);
		eventTableView.setItems(eventDataList);
	}
	/**
	 * Loads the events into tableview.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			eventIDTableColumn.setCellValueFactory(cellData -> cellData.getValue().eventIDProperty().asObject());
			eventNameTableColumn.setCellValueFactory(cellData -> cellData.getValue().eventNameProperty());
			eventLocationTableColumn.setCellValueFactory(cellData -> cellData.getValue().getEventLocationProperty());
			eventStartDateTimeTableColumn.setCellValueFactory(cellData -> cellData.getValue().getEventStartDateTimeProperty());
			eventEndDateTimeTableColumn.setCellValueFactory(cellData -> cellData.getValue().getEventEndDateTimeProperty());
			eventEmailTableColumn.setCellValueFactory(cellData -> cellData.getValue().eventEmailProperty());
			eventPhoneTableColumn.setCellValueFactory(cellData -> cellData.getValue().eventPhoneProperty());
			eventSeatAvailTableColumn.setCellValueFactory(cellData -> cellData.getValue().noOfSeatsIntegerProperty().asObject());
			refreshList();
			FilteredList<Event> eventFilteredData = new FilteredList<>(eventDataList, e -> true);
			 // setting changed  listener on key released event.
			searchEventTextField.setOnKeyReleased(e -> {
				searchEventTextField.textProperty().addListener((observableValue, oldValue, newValue) -> {
					eventFilteredData.setPredicate((Predicate<? super Event>) event -> {
						// for event character entered, match data in following columns:
						//(id, name, city, state, postalcode, phone, email)
						String lowerCaseFilter = newValue.toLowerCase();
						if(newValue == null || newValue.isEmpty())
							return true;
						else if(String.valueOf(event.getId()).contains(lowerCaseFilter))
							return true;
						else if(event.getName().toLowerCase().contains(lowerCaseFilter))
							return true;
						else if(event.getEventCity().toLowerCase().contains(lowerCaseFilter))
							return true;
						else if(event.getEventState().toLowerCase().contains(lowerCaseFilter))
							return true;
						else if(event.getEventPostalCode().toLowerCase().contains(lowerCaseFilter))
							return true;
						else if(event.getEventPhone().toLowerCase().contains(lowerCaseFilter))
							return true;
						else if(event.getEventEmail().toLowerCase().contains(lowerCaseFilter))
							return true;
						return false;
					});
				});
			});
			// sets the filtered data to the column.
			eventTableView.setItems(eventFilteredData);
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Event Manager");
			alert.setHeaderText("Oopps, error loading events!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}
}

