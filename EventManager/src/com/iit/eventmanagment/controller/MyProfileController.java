package com.iit.eventmanagment.controller;

import java.net.URL;
import java.util.ResourceBundle;
import com.iit.eventmanagment.model.Department;
import com.iit.eventmanagment.model.Users;
import com.iit.eventmanagment.service.DepartmentService;
import com.iit.eventmanagment.service.UserService;
import com.iit.eventmanagment.service.impl.DepartmentServiceImpl;
import com.iit.eventmanagment.service.impl.UserServiceImpl;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.RequiredFieldValidator;

import application.utils.InputValidator;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.control.Alert.AlertType;

/**
 * This class allows user to view and update his profile.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class MyProfileController implements Initializable{
	@FXML
	private JFXTextField firstNameTextField;

	@FXML
	private JFXTextField lastNameTextField;

	@FXML
	private JFXComboBox<Department> departmentComboBox;

	@FXML
	private JFXTextField emailTextField;

	@FXML
	private JFXTextField phoneTextField;

	@FXML
	private JFXButton saveProfileButton;

	@FXML
	private JFXButton cancelProfileClick;

	@FXML
	private Label nameLabel;

	@FXML
	private Label departmentLabel;

	@FXML
	private Label emailLabel;

	@FXML
	private Label phoneLabel;

	@FXML
	private JFXButton editProfileButton;

    @FXML
    private VBox editProfileVBox;

	DepartmentService departmentService;
	ObservableList<Department> deptList;
	private Users loggedInUser;
	private Stage myProfileStage;
	// required field validators for profile attributes
	RequiredFieldValidator firstNameValidator = new RequiredFieldValidator();
	RequiredFieldValidator lastNameValidator = new RequiredFieldValidator();
	RequiredFieldValidator emailValidator = new RequiredFieldValidator();
	RequiredFieldValidator phoneNoValidator = new RequiredFieldValidator();

	/**
	 * Initialize the department with combo box with all the departments and registers required field listeners.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		departmentService = new DepartmentServiceImpl();
		deptList = FXCollections.observableArrayList();
		try {
			deptList.addAll(departmentService.viewAllDepartment());
			departmentComboBox.setItems(deptList);
			registerListeners();
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("ERROR");
			alert.setHeaderText("Oopps, error while registration!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}

	/**
	 * Register required field validators
	 */
	private void registerListeners() {
		firstNameTextField.getValidators().add(firstNameValidator);
		lastNameTextField.getValidators().add(lastNameValidator);
		emailTextField.getValidators().add(emailValidator);
		phoneTextField.getValidators().add(phoneNoValidator);

		firstNameTextField.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(!newValue){
					firstNameValidator.setMessage("field is required");
					firstNameTextField.validate();
				}
			}
		});
		lastNameTextField.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(!newValue){
					lastNameValidator.setMessage("field is required");
					lastNameTextField.validate();
				}
			}
		});
		emailTextField.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(!newValue){
					emailValidator.setMessage("email is required");
					emailTextField.validate();
				}
			}
		});
		phoneTextField.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(!newValue){
					phoneNoValidator.setMessage("phone number is required");
					phoneTextField.validate();
				}
			}
		});

	}

	public Stage getMyProfileStage() {
		return myProfileStage;
	}

	public void setMyProfileStage(Stage myProfileStage) {
		this.myProfileStage = myProfileStage;
	}

	public Users getLoggedInUser() {
		return loggedInUser;
	}

	public void setLoggedInUser(Users loggedInUser) {
		this.loggedInUser = loggedInUser;
	}

	@FXML
	void onCancelProfileClick(ActionEvent event) {
		this.myProfileStage.close();
	}

	/**
	 * Edits the user profile
	 * @param event
	 */
	@FXML
	void onEditProfileClick(ActionEvent event) {
		editProfileVBox.setDisable(false);
		firstNameTextField.setText(loggedInUser.getFirstName());
		lastNameTextField.setText(loggedInUser.getLastName());
		departmentComboBox.setValue(loggedInUser.getDepartment());
		emailTextField.setText(loggedInUser.getEmail());
		phoneTextField.setText(loggedInUser.getPhoneNumber());
	}

	/**
	 * Saves the edited user profile and updates the views.
	 * @param event
	 */
	@FXML
	void onSaveProfileClick(ActionEvent event) {
		Alert alert1 = new Alert(AlertType.INFORMATION);
		alert1.setTitle("Event Manager");
		alert1.setHeaderText("Event Manager");
		try {
			if(!(firstNameTextField.validate()&&lastNameTextField.validate()&&emailTextField.validate()&&phoneTextField.validate())){
				alert1.setHeaderText("Event Manager");
				alert1.setContentText("Please fill the required fields.");
				alert1.showAndWait();
			}else{
				boolean isValid = true;
				if(!InputValidator.validateName(firstNameTextField.getText())){
					 isValid = false;
					alert1.setContentText("Please enter valid firstname.");
					alert1.showAndWait();
				}else if(!InputValidator.validateName(lastNameTextField.getText())){
					 isValid = false;
					alert1.setContentText("Please enter valid lastname.");
					alert1.showAndWait();
				}else if(!InputValidator.validateEmail(emailTextField.getText())){
					 isValid = false;
					alert1.setContentText("Please enter valid email.");
					alert1.showAndWait();
				}else if(!InputValidator.validatePhoneNumber(phoneTextField.getText())){
					 isValid = false;
					alert1.setContentText("Please enter valid phone number.");
					alert1.showAndWait();
				}
				if(isValid){
					UserService userService = new UserServiceImpl();
					Users updatedUser = new Users();
					updatedUser.setId(loggedInUser.getId());
					updatedUser.setFirstName(firstNameTextField.getText());
					updatedUser.setLastName(lastNameTextField.getText());
					updatedUser.setDepartment(departmentComboBox.getValue());
					updatedUser.setEmail(emailTextField.getText());
					updatedUser.setPhoneNumber(phoneTextField.getText());
					System.out.println("Updated user: "+updatedUser);
					if(userService.updateUser(updatedUser)){
						Alert alert = new Alert(AlertType.INFORMATION);
						alert.setTitle("My Profile");
						alert.setHeaderText("Success");
						alert.setContentText("User update successfully!");
						alert.showAndWait();
						loggedInUser = updatedUser;
						editProfileVBox.setDisable(true);
						displayProfile();
					} else {
						Alert alert = new Alert(AlertType.ERROR);
						alert.setTitle("MY PROFILE");
						alert.setHeaderText("Update User"+loggedInUser.getFirstName());
						alert.setContentText("Oopps, error while updating details!");
						alert.showAndWait();
					}
				}
			}
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("ERROR");
			alert.setHeaderText("Error");
			alert.setContentText("Oopps, error while saving details!"+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}

	/**
	 * Displays the data into view.
	 */
	public void displayProfile() {
		nameLabel.setText(loggedInUser.getFirstName()+" "+loggedInUser.getLastName());
		departmentLabel.setText(loggedInUser.getDepartment().getDepartmentName());
		emailLabel.setText(loggedInUser.getEmail());
		phoneLabel.setText(loggedInUser.getPhoneNumber());
	}
}
