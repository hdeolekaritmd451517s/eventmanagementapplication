package com.iit.eventmanagment.controller;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.List;
import java.util.ResourceBundle;

import com.iit.eventmanagment.model.Event;
import com.iit.eventmanagment.model.Users;
import com.iit.eventmanagment.service.EventService;
import com.iit.eventmanagment.service.impl.EventServiceImpl;
import com.jfoenix.controls.JFXDatePicker;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

/**
 * This class is the subclass of MainController. It is a controller for the oragnizer role.
 * It handles oragnizer's menubar actions. Based on the action called, it loads the fxml file
* * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 *
 */
public class OrganiserController extends AdminController implements Initializable {
	@FXML
    private AnchorPane organiserAnchorPane;
    @FXML
    private MenuItem profilePanelMenuItem;
    @FXML
    private MenuItem resourcePanelMenuItem;
    @FXML
    private MenuItem ticketPanelMenuItem;
    @FXML
    private MenuItem assignResourcePanelMenuItem;
    @FXML
    private MenuItem assignTicketPanelMenuItem;

    /**
	 * Constructor sets the organizer stage and organizer.
	 * @param organizerStage organizer to be set
	 * @param loggedInUser loggedInUser
	 */
	public OrganiserController(Stage organiserStage, Users loggedInUser) {
		super(organiserStage, loggedInUser);
	}

	public Users getLoggedInUser() {
		return loggedInUser;
	}

	public void setLoggedInUser(Users loggedInUser) {
		this.loggedInUser = loggedInUser;
	}

	public LoginController getLoginController() {
		return loginController;
	}

	public void setLoginController(LoginController loginController) {
		this.loginController = loginController;
	}

	/**
	 * Allows the organizer to register for the event.(Simply displays the student view)
	 */
	@FXML
	void onEventRegisterClick(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getClassLoader().getResource("\\com\\iit\\eventmanagment\\view\\student.fxml"));
			Stage studentStage = new Stage();
			studentStage.setTitle("Event Registration");
			StudentController studentController = new StudentController(studentStage, loggedInUser);
			loader.setController(studentController);
			Parent root;
			root = loader.load();
			studentStage.setScene(new Scene(root));
			studentStage.show();
		} catch (IOException e) {
			TrayNotification tray = new TrayNotification();
            tray.setNotificationType(NotificationType.ERROR);
            tray.setTitle("Event Manager");
            tray.setMessage("Unable open registeration.\nContact System Administration.");
            tray.setAnimationType(AnimationType.SLIDE);
            tray.showAndDismiss(Duration.millis(1500));
			e.printStackTrace();
		}
	}

    /**
     * Allows user to assign resources to events.
     * @param event
     */
    @FXML
    void onAssignResourcePanelClick(ActionEvent event) {
    	try {
    		FXMLLoader loader = new FXMLLoader();
        	loader.setLocation(getClass().getClassLoader().getResource("\\com\\iit\\eventmanagment\\view\\assign_event_resources.fxml"));
        	Stage assignResourceStage = new Stage();
        	assignResourceStage.setTitle("Assign Resources");
    		AssignEventResourceController assignEventResourceController = new AssignEventResourceController(assignResourceStage, loggedInUser);
        	loader.setController(assignEventResourceController);
        	Parent root = loader.load();
        	assignResourceStage.setScene(new Scene(root));
        	assignResourceStage.show();
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Event Manager");
			alert.setHeaderText("Oopps, error while loading resources!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
    }

    /**
     * Diplays view for user to edit his profile.
     */
    @FXML
    void onProfilePanelClick(ActionEvent event) {
    	try {
    		FXMLLoader loader = new FXMLLoader();
        	loader.setLocation(getClass().getClassLoader().getResource("\\com\\iit\\eventmanagment\\view\\my_profile.fxml"));
			loader.load();
			Parent root = loader.getRoot();
			Stage myProfileStage = new Stage();
			myProfileStage.setTitle("My Profle");
			myProfileStage.setScene(new Scene(root));
			MyProfileController myProfileController = loader.<MyProfileController>getController();
			myProfileController.setLoggedInUser(this.loggedInUser);
			myProfileController.displayProfile();
			myProfileStage.show();
		} catch (IOException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("My Profle");
			alert.setHeaderText("Oopps, View Profile!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
    }

    /**
     * Diplays view which allows user to add and view his resources
     */
    @FXML
    void onResourcePanelClick(ActionEvent event) {
    	try {
    		FXMLLoader loader = new FXMLLoader();
        	loader.setLocation(getClass().getClassLoader().getResource("\\com\\iit\\eventmanagment\\view\\resources.fxml"));
			loader.load();
			Parent root = loader.getRoot();
			Stage resourcePanelStage = new Stage();
			resourcePanelStage.setTitle("Resource Panel");
			resourcePanelStage.setScene(new Scene(root));
			ResourceController resourceController = loader.<ResourceController>getController();
			resourceController.setLoggedInUser(loggedInUser);
			resourcePanelStage.show();
		} catch (IOException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("My Profle");
			alert.setHeaderText("Oopps, View Profile!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
    }

    /**
     * Diplays view which allows user to add, edit, update delete ticket. Additionally assign tickets to the event.
     */
    @FXML
    void onTicketPanelClick(ActionEvent event) {
    	try {
    		FXMLLoader loader = new FXMLLoader();
        	loader.setLocation(getClass().getClassLoader().getResource("\\com\\iit\\eventmanagment\\view\\ticket.fxml"));
			Stage ticketPanelStage = new Stage();
			ticketPanelStage.setTitle("Ticket Panel");
			TicketController ticketController = new TicketController(ticketPanelStage, loggedInUser);
			loader.setController(ticketController);
			Parent root = loader.load();
			ticketPanelStage.setScene(new Scene(root));
			ticketPanelStage.show();
		} catch (IOException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Ticket");
			alert.setHeaderText("Oopps, could not load ticket screen!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
    }
}

