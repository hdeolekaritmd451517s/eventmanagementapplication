package com.iit.eventmanagment.controller;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import com.iit.eventmanagment.model.Department;
import com.iit.eventmanagment.model.Roles;
import com.iit.eventmanagment.service.DepartmentService;
import com.iit.eventmanagment.service.RolesService;
import com.iit.eventmanagment.service.impl.DepartmentServiceImpl;
import com.iit.eventmanagment.service.impl.RolesServiceImpl;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.RequiredFieldValidator;

import application.utils.InputValidator;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;

/**
 * This class allows user to add, edit, update, delete department. This feature is only available to the admin.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
/**
 * @author Harshal
 *
 */
public class DepartmentController implements Initializable {

	Department currentDepartment;

	@FXML
	private AnchorPane departmentAnchorPane;

	@FXML
	private HBox addDepartmentHBox;

	@FXML
	private JFXTextField addDepartmentTextField;

	@FXML
	private TableView<Department> departmentTableView;

	@FXML
	private TableColumn<Department, Integer> departmentIDTableColumn;

	@FXML
	private TableColumn<Department, String> departmentNameTableColumn;

	@FXML
	private JFXButton addDepartmentButton;

	@FXML
	private JFXButton editDepartmentButton;

	@FXML
	private JFXButton saveDepartmentButton;

	@FXML
	private JFXButton updateDepartmentButton;

	@FXML
	private JFXButton deleteDepartmentButton;

	private ObservableList<Department> departmentDataList = FXCollections.observableArrayList();

	RequiredFieldValidator departmentNameValidator = new RequiredFieldValidator();

	/**
	 * This method is called on Add button click. It allows user to add a new department.
	 * It enables save button and disables update and delete button.
	 * @param event
	 */
	@FXML
	void onAddDepartmentClick(ActionEvent event) {
		addDepartmentHBox.setDisable(false);
		saveDepartmentButton.setDisable(false);
		updateDepartmentButton.setDisable(true);
		deleteDepartmentButton.setDisable(true);
		clearDepartment();
	}


	/**
	 * This method clears the add department textfield;
	 */
	private void clearDepartment() {
		if(addDepartmentTextField.getText() != null)
			addDepartmentTextField.clear();
	}

	/**
	 * This method allows user to delete the department.
	 * @param event
	 */
	@FXML
	void onDeleteDepartmentClick(ActionEvent event) {
		try {
			DepartmentService departmentService = new DepartmentServiceImpl();
			Department departmentTobeDeleted = getEditedDepartment();
			boolean result = departmentService.deleteDepartment(departmentTobeDeleted);
			if(result){
				int selectedIndex = departmentTableView.getSelectionModel().getSelectedIndex();
				if (selectedIndex >= 0) {
					// if department was deleted remove it from the tableview.
					departmentTableView.getItems().remove(selectedIndex);
				} else {
					// Nothing selected.
					Alert alert = new Alert(AlertType.WARNING);
					alert.setTitle("Delete Department");
					alert.setHeaderText("Oopps, error deleting department!");
					alert.setContentText("Please select the department.");
					alert.showAndWait();
				}
			}
			// after delete, disable update and delete button and clears the textfields.
			updateDepartmentButton.setDisable(true);
			deleteDepartmentButton.setDisable(true);
			addDepartmentHBox.setDisable(true);
			clearDepartment();
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Update Department");
			alert.setHeaderText("Oopps, error updating department!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}


	/**
	 * This method creates a new department object with the edited fields.
	 * @return
	 */
	private Department getEditedDepartment() {
		Department newEditedDepartment = new Department(currentDepartment.getId(), addDepartmentTextField.getText());
		return newEditedDepartment;
	}

	/**
	 * Checks if user  has selected the department to be edited, if yes, allows user to edit data.
	 * @param event
	 */
	@FXML
	void onEditDepartmentClick(ActionEvent event) {
		currentDepartment = departmentTableView.getSelectionModel().getSelectedItem();
		if(editDepartment(currentDepartment)){
			saveDepartmentButton.setDisable(true);
			updateDepartmentButton.setDisable(false);
			deleteDepartmentButton.setDisable(false);
		}
	}

	/**
	 * If user has selected the department, edits the depart in the textfield
	 * @param currentDepartment
	 * @return
	 */
	private boolean editDepartment(Department currentDepartment) {
		boolean result = false;
		if(currentDepartment != null){
			addDepartmentHBox.setDisable(false);
			addDepartmentTextField.setText(currentDepartment.getDepartmentName());
			result = true;
		} else{
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Department");
			alert.setHeaderText("EDIT Department");
			alert.setContentText("Message: Please select the field first!");
			alert.showAndWait();
			result = false;
		}
		return result;
	}

	/**
	 * Allows user to save the department. On successful addition, disables the add,update and delete button.
	 * @param event
	 */
	@FXML
	void onSaveDepartmentClick(ActionEvent event) {
		try {
			Alert alert1 = new Alert(AlertType.INFORMATION);
			alert1.setTitle("Event Manager");
			alert1.setHeaderText("Event Manager");
			if(!addDepartmentTextField.validate()){
				alert1.setHeaderText("Event Manager");
				alert1.setContentText("Please fill the required fields.");
				alert1.showAndWait();
			}else{
				boolean isValid = true;
				// validates department name. only a-zA-Z allowed.
				if(!InputValidator.validateName(addDepartmentTextField.getText())){
					isValid = false;
					alert1.setContentText("Please enter valid firstname.");
					alert1.showAndWait();
				}
				// If name is valid, add it to the database
				if(isValid){
					DepartmentService departmentService = new DepartmentServiceImpl();
					Department newDepartment = getNewDepartment();
					newDepartment = departmentService.addDepartment(newDepartment);
					// if added successfully, add it to the table through observable list.
					if(newDepartment != null){
						departmentDataList.add(newDepartment);
						// clears the department textfield
						clearDepartment();
					} else{
						Alert alert = new Alert(AlertType.ERROR);
						alert.setTitle("Role");
						alert.setHeaderText("Add Role");
						alert.setContentText("Message: Role could not be saved!");
						alert.showAndWait();
					}
					addDepartmentHBox.setDisable(true);
					saveDepartmentButton.setDisable(true);
					updateDepartmentButton.setDisable(true);
					deleteDepartmentButton.setDisable(true);
				}
			}
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("ADD Role");
			alert.setHeaderText("Oopps, error adding roles!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}

	/**
	 * Creates new department object using user input.
	 * @return department object to be added.
	 */
	private Department getNewDepartment() {
		Department newDepartment = new Department(addDepartmentTextField.getText());
		return newDepartment;
	}

	/**
	 * Allows the department to be updated with new edited values.
	 * After update is successful, disables update and delete button.
	 * @param event
	 */
	@FXML
	void onUpdateDepartmentClick(ActionEvent event) {
		Department newEditedDepartment;
		try {
			Alert alert1 = new Alert(AlertType.INFORMATION);
			alert1.setTitle("Event Manager");
			alert1.setHeaderText("Event Manager");
			if(!addDepartmentTextField.validate()){
				alert1.setHeaderText("Event Manager");
				alert1.setContentText("Please fill the required fields.");
				alert1.showAndWait();
			}else{
				boolean isValid = true;
				if(!InputValidator.validateName(addDepartmentTextField.getText())){
					isValid = false;
					alert1.setContentText("Please enter valid department name.");
					alert1.showAndWait();
				}

				if(isValid){
					DepartmentService departmentService = new DepartmentServiceImpl();
					newEditedDepartment = getEditedDepartment();
					boolean result = departmentService.updateDepartment(newEditedDepartment);
					// if update successfull, update the table.
					if(result){
						updateTableRecord(newEditedDepartment);
					} else {
						Alert alert = new Alert(AlertType.ERROR);
						alert.setTitle("Department");
						alert.setHeaderText("Update Department");
						alert.setContentText("Message: No records were updated");
						alert.showAndWait();
					}
					updateDepartmentButton.setDisable(true);
					deleteDepartmentButton.setDisable(true);
					addDepartmentHBox.setDisable(true);
					clearDepartment();
				}
			}
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Update Department");
			alert.setHeaderText("Oopps, error updating department!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}

	/**
	 * Sets the currently editted department with newly values.
	 * @param newEditedDepartment
	 */
	private void updateTableRecord(Department newEditedDepartment) {
		currentDepartment.setId(newEditedDepartment.getId());
		currentDepartment.setDepartmentName(newEditedDepartment.getDepartmentName());
	}

	/**
	 * Initializes tableview with all the departments. Add required field listeners.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			departmentIDTableColumn.setCellValueFactory(cellData -> cellData.getValue().departmentIdProperty().asObject());
			departmentNameTableColumn.setCellValueFactory(cellData -> cellData.getValue().departmentNameProperty());
			DepartmentService departmentService = new DepartmentServiceImpl();
			List<Department> departmentList = departmentService.viewAllDepartment();
			departmentDataList.addAll(departmentList);
			departmentTableView.setItems(departmentDataList);
			addDepartmentTextField.getValidators().add(departmentNameValidator);

			addDepartmentTextField.focusedProperty().addListener(new ChangeListener<Boolean>() {
				@Override
				public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
					if(!newValue){
						departmentNameValidator.setMessage("field is required");
						addDepartmentTextField.validate();
					}
				}
			});
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Department");
			alert.setHeaderText("Oopps, error loading department!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}
}
