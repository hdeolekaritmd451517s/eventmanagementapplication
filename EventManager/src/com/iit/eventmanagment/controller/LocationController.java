package com.iit.eventmanagment.controller;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import com.iit.eventmanagment.model.Location;
import com.iit.eventmanagment.service.LocationService;
import com.iit.eventmanagment.service.impl.LocationServiceImpl;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.RequiredFieldValidator;
import application.utils.InputValidator;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.GridPane;

/**
 * This class allows user to add, update and delete the location.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class LocationController implements Initializable{

	Location currentLocation;

	@FXML
	private MenuItem addLocationMenuItem;

	@FXML
	private GridPane locationGridPane;

	@FXML
	private JFXTextField locationNameTextField;

	@FXML
	private JFXTextField locationAddressTextfield;

	@FXML
	private JFXTextField locationCityTextfield;

	@FXML
	private JFXTextField locationStateTextfield;

	@FXML
	private JFXTextField locationCountryTextfield;

	@FXML
	private JFXTextField locationPostalCodeTextfield;

	@FXML
	private JFXTextField locationEmailTextfield;

	@FXML
	private JFXTextField locationPhoneTextfield;

	@FXML
	private JFXTextField locationImageTextField;

	@FXML
	private JFXButton saveLocationButton;

	@FXML
	private JFXButton updateLocationButton;

	@FXML
	private JFXButton deleteLocationButton;

	@FXML
	private JFXButton editLocationButton;

	@FXML
	private JFXTextField searchLocationTexField;

	@FXML
	private JFXButton searchLocationButton;

	@FXML
	private TableView<Location> locationTableView;

	@FXML
	private TableColumn<Location, Integer> locationIDTableColumn;

	@FXML
	private TableColumn<Location, String> locationNameTableColumn;

	@FXML
	private TableColumn<Location, String> locationAddressTableColumn;

	@FXML
	private TableColumn<Location, String> locationCityTableColumn;

	@FXML
	private TableColumn<Location, String> locationStateTableColumn;

	@FXML
	private TableColumn<Location, String> locationCountryTableColumn;

	@FXML
	private TableColumn<Location, String> locationEmailTableColumn;

	@FXML
	private TableColumn<Location, String> locationPhoneTableColumn;

	private ObservableList<Location> locationDataList = FXCollections.observableArrayList();
	// required field validators
	RequiredFieldValidator locationNameValidator = new RequiredFieldValidator();
	RequiredFieldValidator locationCityValidator = new RequiredFieldValidator();
	RequiredFieldValidator locationStateValidator = new RequiredFieldValidator();
	RequiredFieldValidator locationCountryValidator = new RequiredFieldValidator();
	RequiredFieldValidator locationEmailValidator = new RequiredFieldValidator();
	RequiredFieldValidator locationPhoneValidator = new RequiredFieldValidator();

	/**
	 * Initializes the location table with the locations
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			locationIDTableColumn.setCellValueFactory(cellData -> cellData.getValue().locationIDProperty().asObject());
			locationNameTableColumn.setCellValueFactory(cellData -> cellData.getValue().locationNameProperty());
			locationAddressTableColumn.setCellValueFactory(cellData -> cellData.getValue().locationAddressProperty());
			locationCityTableColumn.setCellValueFactory(cellData -> cellData.getValue().locationCityProperty());
			locationStateTableColumn.setCellValueFactory(cellData -> cellData.getValue().locationStateProperty());
			locationCountryTableColumn.setCellValueFactory(cellData -> cellData.getValue().locationCountryProperty());
			locationEmailTableColumn.setCellValueFactory(cellData -> cellData.getValue().locationEmailProperty());
			locationPhoneTableColumn.setCellValueFactory(cellData -> cellData.getValue().locationPhoneProperty());
			LocationService locationService = new LocationServiceImpl();
			List<Location> locationList = locationService.viewAllLocation();
			if(locationList!=null)
				locationDataList.addAll(locationList);
			locationTableView.setItems(locationDataList);
			// register listeners
			registerListeners();
			// adding observable location list to the filterlist
			FilteredList<Location> locationFilteredData = new FilteredList<>(locationDataList, e -> true);
			// adding key released listener to the search textfield, it will search as user inputs the data.
			searchLocationTexField.setOnKeyReleased(e -> {
				searchLocationTexField.textProperty().addListener((observableValue, oldValue, newValue) -> {
					locationFilteredData.setPredicate((Predicate<? super Location>) loc -> {
						String lowerCaseFilter = newValue.toLowerCase();
						if(newValue == null || newValue.isEmpty())
							return true;
						else if(String.valueOf(loc.getId()).contains(lowerCaseFilter))
							return true;
						else if(loc.getLocationName().toLowerCase().contains(lowerCaseFilter))
							return true;
						else if(loc.getLocationAddress().toLowerCase().contains(lowerCaseFilter))
							return true;
						else if(loc.getLocationCity().toLowerCase().contains(lowerCaseFilter))
							return true;
						else if(loc.getLocationState().toLowerCase().contains(lowerCaseFilter))
							return true;
						else if(loc.getLocationPhone().toLowerCase().contains(lowerCaseFilter))
							return true;
						else if(loc.getLocationEmail().toLowerCase().contains(lowerCaseFilter))
							return true;
						return false;
					});
				});
			});
			//finally, setting the filtered data to the tableview
			locationTableView.setItems(locationFilteredData);
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Location");
			alert.setHeaderText("Oopps, error loading locations!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}

	/**
	 * Registering all the input fields with required field validators.
	 */
	private void registerListeners() {
		locationNameTextField.getValidators().add(locationNameValidator);
		locationCityTextfield.getValidators().add(locationCityValidator);
		locationStateTextfield.getValidators().add(locationStateValidator);
		locationCountryTextfield.getValidators().add(locationCountryValidator);
		locationEmailTextfield.getValidators().add(locationEmailValidator);
		locationPhoneTextfield.getValidators().add(locationPhoneValidator);

		locationNameTextField.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(!newValue){
					locationNameValidator.setMessage("field is required");
					locationNameTextField.validate();
				}
			}
		});
		locationCityTextfield.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(!newValue){
					locationCityValidator.setMessage("field is required");
					locationCityTextfield.validate();
				}
			}
		});

		locationStateTextfield.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(!newValue){
					locationStateValidator.setMessage("field is required");
					locationStateTextfield.validate();
				}
			}
		});
		locationCountryTextfield.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(!newValue){
					locationPhoneValidator.setMessage("field is required");
					locationCountryTextfield.validate();
				}
			}
		});
		locationEmailTextfield.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(!newValue){
					locationEmailValidator.setMessage("field is required");
					locationEmailTextfield.validate();
				}
			}
		});
		locationPhoneTextfield.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(!newValue){
					locationPhoneValidator.setMessage("field is required");
					locationPhoneTextfield.validate();
				}
			}
		});
	}

	/**
	 * Called when used clicks on Add location under location panel
	 * @param event
	 */
	@FXML
	void onAddLocationClick(ActionEvent event) {
		locationGridPane.setDisable(false);
		saveLocationButton.setDisable(false);
		updateLocationButton.setDisable(true);
		deleteLocationButton.setDisable(true);
		clearLocation();
	}

	/**
	 * Deletes the location from the database and removes it from the tableviews.
	 * @param event
	 */
	@FXML
	void onDeleteLocationClick(ActionEvent event) {
		try {
			LocationService locationService = new LocationServiceImpl();
			Location locationTobeDeleted = getEditedLocation();
			boolean result = locationService.deleteLocation(locationTobeDeleted);
			if(result){
				int selectedIndex = locationTableView.getSelectionModel().getSelectedIndex();
				if (selectedIndex >= 0) {
					//locationTableView.getItems().remove(selectedIndex);
					locationDataList.remove(selectedIndex);
				} else {
					// Nothing selected.
					Alert alert = new Alert(AlertType.WARNING);
					alert.setTitle("Delete Location");
					alert.setHeaderText("Oopps, error deleting location!");
					alert.setContentText("Please select the location.");
					alert.showAndWait();
				}
			}
			updateLocationButton.setDisable(true);
			deleteLocationButton.setDisable(true);
			locationGridPane.setDisable(true);
			clearLocation();
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Update Location");
			alert.setHeaderText("Oopps, error updating location!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}

	/**
	 * Edits the input fields with the values of the location selected.
	 * @param event
	 */
	@FXML
	void onEditLocationClick(ActionEvent event) {
		currentLocation = locationTableView.getSelectionModel().getSelectedItem();
		if(editLocation(currentLocation)){
			saveLocationButton.setDisable(true);
			updateLocationButton.setDisable(false);
			deleteLocationButton.setDisable(false);
		}
	}

	/**
	 * Saves the table to the database and add it to the tableview
	 * @param event
	 */
	@FXML
	void onSaveLocationClick(ActionEvent event) {
		try {
			if(validateLocation()){
					LocationService locationService = new LocationServiceImpl();
					Location newLocation = getNewLocation();
					newLocation = locationService.addLocation(newLocation);
					// if added to db successfully, add it to the tableview
					if(newLocation != null){
						locationDataList.add(newLocation);
						clearLocation();
					} else{
						Alert alert = new Alert(AlertType.ERROR);
						alert.setTitle("Location");
						alert.setHeaderText("Add Locatio");
						alert.setContentText("Message: Location could not be saved!");
						alert.showAndWait();
					}
					locationGridPane.setDisable(true);
					saveLocationButton.setDisable(true);
					updateLocationButton.setDisable(true);
					deleteLocationButton.setDisable(true);
			}
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Update Location");
			alert.setHeaderText("Oopps, error updating location!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}

	private boolean validateLocation(){
		Alert alert1 = new Alert(AlertType.INFORMATION);
		alert1.setTitle("Event Manager");
		alert1.setHeaderText("Event Manager");
		boolean isValid = true;
		if(!(locationNameTextField.validate()&&locationCityTextfield.validate()&&locationStateTextfield.validate()
				&&locationCountryTextfield.validate()&&locationEmailTextfield.validate()&&locationPhoneTextfield.validate())){
			alert1.setHeaderText("Event Manager");
			alert1.setContentText("Please fill the required fields.");
			alert1.showAndWait();
			isValid = false;
		}else{
			if(!InputValidator.validateName(locationNameTextField.getText())){
				isValid = false;
				alert1.setContentText("Please enter valid location name.");
				alert1.showAndWait();
			} else if(!InputValidator.validateName(locationCityTextfield.getText())){
				isValid = false;
				alert1.setContentText("Please enter valid city name.");
				alert1.showAndWait();
			}else if(!InputValidator.validateName(locationStateTextfield.getText())){
				isValid = false;
				alert1.setContentText("Please enter valid state name.");
				alert1.showAndWait();
			}else if(!InputValidator.validateName(locationCountryTextfield.getText())){
				isValid = false;
				alert1.setContentText("Please enter valid country name.");
				alert1.showAndWait();
			} else if(!InputValidator.validateEmail(locationEmailTextfield.getText())){
				isValid = false;
				alert1.setContentText("Please enter valid email.");
				alert1.showAndWait();
			}else if(!InputValidator.validatePhoneNumber(locationPhoneTextfield.getText())){
				isValid = false;
				alert1.setContentText("Please enter valid phonenumber.");
				alert1.showAndWait();
			}
		}
		return isValid;
	}
	@FXML
	void onUpdateLocationClick(ActionEvent event) {
		Location newEditedLocation;
		try {
			if(validateLocation()){
				LocationService locationService = new LocationServiceImpl();
				newEditedLocation = getEditedLocation();
				boolean result = locationService.updateLocation(newEditedLocation);
				if(result){
					updateTableRecord(newEditedLocation);
				} else {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Location");
					alert.setHeaderText("Update Location");
					alert.setContentText("Message: No records were updated");
					alert.showAndWait();
				}
				updateLocationButton.setDisable(true);
				deleteLocationButton.setDisable(true);
				locationGridPane.setDisable(true);
				clearLocation();
			}
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Update Location");
			alert.setHeaderText("Oopps, error updating location!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}

	private boolean editLocation(Location currentLocation){
		boolean result = false;
		if(currentLocation != null){
			locationGridPane.setDisable(false);
			locationNameTextField.setText(currentLocation.getLocationName());
			locationAddressTextfield.setText(currentLocation.getLocationAddress());
			locationCityTextfield.setText(currentLocation.getLocationCity());
			locationStateTextfield.setText(currentLocation.getLocationState());
			locationCountryTextfield.setText(currentLocation.getLocationCountry());
			locationEmailTextfield.setText(currentLocation.getLocationEmail());
			locationPhoneTextfield.setText(currentLocation.getLocationPhone());
			result = true;
		} else{
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("EVENT");
			alert.setHeaderText("EDIT LOCATION");
			alert.setContentText("Message: Please select the field first!");
			alert.showAndWait();
			result = false;
		}
		return result;
	}

	private Location getEditedLocation(){
		Location newEditedlocation = new Location(currentLocation.getId(), locationNameTextField.getText(), locationAddressTextfield.getText(), locationCityTextfield.getText(),
				locationStateTextfield.getText(), locationCountryTextfield.getText(), locationEmailTextfield.getText(), locationPhoneTextfield.getText());
		return newEditedlocation;
	}

	private void updateTableRecord(Location newEditedLocation){
		currentLocation.setId(newEditedLocation.getId());
		currentLocation.setLocationName(newEditedLocation.getLocationName());
		currentLocation.setLocationAddress(newEditedLocation.getLocationAddress());
		currentLocation.setLocationCity(newEditedLocation.getLocationCity());
		currentLocation.setLocationState(newEditedLocation.getLocationState());
		currentLocation.setLocationCountry(newEditedLocation.getLocationCountry());
		currentLocation.setLocationEmail(newEditedLocation.getLocationEmail());
		currentLocation.setLocationPhone(newEditedLocation.getLocationPhone());
	}

	/**
	 * Creates new location object with newly inputed fields.
	 * @return new location object
	 */
	private Location getNewLocation(){
		Location newlocation = new Location(locationNameTextField.getText(), locationAddressTextfield.getText(), locationCityTextfield.getText(),
				locationStateTextfield.getText(), locationCountryTextfield.getText(), locationEmailTextfield.getText(), locationPhoneTextfield.getText());
		return newlocation;
	}

	/**
	 * Cleats the location input fields
	 */
	private void clearLocation(){
		if(locationNameTextField.getText() != null)
			locationNameTextField.clear();
		if(locationAddressTextfield.getText() != null)
			locationAddressTextfield.clear();
		if(locationCityTextfield.getText() != null)
			locationCityTextfield.clear();
		if(locationStateTextfield.getText() != null)
			locationStateTextfield.clear();
		if(locationCountryTextfield.getText() != null)
			locationCountryTextfield.clear();
		if(locationEmailTextfield.getText() != null)
			locationEmailTextfield.clear();
		if(locationPhoneTextfield.getText() != null)
			locationPhoneTextfield.clear();
	}
}
