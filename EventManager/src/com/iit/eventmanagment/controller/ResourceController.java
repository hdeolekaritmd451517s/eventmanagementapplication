package com.iit.eventmanagment.controller;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import com.iit.eventmanagment.model.Department;
import com.iit.eventmanagment.model.Roles;
import com.iit.eventmanagment.model.Users;
import com.iit.eventmanagment.service.DepartmentService;
import com.iit.eventmanagment.service.ResourceService;
import com.iit.eventmanagment.service.RolesService;
import com.iit.eventmanagment.service.UserService;
import com.iit.eventmanagment.service.impl.DepartmentServiceImpl;
import com.iit.eventmanagment.service.impl.ResourceServiceImpl;
import com.iit.eventmanagment.service.impl.RolesServiceImpl;
import com.iit.eventmanagment.service.impl.UserServiceImpl;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.RequiredFieldValidator;

import application.utils.InputValidator;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * This class handles adding, editing, deleting and updating of resources.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class ResourceController implements Initializable{
	Users currentUser;

	@FXML
	private MenuItem myResourcesMenuItem;

	@FXML
	private MenuItem addResourceMenuItem;

	@FXML
	private GridPane resourceGridPane;

	@FXML
	private JFXTextField resourceFirstNameTextField;

	@FXML
	private JFXTextField resourceLastNameTextField;

	@FXML
	private JFXComboBox<Department> resourceDepartmentComboBox;

	@FXML
	private JFXTextField resourceEmailTextField;

	@FXML
	private JFXTextField resourcePhoneNumberTextField;

	@FXML
	private JFXComboBox<Roles> resourceRoleComboBox;

	@FXML
	private JFXButton saveResourceButton;

	@FXML
	private JFXButton updateResourceButton;

	@FXML
	private JFXButton editResourceButton;


	@FXML
	private JFXTextField resourceSearchTextField;


	@FXML
	private TableView<Users> resourceTableView;

	@FXML
	private TableColumn<Users, Integer> resourceIDTableColumn;

	@FXML
	private TableColumn<Users, String> resourceFirstNameTableColumn;

	@FXML
	private TableColumn<Users, String> resourceLastNameTableColumn;

	@FXML
	private TableColumn<Users, Department> resourceDepartmentTableColumn;

	@FXML
	private TableColumn<Users, String> resourceEmailTableColumn;

	@FXML
	private TableColumn<Users, String> resourcePhoneTableColumn;

	@FXML
	private TableColumn<Users,Roles> resourceRoleTableColumn;

	private Users loggedInUser;

	private ObservableList<Users> userDataList = FXCollections.observableArrayList();

	RequiredFieldValidator resourceFirstNameValidator = new RequiredFieldValidator();
	RequiredFieldValidator resourceLastNameValidator = new RequiredFieldValidator();
	RequiredFieldValidator resourceEmailValidator = new RequiredFieldValidator();
	RequiredFieldValidator resourcePhoneNumberValidator = new RequiredFieldValidator();


	/**
	 * Registering all the input fields with required field validators.
	 */
	private void registerListeners() {
		resourceFirstNameTextField.getValidators().add(resourceFirstNameValidator);
		resourceLastNameTextField.getValidators().add(resourceLastNameValidator);
		resourceEmailTextField.getValidators().add(resourceEmailValidator);
		resourcePhoneNumberTextField.getValidators().add(resourcePhoneNumberValidator);

		resourceFirstNameTextField.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(!newValue){
					resourceFirstNameValidator.setMessage("field is required");
					resourceFirstNameTextField.validate();
				}
			}
		});
		resourceLastNameTextField.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(!newValue){
					resourceLastNameValidator.setMessage("field is required");
					resourceLastNameTextField.validate();
				}
			}
		});
		resourceEmailTextField.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(!newValue){
					resourceEmailValidator.setMessage("field is required");
					resourceEmailTextField.validate();
				}
			}
		});
		resourcePhoneNumberTextField.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(!newValue){
					resourcePhoneNumberValidator.setMessage("field is required");
					resourcePhoneNumberTextField.validate();
				}
			}
		});
	}
	public Users getLoggedInUser() {
		return this.loggedInUser;
	}

	public void setLoggedInUser(Users loggedInUser) {
		this.loggedInUser = loggedInUser;
	}

	/**
	 * This methods enables the save button and disables update button.
	 * It also enables the input form for adding user as a resource.
	 * @param event
	 */
	@FXML
	void onAddResourceClick(ActionEvent event) {
		resourceGridPane.setDisable(false);
		saveResourceButton.setDisable(false);
		updateResourceButton.setDisable(true);
		clearResource();
	}

	private boolean validateResource(){
		Alert alert1 = new Alert(AlertType.INFORMATION);
		alert1.setTitle("Event Manager");
		alert1.setHeaderText("Event Manager");
		boolean isValid = true;
		if(!(resourceFirstNameTextField.validate()&&resourceLastNameTextField.validate()&&resourceEmailTextField.validate()
				&&resourcePhoneNumberTextField.validate())){
			alert1.setHeaderText("Event Manager");
			alert1.setContentText("Please fill the required fields.");
			alert1.showAndWait();
			isValid = false;
		}else{
			if(!InputValidator.validateName(resourceFirstNameTextField.getText())){
				isValid = false;
				alert1.setContentText("Please enter valid first name.");
				alert1.showAndWait();
			} else if(!InputValidator.validateName(resourceLastNameTextField.getText())){
				isValid = false;
				alert1.setContentText("Please enter valid last name.");
				alert1.showAndWait();
			}else if(!InputValidator.validateEmail(resourceEmailTextField.getText())){
				isValid = false;
				alert1.setContentText("Please enter valid email name.");
				alert1.showAndWait();
			}else if(!InputValidator.validatePhoneNumber(resourcePhoneNumberTextField.getText())){
				isValid = false;
				alert1.setContentText("Please enter valid phone number.");
				alert1.showAndWait();
			}
		}
		return isValid;
	}

	/**
	 * Clears the input field.
	 */
	private void clearResource() {
		if(resourceFirstNameTextField.getText() != null)
			resourceFirstNameTextField.clear();
		if(resourceLastNameTextField.getText() != null)
			resourceLastNameTextField.clear();
		if(resourceDepartmentComboBox.getValue() != null)
			resourceDepartmentComboBox.getSelectionModel().clearSelection();
		if(resourceEmailTextField.getText() != null)
			resourceEmailTextField.clear();
		if(resourcePhoneNumberTextField.getText() != null)
			resourcePhoneNumberTextField.clear();
		if(resourceRoleComboBox.getValue() != null)
			resourceRoleComboBox.getSelectionModel().clearSelection();
	}

	/**
	 * This method saves the user to the database
	 * @param event
	 */
	@FXML
	void onSaveResourceClick(ActionEvent event) {
		try {
			if(validateResource()){
				ResourceService resourceService = new ResourceServiceImpl();
				Users newUser = getNewResource();
				if (newUser.getRole().getId()==1){
					Alert alert = new Alert(AlertType.WARNING);
					alert.setTitle("Event Manager");
					alert.setHeaderText("Add Resource");
					alert.setContentText("Message: Cannot add admin as a resource!");
					alert.showAndWait();
					return;
				}
				newUser = resourceService.addResource(newUser, loggedInUser);
				if(newUser != null){
					userDataList.add(newUser);
					clearResource();
				} else{
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Location");
					alert.setHeaderText("Add Locatio");
					alert.setContentText("Message: Location could not be saved!");
					alert.showAndWait();
				}
				resourceGridPane.setDisable(true);
				saveResourceButton.setDisable(true);
				updateResourceButton.setDisable(true);
			}
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Update Location");
			alert.setHeaderText("Oopps, error updating location!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}

	/**
	 * Creates a new resource object using the edited fields.
	 * @return newly edit user object as a resource.
	 */
	private Users getNewResource() {
		Users newEditedUser = new Users(resourceFirstNameTextField.getText(), resourceLastNameTextField.getText(), resourceDepartmentComboBox.getValue(),
				resourceEmailTextField.getText(), resourcePhoneNumberTextField.getText(), resourceRoleComboBox.getValue());
		return newEditedUser;
	}

	/**
	 * Creates the new user object as a resource.
	 * @return user object which will be added as the resource.
	 */
	private Users getEditedResource() {
		Users newEditedUser = new Users(currentUser.getId(), resourceFirstNameTextField.getText(), resourceLastNameTextField.getText(), resourceDepartmentComboBox.getValue(),
				resourceEmailTextField.getText(), resourcePhoneNumberTextField.getText(), resourceRoleComboBox.getValue());
		return newEditedUser;
	}

	/**
	 * Enables user to disables save and enables update button.
	 * @param event
	 */
	@FXML
	void onEditResourceClick(ActionEvent event) {
		currentUser = resourceTableView.getSelectionModel().getSelectedItem();
		if(editResource(currentUser)){
			saveResourceButton.setDisable(true);
			updateResourceButton.setDisable(false);
		}
	}


	/**
	 * This method edits the selected user into input screen
	 * @param currentUser user to be edited
	 * @return true if user details were set for editing, else false
	 */
	private boolean editResource(Users currentUser){
		boolean result = false;
		if(currentUser != null){
			resourceGridPane.setDisable(false);
			resourceFirstNameTextField.setText(currentUser.getFirstName());
			resourceLastNameTextField.setText(currentUser.getLastName());
			resourceDepartmentComboBox.setValue(currentUser.getDepartment());
			resourceDepartmentComboBox.setDisable(false);
			resourceEmailTextField.setText(currentUser.getEmail());
			resourcePhoneNumberTextField.setText(currentUser.getPhoneNumber());
			resourceRoleComboBox.setValue(currentUser.getRole());
			result = true;
		}else{
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Resource Panel");
			alert.setHeaderText("Edit Resource");
			alert.setContentText("Message: Please select the user first!");
			alert.showAndWait();
		}
		return result;
	}


	/**
	 * updates the selected user.
	 * @param newEditedResource
	 */
	private void updateTableRecord(Users newEditedResource){
		currentUser.setId(newEditedResource.getId());
		currentUser.setFirstName(newEditedResource.getFirstName());
		currentUser.setLastName(newEditedResource.getLastName());
		currentUser.setDepartment(newEditedResource.getDepartment());
		currentUser.setRole(newEditedResource.getRole());
		currentUser.setEmail(newEditedResource.getEmail());
		currentUser.setPhoneNumber(newEditedResource.getPhoneNumber());

	}
	/**
	 * Updates the resource in the database and disables update and delete button, also disables the  form
	 * @param event
	 */
	@FXML
	void onUpdateResourceClick(ActionEvent event) {
		Users newEditedResource;
		try {
			if(validateResource()){
				ResourceService resourceService = new ResourceServiceImpl();
				newEditedResource = getEditedResource();
				boolean result = resourceService.updateAndAddResource(newEditedResource, loggedInUser);
				if(result){
					updateTableRecord(newEditedResource);
				} else {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Event Manager");
					alert.setHeaderText("Adding existing user");
					alert.setContentText("Message: you might have this resource already added");
					alert.showAndWait();
				}
				updateResourceButton.setDisable(true);
				resourceDepartmentComboBox.setDisable(false);
				resourceGridPane.setDisable(true);
				clearResource();
			}
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Update Location");
			alert.setHeaderText("Oopps, error updating location!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}

	/**
	 * Loads the departments and roles into combo box.
	 * Initializes the tableview columns. Registers key released listeners.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		DepartmentService departmentService;
		ObservableList<Department> deptList;
		RolesService rolesService;
		ObservableList<Roles> rolesList;
		try {
			departmentService = new DepartmentServiceImpl();
			deptList = FXCollections.observableArrayList();
			rolesService = new RolesServiceImpl();
			rolesList = FXCollections.observableArrayList();
			deptList.addAll(departmentService.viewAllDepartment());
			resourceDepartmentComboBox.setItems(deptList);
			rolesList.addAll(rolesService.viewAllRoles());
			resourceRoleComboBox.setItems(rolesList);

			resourceIDTableColumn.setCellValueFactory(cellData -> cellData.getValue().userIDProperty().asObject());
			resourceFirstNameTableColumn.setCellValueFactory(cellData -> cellData.getValue().userFirstNameProperty());
			resourceLastNameTableColumn.setCellValueFactory(cellData -> cellData.getValue().userLastNameProperty());
			resourceDepartmentTableColumn.setCellValueFactory(cellData -> cellData.getValue().userDepartmentProperty());
			resourceEmailTableColumn.setCellValueFactory(cellData -> cellData.getValue().userEmailProperty());
			resourcePhoneTableColumn.setCellValueFactory(cellData -> cellData.getValue().userPhoneNumberProperty());
			resourceRoleTableColumn.setCellValueFactory(cellData -> cellData.getValue().userRoleProperty());
			UserService userService = new UserServiceImpl();
			registerListeners();
			List<Users> userList = userService.viewAllUser();
			if(userList != null)
				userDataList.addAll(userList);
			//resourceTableView.setItems(userDataList);
			FilteredList<Users> resourceFilteredData = new FilteredList<>(userDataList, e -> true);
			resourceSearchTextField.setOnKeyReleased(e -> {
				resourceSearchTextField.textProperty().addListener((observableValue, oldValue, newValue) -> {
					resourceFilteredData.setPredicate((Predicate<? super Users>) res -> {
						String lowerCaseFilter = newValue.toLowerCase();
						if(newValue == null || newValue.isEmpty())
							return true;
						else if(String.valueOf(res.getId()).contains(lowerCaseFilter))
							return true;
						else if(res.getFirstName().toLowerCase().contains(lowerCaseFilter))
							return true;
						else if(res.getLastName().toLowerCase().contains(lowerCaseFilter))
							return true;
						else if(res.getDepartment().getDepartmentName().toLowerCase().contains(lowerCaseFilter))
							return true;
						else if(res.getEmail().toLowerCase().contains(lowerCaseFilter))
							return true;
						else if(res.getPhoneNumber().toLowerCase().contains(lowerCaseFilter))
							return true;
						return false;
					});
				});
			});
			resourceTableView.setItems(resourceFilteredData);
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Resource");
			alert.setHeaderText("Oopps, error loading resources!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}

	 /**
	  * Displays all the resources of this particular user.
	 * @param event
	 */
	@FXML
	 void onMyResourcesClick(ActionEvent event) {
		 try {
			 FXMLLoader loader = new FXMLLoader();
			 loader.setLocation(getClass().getClassLoader().getResource("\\com\\iit\\eventmanagment\\view\\my_resources.fxml"));
			 Stage organiserStage = new Stage();
			 organiserStage.setTitle("My Resources");
			 MyResourceController myResourceController = new MyResourceController(loggedInUser);
			 loader.setController(myResourceController);
			 Parent root;
			 root = loader.load();
			 organiserStage.setScene(new Scene(root));
			 organiserStage.show();
		 } catch (IOException e) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Resource");
				alert.setHeaderText("Oopps, error loading resources!");
				alert.setContentText("Message: "+e.getMessage());
				alert.showAndWait();
				e.printStackTrace();
			 e.printStackTrace();
		 }
	 }

}
