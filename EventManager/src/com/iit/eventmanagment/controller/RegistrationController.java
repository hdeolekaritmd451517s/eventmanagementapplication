package com.iit.eventmanagment.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import com.iit.eventmanagment.model.Department;
import com.iit.eventmanagment.model.Roles;
import com.iit.eventmanagment.model.Users;
import com.iit.eventmanagment.service.DepartmentService;
import com.iit.eventmanagment.service.RolesService;
import com.iit.eventmanagment.service.UserService;
import com.iit.eventmanagment.service.impl.DepartmentServiceImpl;
import com.iit.eventmanagment.service.impl.RolesServiceImpl;
import com.iit.eventmanagment.service.impl.UserServiceImpl;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.RequiredFieldValidator;

import application.utils.InputValidator;
import application.utils.MessageType;
import application.utils.SendHTMLEmail;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import javafx.util.Duration;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

/**
 * This contollers handles user registration. It validates the input from the user.
  * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class RegistrationController implements Initializable {

	public Stage registrationStage;

	@FXML
	private JFXTextField fnameTextField;

	@FXML
	private JFXTextField lnameTextField;

	@FXML
	private JFXTextField emailTextField;

	@FXML
	private JFXTextField phoneNoTextField;

	@FXML
	private JFXPasswordField passwordField;

	@FXML
	private JFXPasswordField confirmPasswordField;

	@FXML
	private JFXComboBox<Department> deptComboBox;

	@FXML
	private JFXComboBox<Roles> roleComboBox;

	@FXML
	private JFXButton registerButton;

	@FXML
	private JFXButton cancelButton;
	RequiredFieldValidator fNameValidator = new RequiredFieldValidator();
	RequiredFieldValidator lNameValidator = new RequiredFieldValidator();
	RequiredFieldValidator emailValidator = new RequiredFieldValidator();
	RequiredFieldValidator phoneNoValidator = new RequiredFieldValidator();
	RequiredFieldValidator pwdValidator = new RequiredFieldValidator();
	RequiredFieldValidator confirmPwdValidator = new RequiredFieldValidator();

	@FXML
	private LoginController loginController;
	DepartmentService departmentService;
	ObservableList<Department> deptList;
	RolesService rolesService;
	ObservableList<Roles> rolesList;
	FXMLLoader loader ;
	boolean isValid = false;

	public RegistrationController() throws Exception{
	}

	/**
	 * Initializes department and role combo box and register required input field listeners.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		departmentService = new DepartmentServiceImpl();
		deptList = FXCollections.observableArrayList();
		rolesService = new RolesServiceImpl();
		rolesList = FXCollections.observableArrayList();
		registerListeners();
		try {
			deptList.addAll(departmentService.viewAllDepartment());
			deptComboBox.setItems(deptList);
			rolesList.addAll(rolesService.viewAllRoles());
			roleComboBox.setItems(rolesList);
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("ERROR");
			alert.setHeaderText("Oopps, error while registration!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}

	/**
	 * This method registers listeners with the input fields.
	 */
	private void registerListeners() {
		fnameTextField.getValidators().add(fNameValidator);
		lnameTextField.getValidators().add(lNameValidator);
		emailTextField.getValidators().add(emailValidator);
		phoneNoTextField.getValidators().add(phoneNoValidator);
		passwordField.getValidators().add(pwdValidator);
		confirmPasswordField.getValidators().add(confirmPwdValidator);

		fnameTextField.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(!newValue){
					fNameValidator.setMessage("field is required");
					fnameTextField.validate();
				}
			}
		});

		lnameTextField.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(!newValue){
					lNameValidator.setMessage("field is required");
					lnameTextField.validate();
				}
			}
		});

		emailTextField.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(!newValue){
					emailValidator.setMessage("email is required");
					emailTextField.validate();
				}
			}
		});

		phoneNoTextField.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				System.out.println(oldValue+ "   :   "+newValue);
				if(!newValue){
					phoneNoValidator.setMessage("phone number is required");
					phoneNoTextField.validate();
				}
			}
		});

		passwordField.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(!newValue){
					pwdValidator.setMessage("passwordField is required");
					passwordField.validate();
				}
			}
		});

		confirmPasswordField.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(!newValue){
					confirmPwdValidator.setMessage("confirm passwordField is required");
					confirmPasswordField.validate();
				}
			}
		});
	}

	public LoginController getLogin() {
		return loginController;
	}

	public void setLogin(LoginController loginController) {
		this.loginController = loginController;
	}

	public Stage getRegistrationStage() {
		return registrationStage;
	}

	public void setRegistrationStage(Stage registrationStage) {
		this.registrationStage = registrationStage;
	}

	/**
	 * This action method is called when user clicks register button. It handles input validations.
	 * @param event
	 * @throws Exception
	 */
	@FXML
	void onRegisterAction(ActionEvent event) throws Exception {
		Alert alert1 = new Alert(AlertType.INFORMATION);
		alert1.setTitle("Event Manager");
		alert1.setHeaderText("Event Manager");
		// checks whether user has filled up all the required fields
		if(!(fnameTextField.validate()&&lnameTextField.validate()&&lnameTextField.validate()&&phoneNoTextField.validate()
				&&passwordField.validate()&&confirmPasswordField.validate())){
			alert1.setHeaderText("Event Manager");
			alert1.setContentText("Please fill the required fields.");
			alert1.showAndWait();
			// checks if department and role is selected
		}else if(deptComboBox.getSelectionModel().isEmpty()||roleComboBox.getSelectionModel().isEmpty()){
			alert1.setContentText("Pleasese select dept/role");
			alert1.showAndWait();
		}else{
			boolean isValid = true;
			// validates all the input fields, validations could have been more aggressive,
			//however, for now, we want to show case this feature
			if(!InputValidator.validateName(fnameTextField.getText())){
				 isValid = false;
				alert1.setContentText("Please enter valid firstname.");
				alert1.showAndWait();
			}else if(!InputValidator.validateName(lnameTextField.getText())){
				 isValid = false;
				alert1.setContentText("Please enter valid lastname.");
				alert1.showAndWait();
			}else if(!InputValidator.validateEmail(emailTextField.getText())){
				 isValid = false;
				alert1.setContentText("Please enter valid email.");
				alert1.showAndWait();
			}else if(!InputValidator.validatePhoneNumber(phoneNoTextField.getText())){
				 isValid = false;
				alert1.setContentText("Please enter valid phone number.");
				alert1.showAndWait();
			}else if(!InputValidator.validatePassword(passwordField.getText())){
				 isValid = false;
				 alert1.setHeaderText("Password Policy");
				alert1.setContentText("At least one lowercase letter.\n"
										+"At least one digit i.e. 0-9.\n"
										+"At least one special character.\n"
										+"At least one capital letter.\n"
										+"Length of password from minimum 6 letters to maximum 16 letters.");
				alert1.showAndWait();
			}else if((InputValidator.validateConfirmPassword(passwordField.getText(), confirmPasswordField.getText()) == 1)){
				 isValid = false;
				 alert1.setHeaderText("Password Policy");
				 alert1.setContentText("This matches the presence of at least one lowercase letter.\n"
							+"This matches the presence of at least one digit i.e. 0-9.\n"
							+"This matches the presence of at least one special character.\n"
							+"This matches the presence of at least one capital letter.\n"
							+"This limits the length of password from minimum 6 letters to maximum 16 letters.");
				alert1.showAndWait();
			}else if((InputValidator.validateConfirmPassword(passwordField.getText(), confirmPasswordField.getText()) == 2)){
				 isValid = false;
				alert1.setContentText("passwords donot match");
				alert1.showAndWait();
			}
			// if validations are successful,  add the user to the system.
			if(isValid){
				Users user = new Users();
				user.setFirstName(fnameTextField.getText());
				user.setLastName(lnameTextField.getText());
				user.setDepartment(deptComboBox.getValue());
				user.setRole(roleComboBox.getValue());
				user.setEmail(emailTextField.getText());
				user.setPhoneNumber(phoneNoTextField.getText());
				user.setPassword(passwordField.getText());
				UserService userService = new UserServiceImpl();
				try {
					// if added successfully, send email notification
					if(userService.addUser(user)){
						SendHTMLEmail sendEmail = new SendHTMLEmail();
						String message = sendEmail.getMessage(user, MessageType.USER_SIGNUP_MESSAGE);
						sendEmail.sendMessage(user.getEmail(),message, MessageType.USER_SIGNUP_MESSAGE);
						// notify user
						TrayNotification tray = new TrayNotification();
			            tray.setNotificationType(NotificationType.SUCCESS);
			            tray.setTitle("Registration Successfull");
			            tray.setMessage("Welcome onboard!");
			            tray.setAnimationType(AnimationType.SLIDE);
			            tray.showAndDismiss(Duration.millis(3000));
					} else {
						Alert alert = new Alert(AlertType.ERROR);
						alert.setTitle("ERROR");
						alert.setHeaderText("Error while adding user");
						alert.setContentText("Registration Failed!!");
						alert.showAndWait();
					}
				} catch (Exception e) {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("ERROR");
					alert.setHeaderText("Error while adding user");
					alert.setContentText(e.getMessage()+"!");
					alert.showAndWait();
					e.printStackTrace();
				}
				// hide registration screen and show back login screen.
				((Node)event.getSource()).getScene().getWindow().hide();
				loginController.displayLoginWindow();;
			}
		}
	}

	/**
	 * Shows login screen.
	 * @param event
	 * @throws IOException
	 */
	@FXML
	void onCancelAction(ActionEvent event) throws IOException {
		System.out.println("New Client Signed Up");
		// store details in database, if successful
		((Node)event.getSource()).getScene().getWindow().hide();
		loginController.displayLoginWindow();
	}


	/**
	 * Close registration window.
	 */
	public void closeRegistrationWindow(){
		registrationStage.close();
	}
}