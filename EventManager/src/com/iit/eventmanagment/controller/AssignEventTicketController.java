package com.iit.eventmanagment.controller;

import java.net.URL;
import java.util.List;
import java.util.ListIterator;
import java.util.ResourceBundle;
import org.controlsfx.control.CheckComboBox;
import com.iit.eventmanagment.model.Event;
import com.iit.eventmanagment.model.Ticket;
import com.iit.eventmanagment.model.Users;
import com.iit.eventmanagment.service.EventService;
import com.iit.eventmanagment.service.ResourceService;
import com.iit.eventmanagment.service.TicketService;
import com.iit.eventmanagment.service.impl.EventServiceImpl;
import com.iit.eventmanagment.service.impl.ResourceServiceImpl;
import com.iit.eventmanagment.service.impl.TicketServiceImpl;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.skins.JFXComboBoxListViewSkin;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * This is controller allows user to assign and view resource to the events. Before assigning resource to the event,
 * user must first add/create  his resources. This controller can be called from admin as well as organizer.
 *  * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class AssignEventTicketController implements Initializable{
    @FXML
    private JFXButton unAssignTicketButton;

    @FXML
    private JFXButton assignTicketButton;

    @FXML
    private VBox assignTicketVBox;

    @FXML
    private JFXComboBox<Event> selectEventComboBox;

    @FXML
    private CheckComboBox<Ticket> selectTicketComboBox;

    @FXML
    private TreeView<Object> eventTicketTreeView;

    @FXML
    private  TreeItem<Object> eventTicketRootNode;

    List<Event> eventList;
    private ObservableList<Event> selectEventComboBoxDataList = FXCollections.observableArrayList();
    private ObservableList<Ticket>  selectTicketComboBoxDataList = FXCollections.observableArrayList();
	private Stage assignTicketStage;
    Users loggedInUser;


    public AssignEventTicketController(Stage assignTicketStage, Users loggedInUser) {
		setAssignTicketStage(assignTicketStage);
		setLoggedInUser(loggedInUser);
	}

	public Stage getAssignTicketStage() {
		return assignTicketStage;
	}

	public void setAssignTicketStage(Stage assignTicketStage) {
		this.assignTicketStage = assignTicketStage;
	}

	public Users getLoggedInUser() {
		return loggedInUser;
	}

	public void setLoggedInUser(Users loggedInUser) {
		this.loggedInUser = loggedInUser;
	}

	/**
	 * This method enables user to assign ticket to the event
	 * @param e
	 */
	@FXML
    void onAssignTicketClick(ActionEvent e) {
		assignTicketVBox.setDisable(false);
	}


	/**
	 * This method assigns new ticket to the event.
	 * @param e
	 */
	@FXML
    void onAssignClick(ActionEvent e) {
    	try {
    		Event event = selectEventComboBox.getSelectionModel().getSelectedItem();
    		if(event!=null){
    			ObservableList<Ticket> ticket = selectTicketComboBox.getCheckModel().getCheckedItems();
    			if(ticket != null){
    				event.setEventTicket(ticket);
    				TicketService ticketService = new TicketServiceImpl();
    				boolean result = ticketService.assignEventTicketsById(loggedInUser, event);
    				if(result){
    					displayTreeView(event);
    					selectEventComboBox.getSelectionModel().clearSelection();
    					selectTicketComboBox.getCheckModel().clearChecks();
    					assignTicketVBox.setDisable(true);
    				}
    			}else{
    				Alert alert = new Alert(AlertType.INFORMATION);
    				alert.setTitle("Event Manager");
    				alert.setHeaderText("Assign Event");
    				alert.setContentText("Message: Please select the resources");
    				alert.showAndWait();
    			}
    		}else{
    			Alert alert = new Alert(AlertType.INFORMATION);
    			alert.setTitle("Event Manager");
    			alert.setHeaderText("Assign Event");
    			alert.setContentText("Message: Please select the event");
    			alert.showAndWait();
    		}
    	} catch (Exception e1) {
    		Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Event Manager");
			alert.setHeaderText("Assign Event");
			alert.setContentText("Message: Error occured while assigning resource"+e1.getMessage());
			alert.showAndWait();
    		e1.printStackTrace();
    	}
    }

	/**
	 * This method displays the table with events and its assigned tickets.
	 * For the scope of this project, we just show newly assigned tickets.
	 */
	private void displayTreeView(Event e) {
		boolean isEventMatched  = false;
		if(eventTicketRootNode.getChildren().isEmpty()){
			isEventMatched  = true;
			TreeItem<Object> newParentTreeItem = new TreeItem<Object>(e);
			eventTicketRootNode.getChildren().add(newParentTreeItem);
			ObservableList<TreeItem<Object>> events = eventTicketRootNode.getChildren();
			for (TreeItem<Object> event : events) {
				Event castedEvent = (Event)event.getValue();
				if(castedEvent.getName().equals(e.getName())){
					e.getEventTickets().forEach(r->{
						TreeItem<Object> newChildTreeItem = new TreeItem<Object>(r);
						event.getChildren().add(newChildTreeItem);
					});
				}
			}
		}else{
			ObservableList<TreeItem<Object>> events = eventTicketRootNode.getChildren();
			for (TreeItem<Object> event : events) {
				Event castedEvent = (Event)event.getValue();
				if(castedEvent.getName().equals(e.getName())){
					isEventMatched = true;
					e.getEventTickets().forEach(r->{
						TreeItem<Object> newChildTreeItem = new TreeItem<Object>(r);
						event.getChildren().add(newChildTreeItem);
					});
				}
			}
			if(!isEventMatched){
				TreeItem<Object> newParentTreeItem = new TreeItem<Object>(e);
				eventTicketRootNode.getChildren().add(newParentTreeItem);
				for (TreeItem<Object> event : events) {
					Event castedEvent = (Event)event.getValue();
					if(castedEvent.getName().equals(e.getName())){
						isEventMatched = true;
						e.getEventResources().forEach(r->{
							TreeItem<Object> newChildTreeItem = new TreeItem<Object>(r);
							event.getChildren().add(newChildTreeItem);
						});
					}
				}
			}
		}
	}

	/**
	 * This method is called on unassigned click.
	 * @param event
	 */
	@FXML
	void onUnAssignTicketClick(ActionEvent event) {
		try {
			TreeItem<Object> selectedTicketTreeItem = eventTicketTreeView.getSelectionModel().getSelectedItem();
			TreeItem<Object> selectedEventTreeItem = selectedTicketTreeItem.getParent();
			if((selectedTicketTreeItem != null) && (selectedEventTreeItem != null)){
				Event selectedEvent = (Event) selectedEventTreeItem.getValue();
				Ticket selectedTicket = (Ticket) selectedTicketTreeItem.getValue();
				TicketService ticketService = new TicketServiceImpl();
				boolean result = ticketService.unAssignEventTicketsById(loggedInUser, selectedEvent, selectedTicket);
				if(result){
					// if db operation successful, the remove the ticket node from the tree.
					ObservableList<TreeItem<Object>> l = selectedEventTreeItem.getChildren();
					ListIterator<TreeItem<Object>> it =l.listIterator();
					while (it.hasNext()) {
						Ticket ticket = (Ticket)it.next().getValue();
						if(ticket.getId() == selectedTicket.getId())
							it.remove();
					}
					unAssignTicketButton.setDisable(true);
				}else{
					Alert alert = new Alert(AlertType.WARNING);
					alert.setTitle("Event Manager");
					alert.setHeaderText("UnAssign Resource!");
					alert.setContentText("Message: No resources were unassigned!");
					alert.showAndWait();
				}
			}
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Event Manager");
			alert.setHeaderText("UnAssign Resource!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}

	/**
	 * Initializes the event and ticket combo box based on the logged in user.
	 */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		try {
			EventService eventService = new EventServiceImpl();
			eventList = eventService.viewAllEventByUserId(loggedInUser);
			selectEventComboBoxDataList.addAll(eventList);
			selectEventComboBox.setItems(selectEventComboBoxDataList);
			TicketService ticketService = new TicketServiceImpl();
			List<Ticket> ticketList = ticketService.viewAllTicketsById(loggedInUser);
			selectTicketComboBoxDataList.addAll(ticketList);
			selectTicketComboBox.getItems().addAll(selectTicketComboBoxDataList);
			eventTicketRootNode = new TreeItem<Object>(loggedInUser.getFirstName()+"'s Tickets Pool");
			eventTicketTreeView.setRoot(eventTicketRootNode);
		} catch (Exception e) {
			e.printStackTrace();
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Event Manager");
			alert.setHeaderText("Oopps, error while loading your events and resources!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
		}
	}


	public  void mouseClickOnTreeView(MouseEvent me){
			TreeItem<Object> selectedObject = eventTicketTreeView.getSelectionModel().getSelectedItem();
			if(selectedObject!=null){
				Object obj = selectedObject.getValue();
				if(obj instanceof Event){
					System.out.println("Event Node: "+((Event)obj).getName());
					unAssignTicketButton.setDisable(true);
				} else if(obj instanceof Ticket){
					System.out.println("Ticket Node: "+((Ticket)obj).getTicketName());
					unAssignTicketButton.setDisable(false);
				} else if(obj instanceof String){
					System.out.println("Root Nodet: "+((String)obj));
					unAssignTicketButton.setDisable(true);
				}
			}
	}
}
