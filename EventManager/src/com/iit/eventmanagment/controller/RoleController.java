package com.iit.eventmanagment.controller;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import com.iit.eventmanagment.model.Roles;
import com.iit.eventmanagment.service.RolesService;
import com.iit.eventmanagment.service.impl.RolesServiceImpl;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;

/**
 * This class handles adding, editing, updating and deleting of the roles. Feature only available for admin.
 * @author Harshal
 *
 */
public class RoleController implements Initializable{

	Roles currentRole;
    @FXML
    private AnchorPane roleAnchorPane;

    @FXML
    private HBox addRoleHBox;

    @FXML
    private JFXTextField addRoleTextField;


    @FXML
    private TableView<Roles> roleTableView;

    @FXML
    private TableColumn<Roles, Integer> roleIDTableColumn;

    @FXML
    private TableColumn<Roles, String> roleNameTableColumn;

    @FXML
    private JFXButton addRoleButton;

    @FXML
    private JFXButton editRoleButton;

    @FXML
    private JFXButton saveRoleButton;

    @FXML
    private JFXButton updateRoleButton;

    @FXML
    private JFXButton deleteRoleButton;

	private ObservableList<Roles> rolesDataList = FXCollections.observableArrayList();

    /**
     * Enables save button, disables update and delete button, clears the textfield.
     * @param event
     */
    @FXML
    void onAddRoleClick(ActionEvent event) {
		addRoleHBox.setDisable(false);
		saveRoleButton.setDisable(false);
		updateRoleButton.setDisable(true);
		deleteRoleButton.setDisable(true);
		clearRole();
    }

    /**
     * Clears the text field
     */
    private void clearRole() {
    	if(addRoleTextField.getText() != null)
    		addRoleTextField.clear();
	}

	/**
	 * Deletes the user from the database and removes it from the table.
	 * @param event
	 */
	@FXML
    void onDeleteRoleClick(ActionEvent event) {
		try {
			RolesService rolesService = new RolesServiceImpl();
			Roles roleTobeDeleted = getEditedRole();
			boolean result = rolesService.deleteRole(roleTobeDeleted);
			if(result){
				 int selectedIndex = roleTableView.getSelectionModel().getSelectedIndex();
				    if (selectedIndex >= 0) {
				    	roleTableView.getItems().remove(selectedIndex);
				    } else {
				        // Nothing selected.
				        Alert alert = new Alert(AlertType.WARNING);
						alert.setTitle("Delete Role");
						alert.setHeaderText("Oopps, error deleting role!");
						alert.setContentText("Please select the role.");
						alert.showAndWait();
				    }
			}
			updateRoleButton.setDisable(true);
			deleteRoleButton.setDisable(true);
			addRoleHBox.setDisable(true);
			clearRole();
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Update Location");
			alert.setHeaderText("Oopps, error updating location!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
    }

    /**
     * Creates new role object to be updated
     * @return role object with new edited values
     */
    private Roles getEditedRole() {
    	Roles newEditedRole = new Roles(currentRole.getId(), addRoleTextField.getText());
		return newEditedRole;
	}

	/**
	 * Allows user to edit the role, enables save button and disable update and delete button.
	 * @param event
	 */
	@FXML
    void onEditRoleClick(ActionEvent event) {
		currentRole = roleTableView.getSelectionModel().getSelectedItem();
		if(editRole(currentRole)){
			saveRoleButton.setDisable(true);
			updateRoleButton.setDisable(false);
			deleteRoleButton.setDisable(false);
		}
    }

    /**
     * Edits currently selected role
     * @param currentRole
     * @return
     */
    private boolean editRole(Roles currentRole) {
    	boolean result = false;
		if(currentRole != null){
			addRoleHBox.setDisable(false);
			addRoleTextField.setText(currentRole.getRoleName());
			result = true;
		} else{
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("ROLE");
			alert.setHeaderText("EDIT ROLE");
			alert.setContentText("Message: Please select the field first!");
			alert.showAndWait();
			result = false;
		}
		return result;
	}

	/**
	 * Saves the role into database and add it to the tableview
	 * @param event
	 */
	@FXML
    void onSaveRoleClick(ActionEvent event) {
		try {
			RolesService roleService = new RolesServiceImpl();
			Roles newRole = getNewRoles();
			if(newRole != null){
				newRole = roleService.addRole(newRole);
				rolesDataList.add(newRole);
				clearRole();
			} else{
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Role");
				alert.setHeaderText("Add Role");
				alert.setContentText("Message: Role could not be saved!");
				alert.showAndWait();
			}
			addRoleHBox.setDisable(true);
			saveRoleButton.setDisable(true);
			updateRoleButton.setDisable(true);
			deleteRoleButton.setDisable(true);
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("ADD Role");
			alert.setHeaderText("Oopps, error adding roles!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
    }

    /**
     * Creates the role object with new value
     * @return newly created role
     */
    private Roles getNewRoles() {
    	Roles newRole = null;
    	if(addRoleTextField.getText()!=null){
    		newRole = new Roles(addRoleTextField.getText());
    		return newRole;
    	}
    	return null;
	}

	/**
	 * Updates the role into the database and the tableview,
	 * and clears and disables the input form and disables update and delete buttons.
	 * @param event
	 */
	@FXML
    void onUpdateRoleClick(ActionEvent event) {
    	Roles newEditedRole;
		try {
			RolesService roleService = new RolesServiceImpl();
			newEditedRole = getEditedRole();
			boolean result = roleService.updateRole(newEditedRole);
			if(result){
				updateTableRecord(newEditedRole);
			} else {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Role");
				alert.setHeaderText("Update Role");
				alert.setContentText("Message: No records were updated");
				alert.showAndWait();
			}
			updateRoleButton.setDisable(true);
			deleteRoleButton.setDisable(true);
			addRoleHBox.setDisable(true);
			clearRole();
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Update Role");
			alert.setHeaderText("Oopps, error updating role!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
    }

	/**
	 * Updates the tableview.
	 * @param newEditedRole
	 */
	private void updateTableRecord(Roles newEditedRole) {
		currentRole.setId(newEditedRole.getId());
		currentRole.setRoleName(newEditedRole.getRoleName());
	}


	/**
	 * Initializes the roles table view.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			roleIDTableColumn.setCellValueFactory(cellData -> cellData.getValue().roleIdProperty().asObject());
			roleNameTableColumn.setCellValueFactory(cellData -> cellData.getValue().roleNameProperty());
			RolesService roleService = new RolesServiceImpl();
			List<Roles> roleList = roleService.viewAllRoles();
			rolesDataList.addAll(roleList);
			roleTableView.setItems(rolesDataList);
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Event Manager");
			alert.setHeaderText("Oopps, error loading roles!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}
}
