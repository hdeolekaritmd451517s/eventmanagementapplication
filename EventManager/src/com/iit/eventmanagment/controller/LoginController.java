package com.iit.eventmanagment.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.iit.eventmanagment.model.Login;
import com.iit.eventmanagment.model.Roles;
import com.iit.eventmanagment.model.Users;
import com.iit.eventmanagment.service.LoginService;
import com.iit.eventmanagment.service.RolesService;
import com.iit.eventmanagment.service.impl.LoginServiceImpl;
import com.iit.eventmanagment.service.impl.RolesServiceImpl;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.RequiredFieldValidator;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.util.Duration;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

/**
 * This class handles the user login. I also allows the user to login.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class LoginController implements Initializable{

	private Stage primaryStage;
	@FXML
	private JFXTextField userName;

	@FXML
	private JFXPasswordField password;

	@FXML
	private Label loginMessage;

	@FXML
	private JFXButton loginButton;

	@FXML
	private JFXButton registerButton;

	@FXML
	private ImageView loginImageView;

	@FXML
	private JFXComboBox<Roles> userRoleComboBox;

	private boolean isLoginValid = false;

	public LoginController() {
		System.out.println("login controller called....");
	}

	public Stage getPrimaryStage() {
		return primaryStage;
	}

	public void setPrimaryStage(Stage primaryStage) {
		this.primaryStage = primaryStage;
	}

	/**
	 * Validates user credentials, displays window based on user role e.g. admin, organizer and student.
	 * @param event
	 */
	@FXML
	void onLoginAction(ActionEvent event) {
		String uname = "";
		String pwd = "";
		int roleId = 0;
		// checks if required fields are not empty and role is selected by the user.
		if(!(userName.validate() && password.validate()&&!userRoleComboBox.getSelectionModel().isEmpty())){
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Event Manager");
			alert.setHeaderText("Login");
			alert.setContentText("Message: Please enter your credentials");
			alert.showAndWait();
		}else{
			uname = userName.getText();
			pwd = password.getText();
			roleId = userRoleComboBox.getSelectionModel().getSelectedItem().getId();
			Login login = new Login();
			login.setUsername(uname);
			login.setPassword(pwd);
			login.setRoleId(roleId);
			LoginService loginService = new LoginServiceImpl();
			try {
				Users loggedInUser = loginService.verifyUser(login);
				System.out.println("User:"+loggedInUser);
				try {
					// if user is valid
					if(loggedInUser!=null){
						//  if user role is matches with the one selected, the display appropriate window.
						if(loggedInUser.getRole().getId() == roleId){
							System.out.println("LoggedIn user : "+loggedInUser);
							switch (loggedInUser.getRole().getId()) {
							case 1:
								//display admin view
								displayAdminWindow(loggedInUser);
								((Node)event.getSource()).getScene().getWindow().hide();
								break;
							case 2:
								// display organizer view
								displayOrganiserWindow(loggedInUser);
								((Node)event.getSource()).getScene().getWindow().hide();
								break;
							case 3:
								displayStudentWindow(loggedInUser);
								((Node)event.getSource()).getScene().getWindow().hide();
								break;
							default:
								// user not found
								Alert alert = new Alert(AlertType.ERROR);
								alert.setTitle("ERROR");
								alert.setHeaderText("Login Failed");
								alert.setContentText("Oopps, user not found");
								alert.showAndWait();
								break;
							}
						}else{
							// wrong credentials
							TrayNotification tray = new TrayNotification();
				            tray.setNotificationType(NotificationType.ERROR);
				            tray.setTitle("Login Failure");
				            tray.setMessage("Please enter valid credential");
				            tray.setAnimationType(AnimationType.SLIDE);
				            tray.showAndDismiss(Duration.millis(1500));
						}
					}
				} catch (Exception e) {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("ERROR");
					alert.setHeaderText("System Error");
					alert.setContentText("Oopps, system failure!");
					alert.showAndWait();
					e.printStackTrace();
				}
			} catch (Exception e) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("ERROR");
				alert.setHeaderText("Login Failed");
				alert.setContentText("Oopps, error during login. Please try again!");
				alert.showAndWait();
				e.printStackTrace();
			}
		}

	}

	/**
	 * This action method allows user to be registered into the system by displaying registration form
	 * @param event
	 */
	@FXML
	public void onRegisterAction(ActionEvent event){
		try {
			System.out.println("[SIGN UP BUTTON CLICKED]");
			((Node)event.getSource()).getScene().getWindow().hide();
			displayRegistrationWindow();
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("ERROR");
			alert.setHeaderText("Oopps, error while registration!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}

	public void displayLoginWindow() throws IOException {
		this.primaryStage.show();
	}

	/**
	 * Displays registration form
	 * @throws Exception if view could not be loaded
	 */
	public void displayRegistrationWindow() throws Exception {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getClassLoader().getResource("\\com\\iit\\eventmanagment\\view\\add_user.fxml"));
		Parent root = loader.load();
		//Platform.runLater( () -> root.requestFocus() );
		Stage registrationStage = new Stage();
		registrationStage.setTitle("Register");
		registrationStage.setScene(new Scene(root));
		RegistrationController registrationController = loader.<RegistrationController>getController();
		registrationController.setRegistrationStage(registrationStage);
		registrationController.setLogin(this);
		registrationStage.show();
	}

	/**
	 * Loads and displays admin's view.
	 * @param loggedInUser to load into admin controller
	 * @throws IOException if view could not be loaded
	 */
	public void displayAdminWindow(Users loggedInUser) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getClassLoader().getResource("\\com\\iit\\eventmanagment\\view\\admin.fxml"));
		Stage adminStage = new Stage();
		adminStage.setTitle("Admin Login");
		AdminController adminController = new AdminController(adminStage, loggedInUser);
		loader.setController(adminController);
		Parent root = loader.load();
		adminStage.setScene(new Scene(root));
		adminController.setLoginController(this);
		adminStage.setResizable(false);
		adminStage.show();
	}

	/**
	 * Loads and displays the organizer's view.
	 * @param loggedInUser
	 * @throws IOException if view could not be loaded properly.
	 */
	public void displayOrganiserWindow(Users loggedInUser) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getClassLoader().getResource("\\com\\iit\\eventmanagment\\view\\manager.fxml"));
		Stage organiserStage = new Stage();
		organiserStage.setTitle("Organiser Login");
		OrganiserController organiserController = new OrganiserController(organiserStage, loggedInUser);
		loader.setController(organiserController);
		Parent root = loader.load();
		organiserStage.setScene(new Scene(root));
		organiserController.setLoginController(this);
		organiserStage.show();
	}

	/**
	 * Loads and displays the students window.
	 * @param loggedInUser
	 * @throws IOException if view could not be loaded
	 */
	private void displayStudentWindow(Users loggedInUser) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getClassLoader().getResource("\\com\\iit\\eventmanagment\\view\\student.fxml"));
		Stage studentStage = new Stage();
		studentStage.setTitle("Student Login");
		StudentController studentController = new StudentController(studentStage, loggedInUser);
		loader.setController(studentController);
		Parent root = loader.load();
		studentStage.setScene(new Scene(root));
		studentController.setLoginController(this);
		studentStage.show();
	}

	/**
	 * Loads the login page role combo box with all the roles and add required field listeners.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			System.out.println("login window initialized...");
			RolesService rolesService = new RolesServiceImpl();
			ObservableList<Roles> rolesList = FXCollections.observableArrayList();
			rolesList.addAll(rolesService.viewAllRoles());
			userRoleComboBox.setItems(rolesList);
			RequiredFieldValidator userNameValidator = new RequiredFieldValidator();
			RequiredFieldValidator passwordValidator = new RequiredFieldValidator();
			userName.getValidators().add(userNameValidator);
			userName.focusedProperty().addListener(new ChangeListener<Boolean>() {
				@Override
				public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
					if(!newValue){
							userNameValidator.setMessage("username is required");
							userName.validate();
					}
				}
			});

			password.getValidators().add(passwordValidator);
			password.focusedProperty().addListener(new ChangeListener<Boolean>() {
				@Override
				public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
					if(!newValue){
							passwordValidator.setMessage("password is  required");
							password.validate();
					}
				}
			});
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("ERROR");
			alert.setHeaderText("Oopps, error while loading roles.\nPlease contact system administrator!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}
}
