package com.iit.eventmanagment.controller;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import com.iit.eventmanagment.model.Department;
import com.iit.eventmanagment.model.Event;
import com.iit.eventmanagment.model.Roles;
import com.iit.eventmanagment.model.Users;
import com.iit.eventmanagment.service.DepartmentService;
import com.iit.eventmanagment.service.EventService;
import com.iit.eventmanagment.service.ResourceService;
import com.iit.eventmanagment.service.RolesService;
import com.iit.eventmanagment.service.UserService;
import com.iit.eventmanagment.service.impl.DepartmentServiceImpl;
import com.iit.eventmanagment.service.impl.EventServiceImpl;
import com.iit.eventmanagment.service.impl.ResourceServiceImpl;
import com.iit.eventmanagment.service.impl.RolesServiceImpl;
import com.iit.eventmanagment.service.impl.UserServiceImpl;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;

/**
 * This class enables user to view his resources. Additionally, delete the resources.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class MyResourceController implements Initializable {
	Users currentResource;
	@FXML
	private TableView<Users> resourceTableView;

	@FXML
	private TableColumn<Users, Integer> resourceIDTableColumn;

	@FXML
	private TableColumn<Users, String> resourceFirstNameTableColumn;

	@FXML
	private TableColumn<Users, String> resourceLastNameTableColumn;

	@FXML
	private TableColumn<Users, Department> resourceDepartmentTableColumn;

	@FXML
	private TableColumn<Users, String> resourceEmailTableColumn;

	@FXML
	private TableColumn<Users, String> resourcePhoneTableColumn;

	@FXML
	private TableColumn<Users,Roles> resourceRoleTableColumn;

	private ObservableList<Users> userDataList = FXCollections.observableArrayList();

	@FXML
	private Button deleteMyResourceButton;

	private Users loggedInUser;


	public MyResourceController(Users loggedInUser) {
		setLoggedInUser(loggedInUser);
	}

	public Users getLoggedInUser() {
		return loggedInUser;
	}

	public void setLoggedInUser(Users loggedInUser) {
		this.loggedInUser = loggedInUser;
	}


	/**
	 * Deletes the resources under this user(Organizer). Removes it from the table
	 * @param event
	 */
	@FXML
	void onDeleteResourceClick(ActionEvent event) {
		int selectedItemIndex;
		try{
			currentResource = resourceTableView.getSelectionModel().getSelectedItem();
			selectedItemIndex =  resourceTableView.getSelectionModel().getSelectedIndex();
			// if resource is selected
			if(currentResource != null){
				ResourceService resourceService = new ResourceServiceImpl();
				// delete it
				boolean result =  resourceService.deleteResourceById(currentResource);
				// if delete successful,
				if(result){
					// remove it from the tableview
					if (selectedItemIndex >= 0) {
						resourceTableView.getItems().remove(selectedItemIndex);
					} else {
						// Nothing selected.
						Alert alert = new Alert(AlertType.WARNING);
						alert.setTitle("Delete Location");
						alert.setHeaderText("Oopps, error deleting location!");
						alert.setContentText("Please select the location.");
						alert.showAndWait();
					}
				}
			}
		}catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Event Manager");
			alert.setHeaderText("Delete Resource:");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}

	/**
	 * Initializes the tableview withe the organizer's resources.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			resourceIDTableColumn.setCellValueFactory(cellData -> cellData.getValue().userIDProperty().asObject());
			resourceFirstNameTableColumn.setCellValueFactory(cellData -> cellData.getValue().userFirstNameProperty());
			resourceLastNameTableColumn.setCellValueFactory(cellData -> cellData.getValue().userLastNameProperty());
			resourceDepartmentTableColumn.setCellValueFactory(cellData -> cellData.getValue().userDepartmentProperty());
			resourceEmailTableColumn.setCellValueFactory(cellData -> cellData.getValue().userEmailProperty());
			resourcePhoneTableColumn.setCellValueFactory(cellData -> cellData.getValue().userPhoneNumberProperty());
			resourceRoleTableColumn.setCellValueFactory(cellData -> cellData.getValue().userRoleProperty());
			ResourceService resourceService = new ResourceServiceImpl();
			List<Users> userList = resourceService.viewAllResourcesById(loggedInUser);
			if(userList!=null){
				userDataList.addAll(userList);
				resourceTableView.setItems(userDataList);
			}
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("My Resource");
			alert.setHeaderText("Oopps, error loading you resources!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}
}
