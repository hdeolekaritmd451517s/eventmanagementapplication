package com.iit.eventmanagment.controller;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.List;
import java.util.ResourceBundle;
import com.iit.eventmanagment.model.Event;
import com.iit.eventmanagment.model.Users;
import com.iit.eventmanagment.service.EventService;
import com.iit.eventmanagment.service.impl.EventServiceImpl;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;

import application.MainController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

/**
 * This class is the subclass of MainController. It is a controller for the administrator role.
 * It handles admin's menubar actions. Based on the action called, it loads the fxml file
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 *
 */
public class AdminController extends MainController implements Initializable{

    @FXML
    private AnchorPane adminAnchorPane;

    @FXML
    private MenuItem adminProfilePanelMenuItem;

    @FXML
    private MenuItem adminRolePanelMenuItem;

    @FXML
    private MenuItem adminLocationPanelMenuItem;

    @FXML
    private MenuItem adminResourcePanelMenuItem;

    @FXML
    private MenuItem adminAssignResourceMenuItem;

    @FXML
    private MenuItem adminTicketPanelMenuItem;

    @FXML
    private MenuItem departmentPanelMenuItem;

    @FXML
    private MenuItem eventRegsterMenuItem;

	/**
	 * Constructor sets the admin stage and logged user.
	 * @param adminStage adminStage to be set
	 * @param loggedInUser loggedInUser
	 */
	public AdminController(Stage adminStage, Users loggedInUser) {
		super(adminStage, loggedInUser);
	}

	/**
	 * This method is called on location panel click.
	 * @param event
	 */
	@FXML
	void onLocationPanelClick(ActionEvent event) {
		try {
    		FXMLLoader loader = new FXMLLoader();
        	loader.setLocation(getClass().getClassLoader().getResource("\\com\\iit\\eventmanagment\\view\\location.fxml"));
			loader.load();
			Parent root = loader.getRoot();
			Stage myLocationStage = new Stage();
			myLocationStage.setTitle("Location Panel");
			myLocationStage.setScene(new Scene(root));
			LocationController locationController = loader.<LocationController>getController();
			myLocationStage.show();
		} catch (IOException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("My Profle");
			alert.setHeaderText("Oopps, View Profile!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}

	/**
	 * This method is called on resource panel click.
	 * @param event
	 */
	@FXML
	void onResourcePanelClick(ActionEvent event) {
		try {
    		FXMLLoader loader = new FXMLLoader();
        	loader.setLocation(getClass().getClassLoader().getResource("\\com\\iit\\eventmanagment\\view\\resources.fxml"));
			loader.load();
			Parent root = loader.getRoot();
			Stage resourcePanelStage = new Stage();
			resourcePanelStage.setTitle("Resource Panel");
			resourcePanelStage.setScene(new Scene(root));
			ResourceController resourceController = loader.<ResourceController>getController();
			resourceController.setLoggedInUser(loggedInUser);
			resourcePanelStage.show();
		} catch (IOException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("My Profle");
			alert.setHeaderText("Oopps, View Profile!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}

	/**
	 * This method is called on register panel click.
	 * @param event
	 */
	@FXML
	void onEventRegisterClick(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getClassLoader().getResource("\\com\\iit\\eventmanagment\\view\\student.fxml"));
			Stage studentStage = new Stage();
			studentStage.setTitle("Event Registration");
			StudentController studentController = new StudentController(studentStage, loggedInUser);
			loader.setController(studentController);
			Parent root;
			root = loader.load();
			studentStage.setScene(new Scene(root));
			studentStage.show();
		} catch (IOException e) {
			TrayNotification tray = new TrayNotification();
            tray.setNotificationType(NotificationType.ERROR);
            tray.setTitle("Event Manager");
            tray.setMessage("Unable open registeration.\nContact System Administration.");
            tray.setAnimationType(AnimationType.SLIDE);
            tray.showAndDismiss(Duration.millis(1500));
			e.printStackTrace();
		}
	}

	/**
	 * This method is called on role panel click.
	 * @param event
	 */
	@FXML
	void onRolePanelClick(ActionEvent event) {
		try {
    		FXMLLoader loader = new FXMLLoader();
        	loader.setLocation(getClass().getClassLoader().getResource("\\com\\iit\\eventmanagment\\view\\role.fxml"));
			loader.load();
			Parent root = loader.getRoot();
			Stage rolePanelStage = new Stage();
			rolePanelStage.setTitle("Role Panel");
			rolePanelStage.setScene(new Scene(root));
			RoleController roleController = loader.<RoleController>getController();
			rolePanelStage.show();
		} catch (IOException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("My Profle");
			alert.setHeaderText("Oopps, View Profile!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}

	/**
	 * This method is called on assign resource panel click.
	 * @param event
	 */
	@FXML
	void onAssignResourceClick(ActionEvent event) {
		try {
    		FXMLLoader loader = new FXMLLoader();
        	loader.setLocation(getClass().getClassLoader().getResource("\\com\\iit\\eventmanagment\\view\\assign_event_resources.fxml"));
        	Stage assignResourceStage = new Stage();
        	assignResourceStage.setTitle("Assign Resources");
    		AssignEventResourceController assignEventResourceController = new AssignEventResourceController(assignResourceStage, loggedInUser);
        	loader.setController(assignEventResourceController);
        	Parent root = loader.load();
        	assignResourceStage.setScene(new Scene(root));
        	assignResourceStage.show();
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Event Manager");
			alert.setHeaderText("Oopps, error while loading resources!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}


	/**
	 * This method is called on department panel click.
	 * @param event
	 */
    @FXML
    void onDepartmentPanelClick(ActionEvent event) {
    	try {
    		FXMLLoader loader = new FXMLLoader();
        	loader.setLocation(getClass().getClassLoader().getResource("\\com\\iit\\eventmanagment\\view\\department.fxml"));
			loader.load();
			Parent root = loader.getRoot();
			Stage departmentPanelStage = new Stage();
			departmentPanelStage.setTitle("Department Panel");
			departmentPanelStage.setScene(new Scene(root));
			DepartmentController departmentController = loader.<DepartmentController>getController();
			departmentPanelStage.show();
		} catch (IOException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Department");
			alert.setHeaderText("Oopps, could not load department screen!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
    }

	/**
	 * This method is called on ticket panel click.
	 * @param event
	 */
	@FXML
	void onTicketPanelClick(ActionEvent event) {
		try {
    		FXMLLoader loader = new FXMLLoader();
        	loader.setLocation(getClass().getClassLoader().getResource("\\com\\iit\\eventmanagment\\view\\ticket.fxml"));
			Stage ticketPanelStage = new Stage();
			ticketPanelStage.setTitle("Ticket Panel");
			TicketController ticketController = new TicketController(ticketPanelStage, loggedInUser);
			loader.setController(ticketController);
			Parent root = loader.load();
			ticketPanelStage.setScene(new Scene(root));
			ticketPanelStage.show();
		} catch (IOException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Ticket");
			alert.setHeaderText("Oopps, could not load ticket screen!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}

	/**
	 * This method is called on profile panel click.
	 * @param event
	 */
	@FXML
	void onProfilePanelClick(ActionEvent event) {
    	try {
    		FXMLLoader loader = new FXMLLoader();
        	loader.setLocation(getClass().getClassLoader().getResource("\\com\\iit\\eventmanagment\\view\\my_profile.fxml"));
			loader.load();
			Parent root = loader.getRoot();
			Stage myProfileStage = new Stage();
			myProfileStage.setTitle("My Profle");
			myProfileStage.setScene(new Scene(root));
			MyProfileController myProfileController = loader.<MyProfileController>getController();
			myProfileController.setLoggedInUser(this.loggedInUser);
			myProfileController.displayProfile();
			myProfileController.setMyProfileStage(myProfileStage);
			myProfileStage.show();
		} catch (IOException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("My Profle");
			alert.setHeaderText("Oopps, View Profile!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}
}
