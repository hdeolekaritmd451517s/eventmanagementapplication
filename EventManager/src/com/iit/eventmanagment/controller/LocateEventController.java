package com.iit.eventmanagment.controller;

import com.iit.eventmanagment.model.Event;
import com.iit.eventmanagment.model.Users;
import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.object.GoogleMap;
import com.lynden.gmapsfx.javascript.object.InfoWindow;
import com.lynden.gmapsfx.javascript.object.InfoWindowOptions;
import com.lynden.gmapsfx.javascript.object.LatLong;
import com.lynden.gmapsfx.javascript.object.MapOptions;
import com.lynden.gmapsfx.javascript.object.MapTypeIdEnum;
import com.lynden.gmapsfx.javascript.object.Marker;
import com.lynden.gmapsfx.javascript.object.MarkerOptions;

import application.utils.LatitudeLongitude;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Tab;
import javafx.stage.Stage;
import javafx.util.Duration;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

/**
 * This class marks the location of the event on the google map. Additionally, it shows event details with the marker.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class LocateEventController implements MapComponentInitializedListener {
	GoogleMapView mapView;
	GoogleMap map;
	@FXML
	private Tab locateEventTabPane;

	@FXML
	private Tab trackEventProgressTabPane;

	public Users getLoggedInUser() {
		return loggedInUser;
	}

	public void setLoggedInUser(Users loggedInUser) {
		this.loggedInUser = loggedInUser;
	}

	public Event getEventToTrack() {
		return eventToTrack;
	}

	public void setEventToTrack(Event eventToTrack) {
		this.eventToTrack = eventToTrack;
	}

	private Users loggedInUser;
	private Event eventToTrack;
	Stage locateEventStage;

	public LocateEventController(Stage locateEventStage, Users loggedInUser, Event eventToTrack) {
		super();
		this.loggedInUser = loggedInUser;
		this.eventToTrack = eventToTrack;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LocateEventController [loggedInUser=");
		builder.append(loggedInUser);
		builder.append(", eventToTrack=");
		builder.append(eventToTrack);
		builder.append("]");
		return builder.toString();
	}

	@FXML
	void onLocateEventClick(ActionEvent event) {
		//Scene scene = new Scene(mapView);
	}

	@FXML
	void onTrackEventProgressClick(ActionEvent event) {

	}

	/**
	 * This method displays the event location on the map.
	 */
	@Override
	public void mapInitialized() {
		try {
			LatitudeLongitude trackEvent = new LatitudeLongitude();
			System.out.println("Address ---->"+eventToTrack.getEventAddress() + eventToTrack.getEventCity() +
					eventToTrack.getEventState()+ eventToTrack.getEventPostalCode() + eventToTrack.getEventCountry());
			//get the lat and long based on complete address passed as the parameter.
			String latLongs[] = trackEvent.getLatLongPositions(eventToTrack.getEventAddress() + eventToTrack.getEventCity()
			+ eventToTrack.getEventState()+ eventToTrack.getEventPostalCode() + eventToTrack.getEventCountry());

			System.out.println("Tracked lat: "+latLongs[0]+" long:"+latLongs[1]);
			// initialize map options to customize the map to be display
			MapOptions mapOptions = new MapOptions();
			mapOptions.center(new LatLong(Double.parseDouble(latLongs[0]), Double.parseDouble(latLongs[1]))).mapType(MapTypeIdEnum.ROADMAP).overviewMapControl(false)
					.panControl(false).rotateControl(false).scaleControl(false).streetViewControl(false)
					.zoomControl(false).zoom(12);
			// add customization to the mapview
			map = mapView.createMap(mapOptions);
			// Add a marker to the map
			MarkerOptions markerOptions = new MarkerOptions();
			// set customization to the lat and long
			markerOptions.position(new LatLong(Double.parseDouble(latLongs[0]), Double.parseDouble(latLongs[1]))).visible(Boolean.TRUE).title("My Marker");
			// adding map option to the marker
			Marker marker = new Marker(markerOptions);
			// info window shows event details at the marker positioned
			InfoWindowOptions infoWindowOptions = new InfoWindowOptions();
	        infoWindowOptions.content("<font face='georgia' color='green'><b>Event Name:</b> "+eventToTrack.getName()+
	        						"<br><b>Event Address:</b><br>"+eventToTrack.getEventAddress()+",<br>"+
	        						eventToTrack.getEventCity()+"-"+eventToTrack.getEventPostalCode()+",<br>"
	        						+eventToTrack.getEventState()+", "+eventToTrack.getEventCountry()+
	        						"<br><b>Start Date:</b>"+eventToTrack.getEventStartDate()+
	        						"<br><b>Start Time:</b>"+eventToTrack.getEventStartTime()+"<br>"
	        						+"<b>Contact:</b></fomt>"+eventToTrack.getEventEmail()).maxWidth(300);

	        InfoWindow fredWilkeInfoWindow = new InfoWindow(infoWindowOptions);
	        fredWilkeInfoWindow.open(map, marker);
	        // finally, adding the marker to the map.
			map.addMarker(marker);
		} catch (Exception e) {
			// notifies if address passed was junk, if unable to display it on map.
			TrayNotification tray = new TrayNotification();
			tray.setNotificationType(NotificationType.INFORMATION);
			tray.setTitle("Map cannot be initialized. Contact system administrator");
			tray.setMessage("Please select the event first!");
			tray.setAnimationType(AnimationType.SLIDE);
			tray.showAndDismiss(Duration.millis(2000));
			e.printStackTrace();
		}
	}

	public void setMapView(GoogleMapView mapView) {
		this.mapView = mapView;
	}

}
