package com.iit.eventmanagment.controller;

import java.math.BigDecimal;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Predicate;

import com.iit.eventmanagment.model.Ticket;
import com.iit.eventmanagment.model.Users;
import com.iit.eventmanagment.service.TicketService;
import com.iit.eventmanagment.service.impl.TicketServiceImpl;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.RequiredFieldValidator;

import application.utils.InputValidator;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.converter.LocalDateStringConverter;

/**
 * Handles the adding, editing, updating and deleting of the tickets in the database.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class TicketController implements Initializable{

	Ticket currentTicket;

	@FXML
	private MenuItem addTicketMenuItem;

	@FXML
	private MenuItem assignTicketMenuItem;

	@FXML
	private GridPane ticketGridPane;

	@FXML
	private JFXTextField ticketNameTextField;

	@FXML
	private JFXTextField ticketPriceTextField;

	@FXML
	private JFXTextField quantityAvailableTextField;

	@FXML
	private JFXTextField ticketRegistrationLimitTextField;

	@FXML
	private JFXDatePicker ticketStartDatePicker;

	@FXML
	private JFXDatePicker ticketEndTimePicker;

	@FXML
	private JFXDatePicker ticketStartTimePicker;

	@FXML
	private JFXDatePicker ticketEndDatePicker;

	@FXML
	private JFXButton saveTicketButton;

	@FXML
	private JFXButton updateTicketButton;

	@FXML
	private JFXButton deleteTicketButton;

	@FXML
	private JFXButton editTicketButton;

	@FXML
	private JFXTextField searchTicketTextField;

	@FXML
	private JFXButton searchTicketButton;

	@FXML
	private TableView<Ticket> ticketTableView;

	@FXML
	private TableColumn<Ticket, Integer> ticketIDTableColumn;

	@FXML
	private TableColumn<Ticket, String> ticketNameTableColumn;

	@FXML
	private TableColumn<Ticket, BigDecimal> ticketPriceTableColumn;

	@FXML
	private TableColumn<Ticket, Integer> ticketQuantityTableColumn;

	@FXML
	private TableColumn<Ticket, LocalDate> ticketStartDateTableColumn;

	@FXML
	private TableColumn<Ticket, LocalTime> ticketStartTimeTableColumn;

	@FXML
	private TableColumn<Ticket, LocalDate> ticketEndDateTableColumn;

	@FXML
	private TableColumn<Ticket, LocalTime> ticketEndTimeTableColumn;

	@FXML
	private TableColumn<Ticket, String> ticketRegistratonLimitTableColumn;

	private Users loggedInUser;
	private Stage ticketPanelStage;

	private ObservableList<Ticket> ticketDataList = FXCollections.observableArrayList();

	RequiredFieldValidator ticketNameValidator = new RequiredFieldValidator();
	RequiredFieldValidator ticketPriceValidator = new RequiredFieldValidator();
	RequiredFieldValidator ticketQuantiyValidator = new RequiredFieldValidator();
	RequiredFieldValidator quantityAvailableValidator = new RequiredFieldValidator();
	RequiredFieldValidator ticketRegistrationLimitValidator = new RequiredFieldValidator();

	/**
	 * Sets the ticket stage and logged in user.
	 * @param ticketPanelStage ticket stage
	 * @param loggedInUser logged in user
	 */
	public TicketController(Stage ticketPanelStage, Users loggedInUser) {
		setLoggedInUser(loggedInUser);
		setTicketPanelStage(ticketPanelStage);
	}

	/**
	 * Initializes the table with available tickets. Registers required field validators.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			ticketIDTableColumn.setCellValueFactory(cellData -> cellData.getValue().ticketIDProperty().asObject());
			ticketNameTableColumn.setCellValueFactory(cellData -> cellData.getValue().ticketNameProperty());
			ticketPriceTableColumn.setCellValueFactory(cellData -> cellData.getValue().ticketPriceProperty());
			ticketQuantityTableColumn.setCellValueFactory(cellData -> cellData.getValue().ticketQuantityProperty().asObject());
			ticketStartDateTableColumn.setCellValueFactory(cellData -> cellData.getValue().ticketStartDateProperty());
			ticketStartTimeTableColumn.setCellValueFactory(cellData -> cellData.getValue().ticketStartTimeProperty());
			ticketEndDateTableColumn.setCellValueFactory(cellData -> cellData.getValue().ticketEndDateProperty());
			ticketEndTimeTableColumn.setCellValueFactory(cellData -> cellData.getValue().ticketEndTimeProperty());
			ticketRegistratonLimitTableColumn.setCellValueFactory(cellData -> cellData.getValue().ticketRegistrationLimitProperty());
			TicketService ticketService = new TicketServiceImpl();
			registerListeners();
			List<Ticket> ticketList = ticketService.viewAllTicketsById(loggedInUser);
			if(ticketList != null)
				ticketDataList.addAll(ticketList);
			//resourceTableView.setItems(userDataList);
			FilteredList<Ticket> ticketFilteredData = new FilteredList<>(ticketDataList, e -> true);
			searchTicketTextField.setOnKeyReleased(e -> {
				searchTicketTextField.textProperty().addListener((observableValue, oldValue, newValue) -> {
					ticketFilteredData.setPredicate((Predicate<? super Ticket>) ticket -> {
						String lowerCaseFilter = newValue.toLowerCase();
						if(newValue == null || newValue.isEmpty())
							return true;
						else if(String.valueOf(ticket.getId()).contains(lowerCaseFilter))
							return true;
						else if(ticket.getTicketName().toLowerCase().contains(lowerCaseFilter))
							return true;
						else if(String.valueOf(ticket.getTicketPrice()).contains(lowerCaseFilter))
							return true;
						return false;
					});
				});
			});
			ticketTableView.setItems(ticketFilteredData);
			/*if(ticketList != null){
				ticketDataList.addAll(ticketList);
			}
			ticketTableView.setItems(ticketDataList);*/
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Ticket");
			alert.setHeaderText("Oopps, error loading Tickets!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}

	public Users getLoggedInUser() {
		return loggedInUser;
	}

	public void setLoggedInUser(Users loggedInUser) {
		this.loggedInUser = loggedInUser;
	}

	public Stage getTicketPanelStage() {
		return ticketPanelStage;
	}

	public void setTicketPanelStage(Stage ticketPanelStage) {
		this.ticketPanelStage = ticketPanelStage;
	}

	/**
	 * Registers required field validators and change listeners.
	 */
	private void registerListeners() {
		ticketNameTextField.getValidators().add(ticketNameValidator);
		ticketPriceTextField.getValidators().add(ticketPriceValidator);
		quantityAvailableTextField.getValidators().add(quantityAvailableValidator);
		ticketRegistrationLimitTextField.getValidators().add(ticketRegistrationLimitValidator);
		ticketNameTextField.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(!newValue){
					ticketNameValidator.setMessage("field is required");
					ticketNameTextField.validate();
				}
			}
		});
		ticketPriceTextField.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(!newValue){
					ticketPriceValidator.setMessage("field is required");
					ticketPriceTextField.validate();
				}
			}
		});
		quantityAvailableTextField.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(!newValue){
					quantityAvailableValidator.setMessage("field is required");
					quantityAvailableTextField.validate();
				}
			}
		});
		ticketRegistrationLimitTextField.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(!newValue){
					ticketRegistrationLimitValidator.setMessage("field is required");
					ticketRegistrationLimitTextField.validate();
				}
			}
		});

	}

	/**
	 * Allows adding ticket
	 * @param event
	 */
	@FXML
	void onAddTicketClick(ActionEvent event) {
		ticketGridPane.setDisable(false);
		saveTicketButton.setDisable(false);
		updateTicketButton.setDisable(true);
		deleteTicketButton.setDisable(true);
		// clears the input form
		clearTicket();
	}


	/**
	 * Displays screen to assign tickets to the events.
	 * @param event
	 */
	@FXML
	void onAssignTicketPanelClick(ActionEvent event) {
		try {
    		FXMLLoader loader = new FXMLLoader();
        	loader.setLocation(getClass().getClassLoader().getResource("\\com\\iit\\eventmanagment\\view\\assign_event_tickets.fxml"));
        	Stage assignTicketStage = new Stage();
        	assignTicketStage.setTitle("Assign Tickets");
    		AssignEventTicketController assignEventTicketController = new AssignEventTicketController(assignTicketStage, loggedInUser);
        	loader.setController(assignEventTicketController);
        	Parent root = loader.load();
        	assignTicketStage.setScene(new Scene(root));
        	assignTicketStage.show();
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Event Manager");
			alert.setHeaderText("Oopps, error while loading tickets!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}

	/**
	 * Deletes the ticket and updates the tableview
	 * @param event
	 */
	@FXML
	void onDeleteTicketClick(ActionEvent event) {
		try {
				TicketService ticketService = new TicketServiceImpl();
				Ticket ticketTobeDeleted = getEditedTicket();
				boolean result = ticketService.deleteTicket(ticketTobeDeleted);
				if(result){
					int selectedIndex = ticketTableView.getSelectionModel().getSelectedIndex();
					if (selectedIndex >= 0) {
						ticketTableView.getItems().remove(selectedIndex);
					} else {
						// Nothing selected.
						Alert alert = new Alert(AlertType.WARNING);
						alert.setTitle("Delete Location");
						alert.setHeaderText("Oopps, error deleting tickets!");
						alert.setContentText("Please select the tickets.");
						alert.showAndWait();
					}
				}
				updateTicketButton.setDisable(true);
				deleteTicketButton.setDisable(true);
				ticketGridPane.setDisable(true);
				clearTicket();
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Update Location");
			alert.setHeaderText("Oopps, error updating location!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}

	/**
	 * Creates new ticket object with current selected ticke to be edited.
	 * @return
	 */
	private Ticket getEditedTicket() {
		Ticket newEditedTicket = new Ticket(currentTicket.getId(), ticketNameTextField.getText(), ticketPriceTextField.getText(), quantityAvailableTextField.getText(), ticketStartDatePicker.getValue(),
				ticketStartTimePicker.getTime(), ticketEndDatePicker.getValue(), ticketEndTimePicker.getTime(), ticketRegistrationLimitTextField.getText());
		return newEditedTicket;
	}

	/**
	 * Clears the edit form
	 */
	private void clearTicket() {
		if(ticketNameTextField.getText() != null)
			ticketNameTextField.clear();
		if(ticketPriceTextField.getText() != null)
			ticketPriceTextField.clear();
		if(quantityAvailableTextField.getText() != null)
			quantityAvailableTextField.clear();
		if(ticketStartDatePicker.getValue() != null){
			ticketStartDatePicker.getEditor().clear();
			ticketStartDatePicker.setValue(null);
		}
		if(ticketStartTimePicker.getTime() != null){
			ticketStartTimePicker.getEditor().clear();
			ticketStartTimePicker.setTime(null);
		}
		if(ticketEndDatePicker.getValue() != null){
			ticketEndDatePicker.getEditor().clear();
			ticketEndDatePicker.setValue(null);
		}
		if(ticketEndTimePicker.getTime() != null){
			ticketEndTimePicker.getEditor().clear();
			ticketEndTimePicker.setTime(null);
		}
		if(ticketRegistrationLimitTextField.getText() != null)
			ticketRegistrationLimitTextField.clear();
	}

	/**
	 * Allows ticket to be edited and updated
	 * @param event
	 */
	@FXML
	void onEditTicketClick(ActionEvent event) {
		currentTicket = ticketTableView.getSelectionModel().getSelectedItem();
		if(editTicket(currentTicket)){
			saveTicketButton.setDisable(true);
			updateTicketButton.setDisable(false);
			deleteTicketButton.setDisable(false);
		}else{
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Event Manager");
			alert.setHeaderText("Edit Ticket");
			alert.setContentText("Message: Please select the ticket first!!");
			alert.showAndWait();
		}
	}


	/**
	 * Saves the ticket and clears the form, disables the update and delete buttons.
	 * @param event
	 */
	@FXML
	void onSaveTicketClick(ActionEvent event) {
		try {
			if(validateTicket()){
				TicketService ticketService = new TicketServiceImpl();
				Ticket newTicket = getNewTicket();
				newTicket = ticketService.addTicket(newTicket, loggedInUser);
				if(newTicket != null){
					ticketDataList.add(newTicket);
					clearTicket();
				} else{
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Event Manager");
					alert.setHeaderText("Add Ticket");
					alert.setContentText("Message: Ticket could not be saved!");
					alert.showAndWait();
				}
				ticketGridPane.setDisable(true);
				saveTicketButton.setDisable(true);
				updateTicketButton.setDisable(true);
				deleteTicketButton.setDisable(true);
			}
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Update Location");
			alert.setHeaderText("Oopps, error updating location!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}

	/**
	 * Creates a ticket object for the new ticket to be created
	 * @return ticket object to be added.
	 */
	private Ticket getNewTicket() {
		Ticket newEditedTicket = new Ticket(ticketNameTextField.getText(), ticketPriceTextField.getText(), quantityAvailableTextField.getText(), ticketStartDatePicker.getValue(),
				ticketStartTimePicker.getTime(), ticketEndDatePicker.getValue(), ticketEndTimePicker.getTime(), ticketRegistrationLimitTextField.getText());
		return newEditedTicket;
	}


	/**
	 * Update the ticket into the tableview and database
	 * @param event
	 */
	@FXML
	void onUpdateTicketClick(ActionEvent event) {
		Ticket newEditedTicket;
		try {
			// validates whether required fields are filled up.
			if(validateTicket()){
				TicketService ticketService = new TicketServiceImpl();
				newEditedTicket = getEditedTicket();
				// updates in the database
				boolean result = ticketService.updateTicket(newEditedTicket);
				if(result){
					// updates the tableview
					updateTableRecord(newEditedTicket);
				} else {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Location");
					alert.setHeaderText("Update Location");
					alert.setContentText("Message: No records were updated");
					alert.showAndWait();
				}
				updateTicketButton.setDisable(true);
				deleteTicketButton.setDisable(true);
				ticketGridPane.setDisable(true);
				clearTicket();
			}
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Update Location");
			alert.setHeaderText("Oopps, error updating location!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}

	/**
	 * Updates the selected ticket with the new values.
	 * @param newEditedTicket
	 */
	private void updateTableRecord(Ticket newEditedTicket) {
		currentTicket.setId(newEditedTicket.getId());
		currentTicket.setTicketName(newEditedTicket.getTicketName());
		currentTicket.setTicketPrice(newEditedTicket.getTicketPrice());
		currentTicket.setTicketQuantity(newEditedTicket.getTicketQuantity());
		currentTicket.setTicketStartDate(newEditedTicket.getTicketStartDate());
		currentTicket.setTicketStartTime(newEditedTicket.getTicketStartTime());
		currentTicket.setTicketEndDate(newEditedTicket.getTicketEndDate());
		currentTicket.setTicketEndTime(newEditedTicket.getTicketEndTime());
		currentTicket.setPerTicketRegistrationLimit(newEditedTicket.getPerTicketRegistrationLimit());
	}

	/**
	 * Displays the selected ticket into the edit form
	 * @param currentTicket
	 * @return true if successfully able to edit.
	 */
	private boolean editTicket(Ticket currentTicket) {
		boolean result = false;
		if(currentTicket!=null){
			ticketGridPane.setDisable(false);
			ticketNameTextField.setText(currentTicket.getTicketName());
			ticketPriceTextField.setText(currentTicket.getTicketPrice().toPlainString());
			quantityAvailableTextField.setText(String.valueOf(currentTicket.getTicketQuantity()));
			ticketStartDatePicker.setValue(currentTicket.getTicketStartDate());
			ticketStartTimePicker.setTime(currentTicket.getTicketStartTime());
			ticketEndDatePicker.setValue(currentTicket.getTicketEndDate());
			ticketEndTimePicker.setTime(currentTicket.getTicketEndTime());
			ticketRegistrationLimitTextField.setText(currentTicket.getPerTicketRegistrationLimit());
			result = true;
		}
		return result;
	}

	/**
	 * Validates the ticket
	 * @return true if ticket is valid
	 */
	private boolean validateTicket(){
		boolean isValid = true;
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Event Manager");
		alert.setHeaderText("Event Manager");
		// checks if required fields are filled up
		if(!(ticketNameTextField.validate()&&ticketPriceTextField.validate()&&quantityAvailableTextField.validate()
				&&ticketRegistrationLimitTextField.validate())){
			alert.setHeaderText("Event Manager");
			alert.setContentText("Please fill the required fields.");
			alert.showAndWait();
			isValid = false;
			// check if dates are selected
		}else if((ticketStartDatePicker.getValue()==null) ||("null".equals(ticketStartTimePicker.getTime()))||
				(ticketEndDatePicker.getValue()==null)||("null".equals(ticketEndTimePicker.getTime()))){
			alert.setHeaderText("Event Manager");
			alert.setContentText("Please fill the required fields.");
			alert.showAndWait();
			isValid = false;
		}else{
			// validates the values
			if(!InputValidator.validateName(ticketNameTextField.getText())){
				isValid = false;
				alert.setContentText("Please enter valid ticket name.");
				alert.showAndWait();
			} else if(!InputValidator.validatePrice(ticketPriceTextField.getText())){
				isValid = false;
				alert.setContentText("Please enter valid ticket price.");
				alert.showAndWait();
			}else if(!InputValidator.validateNumber(quantityAvailableTextField.getText())){
				isValid = false;
				alert.setContentText("Please enter valid ticket quantity.");
				alert.showAndWait();
			}else if(!InputValidator.validateNumber(ticketRegistrationLimitTextField.getText())){
				isValid = false;
				alert.setContentText("Please enter valid ticket registration limit.");
				alert.showAndWait();
			}
		}
		return isValid;
	}
}
