package com.iit.eventmanagment.controller;

import java.net.URL;
import java.util.List;
import java.util.ListIterator;
import java.util.ResourceBundle;
import org.controlsfx.control.CheckComboBox;
import com.iit.eventmanagment.model.Event;
import com.iit.eventmanagment.model.Users;
import com.iit.eventmanagment.service.EventService;
import com.iit.eventmanagment.service.ResourceService;
import com.iit.eventmanagment.service.impl.EventServiceImpl;
import com.iit.eventmanagment.service.impl.ResourceServiceImpl;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.skins.JFXComboBoxListViewSkin;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * This is controller allows user to assign and view resource to the events. Before assigning resource to the event,
 * user must first add/create  his resources. This controller can be called from admin as well as organizer.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class AssignEventResourceController implements Initializable{
	@FXML
	private JFXButton unAssignResourceButton;

	@FXML
	private JFXButton assignResourceButton;

	@FXML
	private JFXButton assignButton;

	@FXML
	private VBox assignResourceVBox;

	@FXML
	private JFXComboBox<Event> selectEventComboBox;

	@FXML
	private CheckComboBox<Users> selectResourceComboBox;

	@FXML
	private TreeView<Object> eventResourceTreeView;
	private  TreeItem<Object> eventResourceRootNode;
	List<Event> eventList;
	// observable event list
	private ObservableList<Event> selectEventComboBoxDataList = FXCollections.observableArrayList();
	// observable resource  list
	private ObservableList<Users>  selectResourceComboBoxDataList = FXCollections.observableArrayList();
	private Stage assignResourceStage;
	Users loggedInUser;

	/**
	 * Sets the stage assign_event_resources.fxml view
	 * @param assignResourceStage stage to be loaded.
	 * @param loggedInUser logged in user.
	 */
	public AssignEventResourceController(Stage assignResourceStage, Users loggedInUser) {
		setAssignResourceStage(assignResourceStage);
		setLoggedInUser(loggedInUser);
	}

	public Stage getAssignResourceStage() {
		return assignResourceStage;
	}



	public void setAssignResourceStage(Stage assignResourceStage) {
		this.assignResourceStage = assignResourceStage;
	}



	public Users getLoggedInUser() {
		return loggedInUser;
	}



	public void setLoggedInUser(Users loggedInUser) {
		this.loggedInUser = loggedInUser;
	}

	/**
	 * This method is called on assign resource button click. It enables user to assign resources.
	 * @param event
	 */
	@FXML
	void onAssignResourceClick(ActionEvent e) {
		assignResourceVBox.setDisable(false);
	}

	/**
	 * This method is called after the user selects the events and resources to be assigned. On "Assign" button click
	 * Note that user needs to first add his resources before assigning them to the event.
	 * @param event
	 */
	@FXML
	void onAssignClick(ActionEvent e) {
		try {
			Event event = selectEventComboBox.getSelectionModel().getSelectedItem();
			if(event!=null){
				ObservableList<Users> user = selectResourceComboBox.getCheckModel().getCheckedItems();
				if(user != null){
					event.setEventResources(user);
					ResourceService resourceService = new ResourceServiceImpl();
					boolean result = resourceService.assignEventResourcesById(loggedInUser, event);
					if(result){
						displayTreeView(event);
						selectEventComboBox.getSelectionModel().clearSelection();
						selectResourceComboBox.getCheckModel().clearChecks();
						assignResourceVBox.setDisable(true);
					}
				}else{
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Event Manager");
					alert.setHeaderText("Assign Event");
					alert.setContentText("Message: Please select the resources");
					alert.showAndWait();
				}
			}else{
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Event Manager");
				alert.setHeaderText("Assign Event");
				alert.setContentText("Message: Please select the event");
				alert.showAndWait();
			}
		} catch (Exception e1) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Event Manager");
			alert.setHeaderText("Assign Event");
			alert.setContentText("Message: Error occured while assigning resource"+e1.getMessage());
			alert.showAndWait();
			e1.printStackTrace();
		}
	}

	/**
	 * This method displays the table with events and its assigned resources.
	 * For the scope of this project, we just show newly assigned resources.
	 * @param e
	 */
	private void displayTreeView(Event e) {
		boolean isEventMatched  = false;
		// gets the root element, check if any events are present
		if(eventResourceRootNode.getChildren().isEmpty()){
			isEventMatched  = true;
			// if no events are present, add this event to the tree root node.
			TreeItem<Object> newParentTreeItem = new TreeItem<Object>(e);
			eventResourceRootNode.getChildren().add(newParentTreeItem);
			// add all the selected resources to the added event
			ObservableList<TreeItem<Object>> events = eventResourceRootNode.getChildren();
			for (TreeItem<Object> event : events) {
				Event castedEvent = (Event)event.getValue();
				if(castedEvent.getName().equals(e.getName())){
					e.getEventResources().forEach(r->{
						TreeItem<Object> newChildTreeItem = new TreeItem<Object>(r);
						event.getChildren().add(newChildTreeItem);
					});
				}
			}
		}else{
			// if the root is not empty, check which event node matches to the event for which resources are to be assigned
			ObservableList<TreeItem<Object>> events = eventResourceRootNode.getChildren();
			for (TreeItem<Object> event : events) {
				Event castedEvent = (Event)event.getValue();
				// if event node found, add all the selected resources to this event node
				if(castedEvent.getName().equals(e.getName())){
					isEventMatched = true;
					e.getEventResources().forEach(r->{
						TreeItem<Object> newChildTreeItem = new TreeItem<Object>(r);
						event.getChildren().add(newChildTreeItem);
					});
				}
			}
			// if root node is not empty, and event is not found, then add this event to the tree and add its resources
			if(!isEventMatched){
				TreeItem<Object> newParentTreeItem = new TreeItem<Object>(e);
				eventResourceRootNode.getChildren().add(newParentTreeItem);
				for (TreeItem<Object> event : events) {
					Event castedEvent = (Event)event.getValue();
					if(castedEvent.getName().equals(e.getName())){
						isEventMatched = true;
						e.getEventResources().forEach(r->{
							TreeItem<Object> newChildTreeItem = new TreeItem<Object>(r);
							event.getChildren().add(newChildTreeItem);
						});
					}
				}
			}
		}
	}

	/**
	 * This method is called on unassigned click.
	 * @param event
	 */
	@FXML
	void onUnAssignResourceClick(ActionEvent event) {
		try {
			TreeItem<Object> selectedResourceTreeItem = eventResourceTreeView.getSelectionModel().getSelectedItem();
			TreeItem<Object> selectedEventTreeItem = selectedResourceTreeItem.getParent();
			if((selectedResourceTreeItem != null) && (selectedEventTreeItem != null)){
				Event selectedEvent = (Event) selectedEventTreeItem.getValue();
				Users selectedUser = (Users) selectedResourceTreeItem.getValue();
				ResourceService resourceService = new ResourceServiceImpl();
				boolean result = resourceService.unAssignEventResourcesById(loggedInUser, selectedEvent, selectedUser);
				if(result){
					ObservableList<TreeItem<Object>> l = selectedEventTreeItem.getChildren();
					ListIterator<TreeItem<Object>> it =l.listIterator();
					while (it.hasNext()) {
						Users users = (Users)it.next().getValue();
						if(users.getId() == selectedUser.getId())
							it.remove();
					}
					unAssignResourceButton.setDisable(true);
				}else{
					Alert alert = new Alert(AlertType.WARNING);
					alert.setTitle("Event Manager");
					alert.setHeaderText("UnAssign Resource!");
					alert.setContentText("Message: No resources were unassigned!");
					alert.showAndWait();
				}
			}
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Event Manager");
			alert.setHeaderText("UnAssign Resource!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
		}
	}

	/**
	 * Initializes the event and resources combo box based on the logged in user.
	 */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		try {
			EventService eventService = new EventServiceImpl();
			eventList = eventService.viewAllEventByUserId(loggedInUser);
			selectEventComboBoxDataList.addAll(eventList);
			selectEventComboBox.setItems(selectEventComboBoxDataList);
			ResourceService resourceService = new ResourceServiceImpl();
			List<Users> userList = resourceService.viewAllResourcesById(loggedInUser);
			selectResourceComboBoxDataList.addAll(userList);
			selectResourceComboBox.getItems().addAll(selectResourceComboBoxDataList);
			eventResourceRootNode = new TreeItem<Object>(loggedInUser.getFirstName()+"'s Resource Pool");
			eventResourceTreeView.setRoot(eventResourceRootNode);
		} catch (Exception e) {
			e.printStackTrace();
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Event Manager");
			alert.setHeaderText("Oopps, error while loading your events and resources!");
			alert.setContentText("Message: "+e.getMessage());
			alert.showAndWait();
		}
	}


	/**
	 * This method enables or disables the assigned button based on which node the user clicks in the tree.
	 * It will enable the assign button only if user clicks the resource nodes in the tree.
	 * @param me
	 */
	public  void mouseClickOnTreeView(MouseEvent me){
		TreeItem<Object> selectedObject = eventResourceTreeView.getSelectionModel().getSelectedItem();
		if(selectedObject!=null){
			Object obj = selectedObject.getValue();
			if(obj instanceof Event){
				System.out.println("Event Node: "+((Event)obj).getName());
				unAssignResourceButton.setDisable(true);
			} else if(obj instanceof Users){
				System.out.println("Resource Node: "+((Users)obj).getFirstName());
				unAssignResourceButton.setDisable(false);
			} else if(obj instanceof String){
				System.out.println("Root Nodet: "+((String)obj));
				unAssignResourceButton.setDisable(true);
			}
		}
	}
}
