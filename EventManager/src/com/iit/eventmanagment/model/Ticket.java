package com.iit.eventmanagment.model;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalTime;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
/**
 * 	Ticket POJO. Contains all the ticket details
  * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class Ticket {
	private IntegerProperty id;
	private StringProperty ticketName;
	private ObjectProperty<BigDecimal> ticketPrice;
	private IntegerProperty ticketQuantity;
	private ObjectProperty<LocalDate> ticketStartDate;
	private ObjectProperty<LocalTime> ticketStartTime;
	private ObjectProperty<LocalDate> ticketEndDate;
	private ObjectProperty<LocalTime> ticketEndTime;
	private StringProperty perTicketRegistrationLimit;

	public Ticket(int id, String ticketName, String ticketPrice, String ticketQuantity, LocalDate ticketStartDate, LocalTime ticketStartTime,
			LocalDate ticketEndDate, LocalTime ticketEndTime, String perTicketRegistrationLimit) {
		super();
		this.id = new SimpleIntegerProperty(id);
		this.ticketName = new SimpleStringProperty(ticketName);
		this.ticketPrice = new SimpleObjectProperty<BigDecimal>(new BigDecimal(ticketPrice));
		this.ticketQuantity = new SimpleIntegerProperty(Integer.parseInt(ticketQuantity));
		this.ticketStartDate = new SimpleObjectProperty<LocalDate>(ticketStartDate);
		this.ticketStartTime = new SimpleObjectProperty<LocalTime>(ticketStartTime);
		this.ticketEndDate = new SimpleObjectProperty<LocalDate>(ticketEndDate);
		this.ticketEndTime = new SimpleObjectProperty<LocalTime>(ticketEndTime);
		this.perTicketRegistrationLimit = new SimpleStringProperty(perTicketRegistrationLimit);
	}

	public Ticket(String ticketName, String ticketPrice, String ticketQuantity, LocalDate ticketStartDate, LocalTime ticketStartTime,
			LocalDate ticketEndDate, LocalTime ticketEndTime, String perTicketRegistrationLimit){
		this.id = new SimpleIntegerProperty(0);
		this.ticketName = new SimpleStringProperty(ticketName);
		this.ticketPrice = new SimpleObjectProperty<BigDecimal>(new BigDecimal(ticketPrice));
		this.ticketQuantity = new SimpleIntegerProperty(Integer.parseInt(ticketQuantity));
		this.ticketStartDate = new SimpleObjectProperty<LocalDate>(ticketStartDate);
		this.ticketStartTime =new SimpleObjectProperty<LocalTime>(ticketStartTime);
		this.ticketEndDate = new SimpleObjectProperty<LocalDate>(ticketEndDate);
		this.ticketEndTime = new SimpleObjectProperty<LocalTime>(ticketEndTime);
		this.perTicketRegistrationLimit = new SimpleStringProperty(perTicketRegistrationLimit);
	}
	public int getId() {
		return id.get();
	}
	public void setId(int id) {
		this.id.set(id);
	}
	public IntegerProperty ticketIDProperty(){
		return this.id;
	}
	public String getTicketName() {
		return ticketName.get();
	}
	public void setTicketName(String ticketName) {
		this.ticketName.set(ticketName);
	}
	public StringProperty ticketNameProperty() {
		return this.ticketName;
	}
	public BigDecimal getTicketPrice() {
		return ticketPrice.get();
	}
	public void setTicketPrice(BigDecimal ticketPrice) {
		this.ticketPrice.set(ticketPrice);
	}
	public ObjectProperty<BigDecimal> ticketPriceProperty() {
		return this.ticketPrice;
	}
	public int getTicketQuantity() {
		return ticketQuantity.get();
	}
	public void setTicketQuantity(int ticketQuantity) {
		this.ticketQuantity.set(ticketQuantity);
	}
	public IntegerProperty ticketQuantityProperty() {
		return this.ticketQuantity;
	}
	public LocalDate getTicketStartDate() {
		return ticketStartDate.get();
	}
	public void setTicketStartDate(LocalDate ticketStartDate) {
		this.ticketStartDate.set(ticketStartDate);
	}
	public ObjectProperty<LocalDate> ticketStartDateProperty() {
		return this.ticketStartDate;
	}
	public LocalTime getTicketStartTime() {
		return ticketStartTime.get();
	}
	public void setTicketStartTime(LocalTime ticketStartTime) {
		this.ticketStartTime.set(ticketStartTime);
	}
	public ObjectProperty<LocalTime> ticketStartTimeProperty() {
		return this.ticketStartTime;
	}
	public LocalDate getTicketEndDate() {
		return ticketEndDate.get();
	}
	public void setTicketEndDate(LocalDate ticketEndDate) {
		this.ticketEndDate.set(ticketEndDate);;
	}
	public ObjectProperty<LocalDate> ticketEndDateProperty() {
		return this.ticketEndDate;
	}
	public LocalTime getTicketEndTime() {
		return ticketEndTime.get();
	}
	public void setTicketEndTime(LocalTime ticketEndTime) {
		this.ticketEndTime.set(ticketEndTime);
	}
	public ObjectProperty<LocalTime> ticketEndTimeProperty() {
		return this.ticketEndTime;
	}
	public String getPerTicketRegistrationLimit() {
		return perTicketRegistrationLimit.get();
	}
	public void setPerTicketRegistrationLimit(String perTicketRegistrationLimit) {
		this.perTicketRegistrationLimit.set(perTicketRegistrationLimit);
	}
	public StringProperty ticketRegistrationLimitProperty() {
		return this.perTicketRegistrationLimit;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(ticketName.get());
		return builder.toString();
	}

}
