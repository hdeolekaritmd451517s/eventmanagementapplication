package com.iit.eventmanagment.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * 	Roles POJO. Contains all the role details
  * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class Roles {
	private IntegerProperty id;
	private StringProperty roleName;
	private StringProperty rolePriority;

	public Roles(int id, String roleName) {
		super();
		this.id = new SimpleIntegerProperty(id);
		this.roleName = new SimpleStringProperty(roleName);
	}

	public Roles(String roleName) {
		super();
		this.id = new SimpleIntegerProperty(0);
		this.roleName = new SimpleStringProperty(roleName);
	}

	public Roles() {
		super();
		this.id = new SimpleIntegerProperty(0);
		this.roleName = new SimpleStringProperty();
	}

	public int getId() {
		return id.get();
	}

	public IntegerProperty roleIdProperty() {
		return this.id;
	}

	public void setId(int id) {
		this.id.set(id);
	}

	public String getRoleName() {
		return roleName.get();
	}

	public StringProperty roleNameProperty() {
		return this.roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName.set(roleName);
	}

	public String getRolePriority() {
		return rolePriority.get();
	}

	public StringProperty rolePriorityProperty() {
		return this.rolePriority;
	}

	public void setRolePriority(String rolePriority) {
		this.rolePriority.set(rolePriority);
	}

	@Override
	public String toString() {
		return roleName.get();
	}
}
