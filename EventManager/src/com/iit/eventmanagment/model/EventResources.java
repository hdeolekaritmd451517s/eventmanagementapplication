package com.iit.eventmanagment.model;


/**
 * 	EventResources POJO.
  * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class EventResources extends Users{
	private int id;
	private String resourceTask;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getResourceTask() {
		return resourceTask;
	}
	public void setResourceTask(String resourceTask) {
		this.resourceTask = resourceTask;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EventResources [id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}
}
