package com.iit.eventmanagment.model;

import java.time.LocalDateTime;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * POJO for Booking events
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 *
 */
public class BookEvent {
	private IntegerProperty id;
	private IntegerProperty userId;
	private StringProperty userFirstName;
	private StringProperty userLastName;
	private StringProperty userEmail;
	private IntegerProperty eventId;
	private StringProperty eventName;
	private ObjectProperty<LocalDateTime> bookingDateTime;
	private BooleanProperty isReminder;

	public BookEvent() {
		this.id = new SimpleIntegerProperty(0);
		this.userId = new SimpleIntegerProperty(0);
		this.userFirstName = new SimpleStringProperty();
		this.userLastName = new SimpleStringProperty();
		this.userEmail = new SimpleStringProperty();
		this.eventId = new SimpleIntegerProperty(0);
		this.eventName = new SimpleStringProperty();
		this.bookingDateTime = new SimpleObjectProperty<LocalDateTime>();
		this.isReminder = new SimpleBooleanProperty();
	}

	public int getId() {
		return id.get();
	}

	public IntegerProperty idProperty() {
		return this.id;
	}

	public void setId(int id) {
		this.id.set(id);
	}

	public int getUserId() {
		return userId.get();
	}

	public IntegerProperty userIdProperty() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId.set(userId);
	}

	public String getUserFirstName() {
		return userFirstName.get();
	}

	public StringProperty userFirstNameProperty() {
		return this.userFirstName;
	}

	public void setUserFirstName(String userFirstName) {
		this.userFirstName.set(userFirstName);
	}

	public String getUserLastName() {
		return userLastName.get();
	}

	public StringProperty userLastNameProperty() {
		return this.userLastName;
	}

	public void setUserLastName(String userLastName) {
		this.userLastName.set(userLastName);
	}

	public String getUserEmail() {
		return userEmail.get();
	}

	public StringProperty userEmailProperty() {
		return this.userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail.set(userEmail);
	}

	public int getEventId() {
		return eventId.get();
	}

	public IntegerProperty eventIdProperty() {
		return this.eventId;
	}

	public void setEventId(int eventId) {
		this.eventId.set(eventId);
	}

	public String getEventName() {
		return eventName.get();
	}

	public StringProperty eventNameProperty() {
		return this.eventName;
	}

	public void setEventName(String eventName) {
		this.eventName.set(eventName);
	}

	public LocalDateTime getBookingDateTime() {
		return bookingDateTime.get();
	}

	public ObjectProperty<LocalDateTime> bookingDateTimeProperty() {
		return this.bookingDateTime;
	}

	public void setBookingDateTime(LocalDateTime bookingDateTime) {
		this.bookingDateTime.set(bookingDateTime);
	}

	public BooleanProperty reminderProperty() {
		return this.isReminder;
	}

	public boolean isReminder() {
		return isReminder.get();
	}

	public void setReminder(boolean isReminder) {
		this.isReminder.set(isReminder);
	}
}
