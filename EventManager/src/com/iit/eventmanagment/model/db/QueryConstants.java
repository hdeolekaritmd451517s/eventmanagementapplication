/**
 *
 */
package com.iit.eventmanagment.model.db;

/**
 * This class contains all the queries. It becomes easy to maintaiin code
 *@author1 Harshal Deolekar
 *@author2 Sri chivukula
 *@version 1.0 build 1.0
 *@since November 01, 2016
 */
public final class QueryConstants {
	public static final String ADD_USERS = "call p_add_user( ?, ?, ?, ?, ?, ?, ?, ?)";
	public static final String ADD_DEPARTMENT = "call p_add_department(?, ?, ?)";
	public static final String ADD_EVENT = "call p_add_events(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	public static final String ADD_LOCATION = "call p_add_location(?, ?, ?, ?, ?, ?, ?, ?, ?)";
	public static final String ADD_TICKET = "call p_add_ticket(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	public static final String ADD_ROLE="call p_add_role(?, ?, ?)";

	public static final String VERIFY_USER = "call p_authenticate_user(?, ?, ?, ?, ?, ?, ?, ?, ?)";
	public static final String UPDATE_USER = "call p_update_user_by_id(?, ?, ?, ?, ?, ?, ?)";
	public static final String UPDATE_LOCATION = "call p_update_location_by_id(?, ?, ?, ?, ?, ?, ?, ?, ?)";
	public static final String UPDATE_ROLE = "call p_update_role_by_id(?, ?, ?)";
	public static final String UPDATE_DEPARTMENT = "call p_update_department_by_id(?, ?, ?)";
	public static final String DELETE_LOCATION = "call p_delete_location_by_id(?, ?)";
	public static final String DELETE_ROLE = "call p_delete_role_by_id(?, ?)";
	public static final String DELETE_RESOURCE_BY_ID = "call p_delete_event_resource_by_id(?, ?)";
	public static final String DELETE_DEPARTMENT = "call p_delete_department_by_id(?, ?)";
	public static final String UPDATE_TICKET = "call p_update_ticket_by_id(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	public static final String UPDATE_EVENT = "call p_update_event_by_id(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	public static final String DELETE_TICKET = "call p_delete_ticket_by_id(?, ?)";
	public static final String DELETE_EVENT="call p_delete_event_by_id(?, ?)";

	public static final String VIEW_ALL_DEPARTMENT = "select id, dept_name from v_all_departments";
	public static final String VIEW_ALL_ROLES = "select id, role_name, role_priority from v_all_roles";
	public static final String VIEW_ALL_LOCATION = "SELECT id, location_name, location_address, location_city, location_state, "
			+ "location_country, location_email, location_phone from v_all_locations";
	public static final String VIEW_ALL_TICKET = "SELECT id, ticket_name, ticket_price, ticket_quantity, ticket_start_date, ticket_start_time, ticket_end_date, "
			+ "ticket_end_time, per_ticket_reg_limit from v_all_tickets";
	public static final String VIEW_ALL_USERS = "SELECT id, first_name, last_name, dept_name, email, phone_number, "
			+ "role_name from v_all_users";
	public  static final String VIEW_ALL_EVENTS = "SELECT id, event_name, event_description,  address, city, state, country, postal_code, email, phone, "
			+ "start_date,  start_time, end_date, end_time, no_of_seats, image, status  from v_all_events";
	public static final String VIEW_ALL_EVENTS_BY_ID = "call p_view_all_events_by_id(?)";
	public static final String VIEW_ALL_RESOUCES_BY_ID = "call p_view_all_resources_by_id(?)";
	public static final String VIEW_ALL_TICKETS_BY_ID = "call p_view_all_tickets_by_id(?)";
	public static final String VIEW_ALL_ASSIGNED_RESOUCES_BY_ID = "call p_view_all_assigned_resources_by_id(?)";
	public static final String ADD_NEW_RESOURCE = "call p_add_new_resource( ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	public static final String ADD_USER_TO_RESOURCE = "call p_add_user_to_resource( ?, ?, ?, ?, ?, ?, ?, ?)";
	public static final String ASSIGN_EVENT_RESOURCES = "call p_mapping_event_resource_by_organiserId(?, ?, ?)";
	public static final String UNASSIGN_EVENT_RESOURCES = "call p_unmapping_event_resource_by_organiserId(?, ?, ?, ?)";

	public static final String ASSIGN_EVENT_TICKETS = "call p_mapping_event_ticket_by_organiserId(?, ?, ?)";
	public static final String UNASSIGN_EVENT_TICKETS = "call p_unmapping_event_ticket_by_organiserId(?, ?, ?, ?)";

	public static final String ADD_NEW_BOOKING = "call p_add_event_booking(?, ?, ?, ?, ?)";
	public static final String VIEW_ALL_BOOKING = "select id, first_name, last_name, email, event_name, booking_datetime from v_all_event_booking";
	public static final String VIEW_ALL_BOOKING_BY_ID  = "call p_view_all_event_bookings_by_userId(?)";
	public static final String CANCEL_BOOKING_BY_ID = "call p_delete_event_booking_by_id(?, ?)";
}


