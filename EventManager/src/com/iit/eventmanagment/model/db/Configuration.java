package com.iit.eventmanagment.model.db;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class Configuration {
	private static Properties properties = new Properties();
	public static final String DB_USERNAME = "dbUserName";
	public static final String DB_PASSWORD = "dbPassword";
	public static final String DB_URL 	   = "dburl";
	public static final String MAIL_SMTP_HOST = "mail.smtp.host";
	public static final String MAIL_SMTP_SOCKETFACTORY_PORT = "mail.smtp.socketFactory.port";
	public static final String MAIL_SMTP_SOCKETFACTORY_CLASS = "mail.smtp.socketFactory.class";
	public static final String MAIL_SMTP_STARTTLS_FLAG = "mail.smtp.starttls.enable";
	public static final String MAIL_SMTP_AUTH = "mail.smtp.auth";
	public static final String MAIL_SMTP_PORT = "mail.smtp.port";
	public static final String MAIL_SMTP_USER = "mailServerUserId";
	public static final String MAIL_SMTP_USER_PASS = "mailServerPassword";
	public static final String MAIL_SET_FROM = "setFromEmail";
	public static final String MAIL_SUBJECT_LINE = "mailSubjectLine";


	static {
		try (BufferedReader br = new BufferedReader(new FileReader("Config.properties"))){
			properties.load(br);
			System.out.println("Message properties Loaded Successfully");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String get(String key) {
		return properties.getProperty(key) == null ? "NA" : properties.getProperty(key);
	}
}