package com.iit.eventmanagment.model.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * This class acts as a connector for mysql database. It has mysql specific functionality to connect the database.
 *@author Harshal Deolekar
 *@version 1.0 build 1.0
 *@since November 01, 2016
 */
public class MySQLConnector extends DBConnector {
	private String jdbcURL;
	private String userName;
	private String password;

	@Override
	public void setConData(String jdbcURL, String userName, String passwd) {
		System.out.println("Connecting to database...");
		this.jdbcURL 	= jdbcURL;//jdbc:mysql://www.papademas.net:3306/510labs;
		this.userName = userName;//db510;
		this.password = passwd;//510
	}

	@Override
	public Connection getConnection() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		Connection conn = null;
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		System.out.println(jdbcURL);
		System.out.println(userName);
		System.out.println(password);
		conn = DriverManager.getConnection(jdbcURL, userName, password);
		System.out.println("Connected to database successfully");
		return conn;
	}
}