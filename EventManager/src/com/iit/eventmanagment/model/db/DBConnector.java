package com.iit.eventmanagment.model.db;

import java.sql.Connection;


/**
 * This abstract class provides connector for database.
 *@author Harshal Deolekar
 *@version 1.0 build 1.0
 *@since November 01, 2016
 */
public abstract class DBConnector {
	public static DBConnector getDBConnector() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		DBConnector 	dbConnector 	= null;
		Class<? extends DBConnector> c	= null;
		String dbClassName	 			= "";
		try {
			// fetchs the connector from the database configuration file
			dbClassName = Configuration.get("DB_CONNECTOR_FULL_CLASS_NAME");
			// loads the database specific connector object. e.g. MySQLConnector, OracleConnector, DB2Connector
			c = Class.forName(dbClassName).asSubclass(com.iit.eventmanagment.model.db.DBConnector.class);
			// constructs the connector
			dbConnector = (DBConnector) c.newInstance();
			// sets the connection details to the connector object
			dbConnector.setConData(Configuration.get("JDBC_URL"), Configuration.get("DB_USER_NAME"),
					Configuration.get("DB_PWD"));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw e;
		} catch (InstantiationException e) {
			e.printStackTrace();
			throw e;
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			throw e;
		}
		return dbConnector;
	}

	/**
	 * This method is used to set the connection parameters
	 * @param jdbcURL database server url
	 * @param userName database username
	 * @param password database password
	 */
	public abstract void setConData(String jdbcURL, String userName, String password);

	/**
	 * This method is used to get database connection
	 * @throws Exception if error while connecting to the database
	 */
	public abstract Connection getConnection() throws Exception;
}
