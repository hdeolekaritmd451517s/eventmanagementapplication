package com.iit.eventmanagment.model;

import java.util.ArrayList;
import java.util.List;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Department POJO
  * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class Department {
	private IntegerProperty id;
	private StringProperty departmentName;
	private List<Users> users = new ArrayList<Users>();

	public Department(int id, String departmentName) {
		super();
		this.id = new SimpleIntegerProperty(id);
		this.departmentName = new SimpleStringProperty(departmentName);
	}

	public Department(String departmentName) {
		super();
		this.id = new SimpleIntegerProperty(0);
		this.departmentName = new SimpleStringProperty(departmentName);
	}

	public Department() {
		super();
		this.id = new SimpleIntegerProperty(0);
		this.departmentName = new SimpleStringProperty();
	}

	public int getId() {
		return id.get();
	}
	public IntegerProperty departmentIdProperty() {
		return this.id;
	}
	public void setId(int id) {
		this.id.set(id);
	}
	public String getDepartmentName() {
		return departmentName.get();
	}
	public StringProperty departmentNameProperty() {
		return this.departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName.set(departmentName);
	}
	public List<Users> getUsers() {
		return users;
	}
	public void setUsers(List<Users> users) {
		this.users = users;
	}
	@Override
	public String toString() {
		return departmentName.get();
	}
}
