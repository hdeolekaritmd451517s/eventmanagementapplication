package com.iit.eventmanagment.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Location POJO. Contains all the location details.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class Location {
	private IntegerProperty id;
	private StringProperty locationName;
	private StringProperty locationAddress;
	private StringProperty locationCity;
	private StringProperty locationState;
	private StringProperty locationCountry;
	private StringProperty locationEmail;
	private StringProperty locationPhone;


	public Location(int id, String locationName, String locationAddress,
			String locationCity, String locationState, String locationCountry,
			String locationEmail, String locationPhone) {
		super();
		this.id = new SimpleIntegerProperty(id);
		this.locationName = new SimpleStringProperty(locationName);
		this.locationAddress = new SimpleStringProperty(locationAddress);
		this.locationCity = new SimpleStringProperty(locationCity);
		this.locationState = new SimpleStringProperty(locationState);
		this.locationCountry = new SimpleStringProperty(locationCountry);
		this.locationEmail = new SimpleStringProperty(locationEmail);
		this.locationPhone = new SimpleStringProperty(locationPhone);
	}

	public Location(String locationName, String locationAddress,
			String locationCity, String locationState, String locationCountry,
			String locationEmail, String locationPhone) {
		super();
		this.id = new SimpleIntegerProperty(0);
		this.locationName = new SimpleStringProperty(locationName);
		this.locationAddress = new SimpleStringProperty(locationAddress);
		this.locationCity = new SimpleStringProperty(locationCity);
		this.locationState = new SimpleStringProperty(locationState);
		this.locationCountry = new SimpleStringProperty(locationCountry);
		this.locationEmail = new SimpleStringProperty(locationEmail);
		this.locationPhone = new SimpleStringProperty(locationPhone);
	}

	public Location() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id.get();
	}

	public final void setId(int id) {
		this.id.set(id);
	}

	public IntegerProperty locationIDProperty(){
		return id;
	}

	public String getLocationName() {
		return locationName.get();
	}

	public void setLocationName(String locationName) {
		this.locationName.set(locationName);
	}

	public StringProperty locationNameProperty(){
		return locationName;
	}

	public String getLocationAddress() {
		return locationAddress.get();
	}

	public void setLocationAddress(String locationAddress) {
		this.locationAddress.set(locationAddress);
	}

	public StringProperty locationAddressProperty(){
		return locationAddress;
	}

	public String getLocationCity() {
		return locationCity.get();
	}

	public void setLocationCity(String locationCity) {
		this.locationCity.set(locationCity);
	}

	public StringProperty locationCityProperty(){
		return locationCity;
	}

	public String getLocationState() {
		return locationState.get();
	}
	public void setLocationState(String locationState) {
		this.locationState.set(locationState);
	}

	public StringProperty locationStateProperty(){
		return locationState;
	}

	public String getLocationCountry() {
		return locationCountry.get();
	}
	public void setLocationCountry(String locationCountry) {
		this.locationCountry.set(locationCountry);
	}

	public StringProperty locationCountryProperty(){
		return locationCountry;
	}

	public String getLocationEmail() {
		return locationEmail.get();
	}
	public void setLocationEmail(String locationEmail) {
		this.locationEmail.set(locationEmail);
	}

	public StringProperty locationEmailProperty(){
		return locationEmail;
	}

	public String getLocationPhone() {
		return locationPhone.get();
	}
	public void setLocationPhone(String locationPhone) {
		this.locationPhone.set(locationPhone);
	}

	public StringProperty locationPhoneProperty(){
		return locationPhone;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Location [id=");
		builder.append(id);
		builder.append(", locationName=");
		builder.append(locationName);
		builder.append(", locationAddress=");
		builder.append(locationAddress);
		builder.append(", locationCity=");
		builder.append(locationCity);
		builder.append(", locationState=");
		builder.append(locationState);
		builder.append(", locationCountry=");
		builder.append(locationCountry);
		builder.append(", locationEmail=");
		builder.append(locationEmail);
		builder.append(", locationPhone=");
		builder.append(locationPhone);
		builder.append("]");
		return builder.toString();
	}
}
