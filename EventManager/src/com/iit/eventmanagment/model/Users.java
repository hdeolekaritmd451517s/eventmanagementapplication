package com.iit.eventmanagment.model;

import java.sql.Date;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
/**
 * 	Users POJO. Contains all the users details
  * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class Users {
	private IntegerProperty id;
	private StringProperty firstName;
	private StringProperty lastName;
	private ObjectProperty<Department> department;
	private StringProperty email;
	private StringProperty phoneNumber;
	private StringProperty password;
	private ObjectProperty<Date> registrationDate;
	private ObjectProperty<Date> lastUpdatedDate;
	private ObjectProperty<Date> lastLoginDate;
	private ObjectProperty<Roles> userRole;
	private StringProperty status;

	public Users() {
		this.id = new SimpleIntegerProperty();
		this.firstName = new SimpleStringProperty();
		this.lastName = new SimpleStringProperty();
		this.department = new SimpleObjectProperty<Department>();
		this.email = new SimpleStringProperty();
		this.phoneNumber = new SimpleStringProperty();
		this.userRole = new SimpleObjectProperty<Roles>();
		this.password =	new SimpleStringProperty();
	}
	public Users(int id, String firstName, String lastName, Department department, String email, String phoneNumber, Roles userRole) {
		this.id = new SimpleIntegerProperty(id);
		this.firstName = new SimpleStringProperty(firstName);
		this.lastName = new SimpleStringProperty(lastName);
		this.department = new SimpleObjectProperty<Department>(department);
		this.email = new SimpleStringProperty(email);
		this.phoneNumber = new SimpleStringProperty(phoneNumber);
		this.userRole = new SimpleObjectProperty<Roles>(userRole);
		this.password =	new SimpleStringProperty();
	}
	public Users(String firstName, String lastName, Department department, String email, String phoneNumber, Roles userRole) {
		this.id = new SimpleIntegerProperty(0);
		this.firstName = new SimpleStringProperty(firstName);
		this.lastName = new SimpleStringProperty(lastName);
		this.department = new SimpleObjectProperty<Department>(department);
		this.email = new SimpleStringProperty(email);
		this.phoneNumber = new SimpleStringProperty(phoneNumber);
		this.userRole = new SimpleObjectProperty<Roles>(userRole);
		this.password =	new SimpleStringProperty();
	}
	public int getId() {
		return id.get();
	}
	public void setId(int id) {
		this.id.set(id);
	}
	public IntegerProperty userIDProperty(){
		return this.id;
	}
	public String getFirstName() {
		return firstName.get();
	}
	public void setFirstName(String firstName) {
		this.firstName.set(firstName);
	}
	public StringProperty userFirstNameProperty(){
		return this.firstName;
	}
	public String getLastName() {
		return lastName.get();
	}
	public void setLastName(String lastName) {
		this.lastName.set(lastName);
	}
	public StringProperty userLastNameProperty(){
		return lastName;
	}
	public Department getDepartment() {
		return department.get();
	}
	public void setDepartment(Department department) {
		this.department.set(department);
	}
	public ObjectProperty<Department> userDepartmentProperty(){
		return this.department;
	}
	public String getEmail() {
		return email.get();
	}
	public void setEmail(String email) {
		this.email.set(email);
	}
	public StringProperty userEmailProperty(){
		return this.email;
	}
	public String getPhoneNumber() {
		return phoneNumber.get();
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber.set(phoneNumber);
	}
	public StringProperty userPhoneNumberProperty(){
		return this.phoneNumber;
	}
	public String getPassword() {
		return password.get();
	}
	public StringProperty passwordProperty() {
		return this.password;
	}
	public void setPassword(String password) {
		this.password.set(password);;
	}
	public Date getRegistrationDate() {
		return registrationDate.get();
	}
	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate.set(registrationDate);
	}
	public Date getLastUpdatedDate() {
		return lastUpdatedDate.get();
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate.set(lastUpdatedDate);
	}
	public Date getLastLoginDate() {
		return lastLoginDate.get();
	}
	public void setLastLoginDate(Date lastLoginDate) {
		this.lastLoginDate.set(lastLoginDate);
	}
	public Roles getRole() {
		return userRole.get();
	}
	public void setRole(Roles role) {
		this.userRole.set(role);
	}
	public ObjectProperty<Roles> userRoleProperty() {
		return this.userRole;
	}

	public String getStatus() {
		return status.get();
	}
	public void setStatus(String status) {
		this.status.set(status);
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(firstName.get());
		builder.append(" "+lastName.get());
		return builder.toString();
	}
}
