package com.iit.eventmanagment.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

/**
 * 	Event POJO. Contains all the event details
  * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class Event {
	private IntegerProperty id;
	private StringProperty name;
	private StringProperty description;
	private StringProperty eventLocation;
	private StringProperty eventAddress;
	private StringProperty eventCity;
	private StringProperty eventState;
	private StringProperty eventCountry;
	private StringProperty eventPostalCode;
	private StringProperty eventEmail;
	private StringProperty eventPhone;
	private ObjectProperty<LocalDateTime> eventStartDateTime;
	private ObjectProperty<LocalDate> eventStartDate;
	private ObjectProperty<LocalTime> eventStartTime;
	private ObjectProperty<LocalDateTime> eventEndDateTime;
	private ObjectProperty<LocalDate>  eventEndDate;
	private ObjectProperty<LocalTime> eventEndTime;
	private IntegerProperty noOfSeats;
	private ObjectProperty<byte[]> eventImage;
	private StringProperty eventStatus;
	private List<Users> eventResources = new ArrayList<Users>();
	private List<Ticket> eventTickets = new ArrayList<Ticket>();

	public Event(int id, String name, String description, String eventAddress, String eventCity,
			String eventState, String eventCountry, String eventPostalCode,
			String eventEmail, String eventPhone, LocalDate eventStartDate,
			LocalTime eventStartTime, LocalDate eventEndDate,
			LocalTime eventEndTime, int noOfSeats, byte[] eventImage) {
		super();
		this.id = new SimpleIntegerProperty(id);
		this.name = new SimpleStringProperty(name);
		this.description =  new SimpleStringProperty(description) ;
		this.eventAddress = new SimpleStringProperty(eventAddress) ;
		this.eventCity =  new SimpleStringProperty(eventCity);
		this.eventState =  new SimpleStringProperty(eventState);
		this.eventCountry = new SimpleStringProperty(eventCountry);
		this.eventPostalCode = new SimpleStringProperty(eventPostalCode);
		this.eventLocation =  new SimpleStringProperty(this.eventAddress.get()+"\n"+this.eventCity.get()+"-"+this.eventPostalCode.get()+", \n"+this.eventState.get()+", "+this.eventCountry.get());
		this.eventEmail = new SimpleStringProperty(eventEmail);
		this.eventPhone = new SimpleStringProperty(eventPhone);
		this.eventStartDate = new SimpleObjectProperty<LocalDate>(eventStartDate);
		this.eventStartTime = new SimpleObjectProperty<LocalTime>(eventStartTime);
		this.eventEndDate = new SimpleObjectProperty<LocalDate>(eventEndDate);
		this.eventEndTime = new SimpleObjectProperty<LocalTime>(eventEndTime);
		this.eventStartDateTime = new SimpleObjectProperty<LocalDateTime>(LocalDateTime.of(eventStartDate, eventStartTime));
		this.eventEndDateTime = new SimpleObjectProperty<LocalDateTime>(LocalDateTime.of(eventEndDate, eventEndTime));
		this.noOfSeats = new SimpleIntegerProperty(noOfSeats);
		this.eventImage = new SimpleObjectProperty<byte[]>(eventImage);
	}

	public Event(int id, String name, String description, String eventAddress, String eventCity,
			String eventState, String eventCountry, String eventPostalCode,
			String eventEmail, String eventPhone, LocalDate eventStartDate,
			LocalTime eventStartTime, LocalDate eventEndDate,
			LocalTime eventEndTime, int noOfSeats, byte[] eventImage, String eventStatus) {
		super();
		this.id = new SimpleIntegerProperty(id);
		this.name = new SimpleStringProperty(name);
		this.description =  new SimpleStringProperty(description) ;
		this.eventAddress = new SimpleStringProperty(eventAddress) ;
		this.eventCity =  new SimpleStringProperty(eventCity);
		this.eventState =  new SimpleStringProperty(eventState);
		this.eventCountry = new SimpleStringProperty(eventCountry);
		this.eventPostalCode = new SimpleStringProperty(eventPostalCode);
		this.eventLocation =  new SimpleStringProperty(this.eventAddress.get()+"\n"+this.eventCity.get()+"-"+this.eventPostalCode.get()+", \n"+this.eventState.get()+", "+this.eventCountry.get());
		this.eventEmail = new SimpleStringProperty(eventEmail);
		this.eventPhone = new SimpleStringProperty(eventPhone);
		this.eventStartDate = new SimpleObjectProperty<LocalDate>(eventStartDate);
		this.eventStartTime = new SimpleObjectProperty<LocalTime>(eventStartTime);
		this.eventEndDate = new SimpleObjectProperty<LocalDate>(eventEndDate);
		this.eventEndTime = new SimpleObjectProperty<LocalTime>(eventEndTime);
		this.eventStartDateTime = new SimpleObjectProperty<LocalDateTime>(LocalDateTime.of(eventStartDate, eventStartTime));
		this.eventEndDateTime = new SimpleObjectProperty<LocalDateTime>(LocalDateTime.of(eventEndDate, eventEndTime));
		this.noOfSeats = new SimpleIntegerProperty(noOfSeats);
		this.eventImage = new SimpleObjectProperty<byte[]>(eventImage);
		this.eventStatus = new SimpleStringProperty(eventStatus);
	}

	public Event(String name, String description,String eventAddress, String eventCity,
			String eventState, String eventCountry, String eventPostalCode,
			String eventEmail, String eventPhone, LocalDate eventStartDate,
			LocalTime eventStartTime, LocalDate eventEndDate,
			LocalTime eventEndTime, int noOfSeats, byte[] eventImage) {
		super();
		this.id = new SimpleIntegerProperty(0);
		this.name = new SimpleStringProperty(name);
		this.description =  new SimpleStringProperty(description) ;
		this.eventAddress = new SimpleStringProperty(eventAddress) ;
		this.eventCity =  new SimpleStringProperty(eventCity);
		this.eventState =  new SimpleStringProperty(eventState);
		this.eventCountry = new SimpleStringProperty(eventCountry);
		this.eventPostalCode = new SimpleStringProperty(eventPostalCode);
		this.eventLocation =  new SimpleStringProperty(this.eventAddress.get()+",\n"+this.eventCity.get()+"-"+this.eventPostalCode.get()+", \n"+this.eventState.get()+", "+this.eventCountry.get());
		this.eventEmail = new SimpleStringProperty(eventEmail);
		this.eventPhone = new SimpleStringProperty(eventPhone);
		this.eventStartDate = new SimpleObjectProperty<LocalDate>(eventStartDate);
		this.eventStartTime = new SimpleObjectProperty<LocalTime>(eventStartTime);
		this.eventEndDate = new SimpleObjectProperty<LocalDate>(eventEndDate);
		this.eventEndTime = new SimpleObjectProperty<LocalTime>(eventEndTime);
		this.eventStartDateTime = new SimpleObjectProperty<LocalDateTime>(LocalDateTime.of(eventStartDate, eventStartTime));
		this.eventEndDateTime = new SimpleObjectProperty<LocalDateTime>(LocalDateTime.of(eventEndDate, eventEndTime));
		this.noOfSeats = new SimpleIntegerProperty(noOfSeats);
		this.eventImage = new SimpleObjectProperty<byte[]>(eventImage);
	}

	public Event() {
		super();
		this.id = new SimpleIntegerProperty(0);
		this.name = new SimpleStringProperty();
	}

	public int getId() {
		return id.get();
	}

	public IntegerProperty eventIDProperty() {
		return this.id;
	}

	public void setId(int id) {
		this.id.set(id);
	}


	public String getName() {
		return name.get();
	}

	public StringProperty eventNameProperty() {
		return this.name;
	}

	public void setName(String name) {
		this.name.set(name);
	}


	public String getDescription() {
		return description.get();
	}

	public StringProperty eventDescriptionProperty() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description.set(description);
	}

	public String getEventAddress() {
		return eventAddress.get();
	}

	public StringProperty eventAddressProperty() {
		return this.eventAddress;
	}

	public void setEventAddress(String eventAddress) {
		this.eventAddress.set(eventAddress);
	}

	public String getEventCity() {
		return eventCity.get();
	}

	public StringProperty eventCityProperty() {
		return this.eventCity;
	}

	public void setEventCity(String eventCity) {
		this.eventCity.set(eventCity);
	}

	public String getEventState() {
		return eventState.get();
	}

	public StringProperty eventStateProperty() {
		return this.eventState;
	}

	public void setEventState(String eventState) {
		this.eventState.set(eventState);
	}

	public String getEventCountry() {
		return eventCountry.get();
	}

	public StringProperty eventCountryProperty() {
		return this.eventCountry;
	}

	public void setEventCountry(String eventCountry) {
		this.eventCountry.set(eventCountry);
	}

	public String getEventPostalCode() {
		return eventPostalCode.get();
	}

	public StringProperty eventPostalCodeProperty() {
		return this.eventPostalCode;
	}

	public void setEventPostalCode(String eventPostalCode) {
		this.eventPostalCode.set(eventPostalCode);
	}

	public String getEventLocation() {
		return eventLocation.get();
	}

	public StringProperty getEventLocationProperty() {
		return this.eventLocation;
	}

	public void setEventLocation(String eventLocation) {
		this.eventLocation.set(eventLocation);;
	}

	public String getEventEmail() {
		return eventEmail.get();
	}

	public StringProperty eventEmailProperty() {
		return this.eventEmail;
	}

	public void setEventEmail(String eventEmail) {
		this.eventEmail.set(eventEmail);
	}

	public String getEventPhone() {
		return eventPhone.get();
	}

	public StringProperty eventPhoneProperty() {
		return this.eventPhone;
	}

	public void setEventPhone(String eventPhone) {
		this.eventPhone.set(eventPhone);
	}

	public LocalDate getEventStartDate() {
		return eventStartDate.get();
	}

	public ObjectProperty<LocalDate> eventStartDateProperty() {
		return this.eventStartDate;
	}

	public void setEventStartDate(LocalDate eventStartDate) {
		this.eventStartDate.set(eventStartDate);
	}


	public LocalDate getEventEndDate() {
		return eventEndDate.get();
	}

	public ObjectProperty<LocalDate> eventEndDateProperty() {
		return this.eventEndDate;
	}

	public void setEventEndDate(LocalDate eventEndDate) {
		this.eventEndDate.set(eventEndDate);
	}

	//====================================================
	public LocalTime getEventStartTime() {
		return eventStartTime.get();
	}

	public ObjectProperty<LocalTime> eventStartTimeProperty() {
		return this.eventStartTime;
	}

	public void setEventStartTime(LocalTime eventStartTime) {
		this.eventStartTime.set(eventStartTime);
	}


	public LocalTime getEventEndTime() {
		return eventEndTime.get();
	}

	public ObjectProperty<LocalTime> eventEndTimeProperty() {
		return this.eventEndTime;
	}

	public void setEventEndTime(LocalTime eventEndTime) {
		this.eventEndTime.set(eventEndTime);
	}
	//==============datetime==========================

	public LocalDateTime getEventStartDateTime() {
		return eventStartDateTime.get();
	}

	public ObjectProperty<LocalDateTime> getEventStartDateTimeProperty() {
		return this.eventStartDateTime;
	}

	public void setEventStartDateTime(LocalDateTime eventStartDateTime) {
		this.eventStartDateTime.set(eventStartDateTime);
	}
//===========
	public ObjectProperty<LocalDateTime> getEventEndDateTimeProperty() {
		return this.eventEndDateTime;
	}

	public void setEventEndDateTime(LocalDateTime eventEndDateTime) {
		this.eventEndDateTime.set(eventEndDateTime);
	}

	public ObjectProperty<LocalDateTime> getEventEndDateTime() {
		return eventEndDateTime;
	}

	public int getNoOfSeats() {
		return noOfSeats.get();
	}

	public IntegerProperty noOfSeatsIntegerProperty() {
		return this.noOfSeats;
	}

	public void setNoOfSeats(int noOfSeats) {
		this.noOfSeats.set(noOfSeats);
	}

	public byte[] getEventImage() {
		return eventImage.get();
	}

	public  ObjectProperty<byte[]> eventImageProperty() {
		return this.eventImage;
	}

	public void setEventImage(byte[] eventImage) {
		this.eventImage.set(eventImage);
	}

	public String getEventStatus() {
		return eventStatus.get();
	}

	public StringProperty eventStatusProperty() {
		return this.eventStatus;
	}

	public void setEventStatus(String eventStatus) {
		this.eventStatus.set(eventStatus);
	}

	public List<Users> getEventResources() {
		return eventResources;
	}

	public void setEventResources(List<Users> eventResources) {
		this.eventResources = eventResources;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(name.get());
		return builder.toString();
	}

	public List<Ticket> getEventTickets() {
		return eventTickets;
	}

	public void setEventTicket(ObservableList<Ticket> eventTickets) {
		this.eventTickets = eventTickets;
	}
}
