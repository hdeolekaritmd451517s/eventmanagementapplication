package com.iit.eventmanagment.service.impl;

import java.util.List;

import com.iit.eventmanagment.dao.EventDAO;
import com.iit.eventmanagment.dao.impl.EventDAOImpl;
import com.iit.eventmanagment.model.Event;
import com.iit.eventmanagment.model.Users;
import com.iit.eventmanagment.service.EventService;
/**
 *
 * This has same methods as DAO interfaces. This layer is added with respect to feature scope.
 * Any business processing which is required should be done in this layer.
 * It should not be done in DAO layer nor in the Controller Layer.
 * DAO layers should only interact with the database and controllers with the views.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class EventServiceImpl implements EventService{
	EventDAO eventDAO = new EventDAOImpl();

	@Override
	public Event addEvent(Event event, Users loggedInUser) throws Exception {
		return eventDAO.addEvent(event, loggedInUser);
	}

	@Override
	public boolean updateEvent(Event event) throws Exception {
		return eventDAO.updateEvent(event);
	}

	@Override
	public boolean deleteEvent(Event event) throws Exception {
		return eventDAO.deleteEvent(event);
	}

	@Override
	public List<Event> viewAllEvent() throws Exception {
		return eventDAO.viewAllEvent();
	}

	@Override
	public List<Event> viewAllEventByUserId(Users loggedInUser) throws Exception {
		return eventDAO.viewAllEventByUserId(loggedInUser);
	}
}
