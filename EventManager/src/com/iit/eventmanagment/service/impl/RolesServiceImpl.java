package com.iit.eventmanagment.service.impl;

import java.util.List;

import com.iit.eventmanagment.dao.RolesDAO;
import com.iit.eventmanagment.dao.impl.RolesDAOImpl;
import com.iit.eventmanagment.model.Roles;
/**
 *
 * This has same methods as DAO interfaces. This layer is added with respect to feature scope.
 * Any business processing which is required should be done in this layer.
 * It should not be done in DAO layer nor in the Controller Layer.
 * DAO layers should only interact with the database and controllers with the views.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class RolesServiceImpl implements com.iit.eventmanagment.service.RolesService {
	RolesDAO rolesDAO = new RolesDAOImpl();
	@Override
	public Roles addRole(Roles role) throws Exception {
		return rolesDAO.addRole(role);
	}

	@Override
	public boolean updateRole(Roles role) throws Exception {
		return  rolesDAO.updateRole(role);
	}

	@Override
	public boolean deleteRole(Roles role) throws Exception {
		return rolesDAO.deleteRole(role);
	}

	@Override
	public List<Roles> viewAllRoles() throws Exception {
		return rolesDAO.viewAllRoles();
	}

}
