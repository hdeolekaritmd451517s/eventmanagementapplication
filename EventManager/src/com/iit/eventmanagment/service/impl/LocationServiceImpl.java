package com.iit.eventmanagment.service.impl;

import java.util.List;

import com.iit.eventmanagment.dao.LocationDAO;
import com.iit.eventmanagment.dao.impl.LocationDAOImpl;
import com.iit.eventmanagment.model.Location;
import com.iit.eventmanagment.service.LocationService;
/**
 *
 * This has same methods as DAO interfaces. This layer is added with respect to feature scope.
 * Any business processing which is required should be done in this layer.
 * It should not be done in DAO layer nor in the Controller Layer.
 * DAO layers should only interact with the database and controllers with the views.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class LocationServiceImpl implements LocationService {
	LocationDAO locationDAO = new LocationDAOImpl();

	@Override
	public Location addLocation(Location location) throws Exception {
		return locationDAO.addLocation(location);
	}

	@Override
	public boolean updateLocation(Location location) throws Exception {
		return locationDAO.updateLocation(location);
	}

	@Override
	public boolean deleteLocation(Location location) throws Exception {
		return locationDAO.deleteLocation(location);
	}

	@Override
	public List<Location> viewAllLocation() throws Exception {
		return locationDAO.viewAllLocation();
	}

}
