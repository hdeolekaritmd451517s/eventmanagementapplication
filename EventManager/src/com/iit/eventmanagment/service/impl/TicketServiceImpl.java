package com.iit.eventmanagment.service.impl;

import java.util.List;
import com.iit.eventmanagment.dao.TicketDAO;
import com.iit.eventmanagment.dao.impl.TicketDAOImpl;
import com.iit.eventmanagment.model.Event;
import com.iit.eventmanagment.model.Ticket;
import com.iit.eventmanagment.model.Users;
import com.iit.eventmanagment.service.TicketService;
/**
 *
 * This has same methods as DAO interfaces. This layer is added with respect to feature scope.
 * Any business processing which is required should be done in this layer.
 * It should not be done in DAO layer nor in the Controller Layer.
 * DAO layers should only interact with the database and controllers with the views.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class TicketServiceImpl implements TicketService {
	TicketDAO ticketDAO = new TicketDAOImpl();
	@Override
	public Ticket addTicket(Ticket ticket, Users loggedInUser) throws Exception {
		return ticketDAO.addTicket(ticket, loggedInUser);
	}

	@Override
	public List<Ticket> viewAllTicket() throws Exception {
		return ticketDAO.viewAllTicket();
	}

	@Override
	public boolean updateTicket(Ticket ticket) throws Exception {
		return ticketDAO.updateTicket(ticket);
	}

	@Override
	public boolean deleteTicket(Ticket ticket) throws Exception {
		return ticketDAO.deleteTicket(ticket);
	}

	@Override
	public boolean assignEventTicketsById(Users loggedInUser, Event event) throws Exception {
		return ticketDAO.assignEventTicketsById(loggedInUser, event);
	}

	@Override
	public boolean unAssignEventTicketsById(Users loggedInUser, Event selectedEvent, Ticket selectedTicket)
			throws Exception {
		return ticketDAO.unAssignEventTicketsById(loggedInUser, selectedEvent, selectedTicket);
	}

	@Override
	public List<Ticket> viewAllTicketsById(Users loggedInUser) throws Exception {
		return ticketDAO.viewAllTicketsById(loggedInUser);
	}

}
