package com.iit.eventmanagment.service.impl;

import java.util.List;

import com.iit.eventmanagment.dao.UserDAO;
import com.iit.eventmanagment.dao.impl.UserDAOImpl;
import com.iit.eventmanagment.model.Users;
import com.iit.eventmanagment.service.UserService;
/**
 *
 * This has same methods as DAO interfaces. This layer is added with respect to feature scope.
 * Any business processing which is required should be done in this layer.
 * It should not be done in DAO layer nor in the Controller Layer.
 * DAO layers should only interact with the database and controllers with the views.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class UserServiceImpl implements UserService {
	UserDAO userDAO = new UserDAOImpl();
	@Override
	public boolean addUser(Users user) throws Exception {
		return userDAO.addUser(user);
	}

	@Override
	public boolean updateUser(Users user) throws Exception {
		return userDAO.updateUser(user);
	}

	@Override
	public boolean deleteUser(Users user) {

		return false;
	}

	@Override
	public List<Users> viewAllUser() throws Exception {
		return userDAO.viewAllUser();
	}
}
