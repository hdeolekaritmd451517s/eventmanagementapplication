package com.iit.eventmanagment.service.impl;

import java.util.List;

import com.iit.eventmanagment.dao.ResourceDAO;
import com.iit.eventmanagment.dao.impl.ResourceDAOImpl;
import com.iit.eventmanagment.model.Event;
import com.iit.eventmanagment.model.Users;
import com.iit.eventmanagment.service.ResourceService;
/**
 *
 * This has same methods as DAO interfaces. This layer is added with respect to feature scope.
 * Any business processing which is required should be done in this layer.
 * It should not be done in DAO layer nor in the Controller Layer.
 * DAO layers should only interact with the database and controllers with the views.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class ResourceServiceImpl implements ResourceService {
	ResourceDAO resourceDAO = new ResourceDAOImpl();

	@Override
	public Users addResource(Users user, Users loggedInUser) throws Exception {
		return resourceDAO.addResource(user, loggedInUser);
	}

	@Override
	public boolean updateAndAddResource(Users user, Users loggedInUser) throws Exception {
		return resourceDAO.updateAndAddResource(user, loggedInUser);
	}

	@Override
	public List<Users> viewAllResourcesById(Users loggedInUser) throws Exception {
		return resourceDAO.viewAllResourcesById(loggedInUser);
	}

	@Override
	public boolean deleteResourceById(Users currentResource) throws Exception {
		return resourceDAO.deleteResourceById(currentResource);
	}

	@Override
	public boolean assignEventResourcesById(Users loggedInUser, Event event) throws Exception {
		return resourceDAO.assignEventResourcesById(loggedInUser, event);
	}

	@Override
	public boolean unAssignEventResourcesById(Users loggedInUser, Event selectedEvent, Users selectedUser)
			throws Exception {
		return resourceDAO.unAssignEventResourcesById(loggedInUser, selectedEvent, selectedUser);
	}
}
