package com.iit.eventmanagment.service.impl;

import java.util.List;

import com.iit.eventmanagment.dao.DepartmentDAO;
import com.iit.eventmanagment.dao.impl.DepartmentDAOImpl;
import com.iit.eventmanagment.model.Department;
import com.iit.eventmanagment.service.DepartmentService;
/**
 *
 * This has same methods as DAO interfaces. This layer is added with respect to feature scope.
 * Any business processing which is required should be done in this layer.
 * It should not be done in DAO layer nor in the Controller Layer.
 * DAO layers should only interact with the database and controllers with the views.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class DepartmentServiceImpl implements DepartmentService{
	DepartmentDAO departmentDAO = new DepartmentDAOImpl();
	@Override
	public Department addDepartment(Department department) throws Exception {
		return departmentDAO.addDepartment(department);
	}

	@Override
	public boolean updateDepartment(Department department) throws Exception {
		return departmentDAO.updateDepartment(department);
	}

	@Override
	public boolean deleteDepartment(Department department) throws Exception {
		return departmentDAO.deleteDepartment(department);
	}

	@Override
	public List<Department> viewAllDepartment() throws Exception {
		return departmentDAO.viewAllDepartment();
	}

}
