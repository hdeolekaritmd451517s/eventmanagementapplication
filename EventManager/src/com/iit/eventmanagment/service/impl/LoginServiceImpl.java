package com.iit.eventmanagment.service.impl;

import com.iit.eventmanagment.dao.LoginDAO;
import com.iit.eventmanagment.dao.impl.LoginDAOImpl;
import com.iit.eventmanagment.model.Login;
import com.iit.eventmanagment.model.Users;
import com.iit.eventmanagment.service.LoginService;
/**
 *
 * This has same methods as DAO interfaces. This layer is added with respect to feature scope.
 * Any business processing which is required should be done in this layer.
 * It should not be done in DAO layer nor in the Controller Layer.
 * DAO layers should only interact with the database and controllers with the views.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class LoginServiceImpl implements LoginService{
	LoginDAO loginDAO = new LoginDAOImpl();
	@Override
	public Users verifyUser(Login login) throws Exception {
		return loginDAO.verifyUser(login);
	}
}
