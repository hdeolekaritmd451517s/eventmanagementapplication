
package com.iit.eventmanagment.service.impl;

import java.util.List;

import com.iit.eventmanagment.dao.BookEventDAO;
import com.iit.eventmanagment.dao.impl.BookEventDAOImpl;
import com.iit.eventmanagment.model.BookEvent;
import com.iit.eventmanagment.model.Users;
import com.iit.eventmanagment.service.BookingService;
/**
 *
 * This has same methods as DAO implementation layer. This layer is added with respect to feature scope.
 * Any business processing which is required should be done in this layer.
 * It should not be done in DAO layer nor in the Controller Layer.
 * DAO layers should only interact with the database and controllers with the views.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class BookingServiceImpl implements BookingService {
	BookEventDAO bookEventDAO = new BookEventDAOImpl();

	@Override
	public BookEvent addBooking(Users loggedInUser, BookEvent bookedEvent) throws Exception {
		return bookEventDAO.addBooking(loggedInUser, bookedEvent);
	}

	@Override
	public List<BookEvent> viewAllBookingById(Users loggedInUser) throws Exception {
		return bookEventDAO.viewAllBookingById(loggedInUser);
	}

	@Override
	public List<BookEvent> viewAllBooking() throws Exception {
		return bookEventDAO.viewAllBooking();
	}

	@Override
	public boolean deleteBookingById(BookEvent bookedEvent) throws Exception {
		return bookEventDAO.deleteBookingById(bookedEvent);
	}
}
