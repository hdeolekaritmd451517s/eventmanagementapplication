package com.iit.eventmanagment.service;

import java.util.List;

import com.iit.eventmanagment.model.Event;
import com.iit.eventmanagment.model.Ticket;
import com.iit.eventmanagment.model.Users;
/**
 *
 * This has same methods as DAO interfaces. This layer is added with respect to feature scope.
 * Any business processing which is required should be done in this layer.
 * It should not be done in DAO layer nor in the Controller Layer.
 * DAO layers should only interact with the database and controllers with the views.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public interface TicketService {
	public abstract Ticket addTicket(Ticket ticket, Users loggedInUser) throws Exception;
	public abstract List<Ticket> viewAllTicket() throws Exception;
	public abstract boolean updateTicket(Ticket ticket) throws Exception;
	public abstract boolean deleteTicket(Ticket ticket) throws Exception;
	public abstract boolean assignEventTicketsById(Users loggedInUser, Event event) throws Exception;
	public abstract boolean unAssignEventTicketsById(Users loggedInUser, Event selectedEvent, Ticket selectedTicket) throws Exception;
	public abstract List<Ticket> viewAllTicketsById(Users loggedInUser) throws Exception;
}
