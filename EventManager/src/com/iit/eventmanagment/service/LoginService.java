package com.iit.eventmanagment.service;

import com.iit.eventmanagment.model.Login;
import com.iit.eventmanagment.model.Users;
/**
 *
 * This has same methods as DAO interfaces. This layer is added with respect to feature scope.
 * Any business processing which is required should be done in this layer.
 * It should not be done in DAO layer nor in the Controller Layer.
 * DAO layers should only interact with the database and controllers with the views.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public interface LoginService {
	public abstract Users verifyUser(Login login) throws Exception;
}
