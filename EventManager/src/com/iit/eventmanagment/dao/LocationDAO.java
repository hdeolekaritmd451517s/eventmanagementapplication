package com.iit.eventmanagment.dao;

import java.util.List;

import com.iit.eventmanagment.model.Location;
/**
 * This is interface for locations. It allows to add locations, view locations, update and delete locations.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public interface LocationDAO {
	public abstract Location addLocation(Location location) throws Exception;
	public abstract List<Location> viewAllLocation() throws Exception;
	public abstract boolean updateLocation(Location location) throws Exception;
	public abstract boolean deleteLocation(Location location) throws Exception;
}
