package com.iit.eventmanagment.dao;

import com.iit.eventmanagment.model.Login;
import com.iit.eventmanagment.model.Users;
/**
 * This is interface for login. It verifies the user.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public interface LoginDAO {
	public abstract Users verifyUser(Login login) throws Exception;
}
