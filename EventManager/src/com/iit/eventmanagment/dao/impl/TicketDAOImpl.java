package com.iit.eventmanagment.dao.impl;

import java.sql.BatchUpdateException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import com.iit.eventmanagment.dao.TicketDAO;
import com.iit.eventmanagment.model.Event;
import com.iit.eventmanagment.model.Ticket;
import com.iit.eventmanagment.model.Users;
import com.iit.eventmanagment.model.db.DBConnector;
import com.iit.eventmanagment.model.db.QueryConstants;

/**
 * This class handles adding, viewing, updating and deleting of tickets.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class TicketDAOImpl implements TicketDAO {
	private DBConnector databaseConnector;

	/**
	 *  Adds tickets to the database. Returns the ticket with ticket ID
	 */
	@Override
	public Ticket addTicket(Ticket ticket, Users loggedInUser) throws Exception {
		this.databaseConnector = DBConnector.getDBConnector();
		try(Connection connection = databaseConnector.getConnection();
				CallableStatement callableStatement = connection.prepareCall(QueryConstants.ADD_TICKET)) {
			callableStatement.setString(1, ticket.getTicketName());
			callableStatement.setBigDecimal(2, ticket.getTicketPrice());
			callableStatement.setInt(3, ticket.getTicketQuantity());
			callableStatement.setDate(4, Date.valueOf(ticket.getTicketStartDate()));
			callableStatement.setTime(5, Time.valueOf(ticket.getTicketStartTime()));
			callableStatement.setDate(6, Date.valueOf(ticket.getTicketEndDate()));
			callableStatement.setTime(7, Time.valueOf(ticket.getTicketEndTime()));
			callableStatement.setString(8, ticket.getPerTicketRegistrationLimit());
			callableStatement.registerOutParameter(9, java.sql.Types.INTEGER);
			callableStatement.registerOutParameter(10, java.sql.Types.BOOLEAN);
			callableStatement.setInt(11,loggedInUser.getId());
			callableStatement.executeQuery();
			boolean result =  callableStatement.getBoolean(10);
			if(result)
				ticket.setId(callableStatement.getInt(9));
			else
				return null;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return ticket;
	}

	/**
	 *  Selects all the tickets
	 */
	@Override
	public List<Ticket> viewAllTicket() throws Exception {
		List<Ticket> ticketList;
		this.databaseConnector = DBConnector.getDBConnector();
		try(Connection connection = databaseConnector.getConnection();
				Statement statement = connection.createStatement()) {
			ResultSet rs = statement.executeQuery((QueryConstants.VIEW_ALL_TICKET));
			ticketList = populateTicket(rs);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return ticketList;
	}

	/**
	 *  Populates all ticket into the list.
	 */
	private List<Ticket> populateTicket(ResultSet rs) throws SQLException {
		List<Ticket> ticketList = new ArrayList<Ticket>();
		while (rs.next()) {
			Ticket ticket = new Ticket(rs.getInt(1), rs.getString(2),
					rs.getBigDecimal(3).toString(), String.valueOf(rs.getInt(4)), rs.getDate(5).toLocalDate(), rs.getTime(6).toLocalTime(), rs.getDate(7).toLocalDate(),
					rs.getTime(8).toLocalTime(), rs.getString(9));
			ticketList.add(ticket);
		}
		return ticketList;
	}

	/**
	 *  Updates ticket in the database.
	 */
	@Override
	public boolean updateTicket(Ticket ticket) throws Exception {
		this.databaseConnector = DBConnector.getDBConnector();
		boolean result = false;
		try(Connection connection = databaseConnector.getConnection();
				CallableStatement callableStatement = connection.prepareCall(QueryConstants.UPDATE_TICKET)) {
			callableStatement.setInt(1, ticket.getId());
			callableStatement.setString(2, ticket.getTicketName());
			callableStatement.setBigDecimal(3, ticket.getTicketPrice());
			callableStatement.setInt(4, ticket.getTicketQuantity());
			callableStatement.setDate(5, Date.valueOf(ticket.getTicketStartDate()));
			callableStatement.setTime(6, Time.valueOf(ticket.getTicketStartTime()));
			callableStatement.setDate(7, Date.valueOf(ticket.getTicketEndDate()));
			callableStatement.setTime(8, Time.valueOf(ticket.getTicketEndTime()));
			callableStatement.setString(9, ticket.getPerTicketRegistrationLimit());
			callableStatement.registerOutParameter(10, java.sql.Types.BOOLEAN);
			callableStatement.executeQuery();
			result =  callableStatement.getBoolean(10);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return result;
	}

	/**
	 *  Deletes the ticket. by ticket id
	 */
	@Override
	public boolean deleteTicket(Ticket ticket) throws Exception {
		this.databaseConnector = DBConnector.getDBConnector();
		boolean result = false;
		try(Connection connection = databaseConnector.getConnection();
				CallableStatement callableStatement = connection.prepareCall(QueryConstants.DELETE_TICKET)) {
			callableStatement.setInt(1, ticket.getId());
			callableStatement.registerOutParameter(2, java.sql.Types.BOOLEAN);
			callableStatement.executeQuery();
			result =  callableStatement.getBoolean(2);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return result;
	}


	public static void checkBatchUpdateCounts(int[] updateCounts) {
		for (int i = 0; i < updateCounts.length; i++) {
			if (updateCounts[i] >= 0) {
				// Successfully executed; the number represents number of affected rows
				System.out.println("OK: updateCount=" + updateCounts[i]);
			} else if (updateCounts[i] == Statement.SUCCESS_NO_INFO) {
				// Successfully executed; number of affected rows not available
				System.out.println("OK: updateCount=Statement.SUCCESS_NO_INFO");
			} else if (updateCounts[i] == Statement.EXECUTE_FAILED) {
				System.out.println("updateCount=Statement.EXECUTE_FAILED");
			}
		}
	}


	/**
	 *  Assign tickets to events
	 */
	@Override
	public boolean assignEventTicketsById(Users loggedInUser, Event event) throws Exception {
		boolean result = true;
		this.databaseConnector = DBConnector.getDBConnector();
		try (Connection connection = databaseConnector.getConnection()) {
			try (CallableStatement callableStatement = connection.prepareCall(QueryConstants.ASSIGN_EVENT_TICKETS)) {
				connection.setAutoCommit(false);
				event.getEventTickets().forEach(ticket -> {
					try {
						callableStatement.setInt(1, loggedInUser.getId());
						callableStatement.setInt(2, event.getId());
						callableStatement.setInt(3, ticket.getId());
						callableStatement.addBatch();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				});
				int[] updateCounts = callableStatement.executeBatch();
				checkBatchUpdateCounts(updateCounts);
				connection.commit();
			} catch (BatchUpdateException e) {
				try {
					e.printStackTrace();
					int[] updateCounts = e.getUpdateCounts();
					checkBatchUpdateCounts(updateCounts);
					connection.rollback();
					result = false;
					throw e;
				} catch (Exception e2) {
					result = false;
					e.printStackTrace();
					throw e;
				}
			}
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			throw e;
		}
		return result;
	}

	/**
	 *  Unassign tickets from the events
	 */
	@Override
	public boolean unAssignEventTicketsById(Users loggedInUser, Event selectedEvent, Ticket selectedTicket)
			throws Exception {
		this.databaseConnector = DBConnector.getDBConnector();
		boolean result = false;
		try(Connection connection = databaseConnector.getConnection();
				CallableStatement callableStatement = connection.prepareCall(QueryConstants.UNASSIGN_EVENT_TICKETS)) {
			callableStatement.setInt(1, loggedInUser.getId());
			callableStatement.setInt(2, selectedEvent.getId());
			callableStatement.setInt(3, selectedTicket.getId());
			callableStatement.registerOutParameter(4, java.sql.Types.BOOLEAN);
			callableStatement.executeQuery();
			result =  callableStatement.getBoolean(4);
			System.out.println("Resource was unassigned by userId:"+loggedInUser.getId());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return result;
	}


	/**
	 *  Selects all the tickets based on ticket ID
	 */
	@Override
	public List<Ticket> viewAllTicketsById(Users loggedInUser) throws Exception {
		List<Ticket> ticketList;
		this.databaseConnector = DBConnector.getDBConnector();
		try(Connection connection = databaseConnector.getConnection();
				CallableStatement callableStatement = connection.prepareCall(QueryConstants.VIEW_ALL_TICKETS_BY_ID)) {
			callableStatement.setInt(1, loggedInUser.getId());
			ResultSet rs = callableStatement.executeQuery();
			ticketList = populateTicket(rs);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return ticketList;
	}
}
