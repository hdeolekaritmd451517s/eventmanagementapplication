package com.iit.eventmanagment.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import com.iit.eventmanagment.dao.LoginDAO;
import com.iit.eventmanagment.model.Department;
import com.iit.eventmanagment.model.Login;
import com.iit.eventmanagment.model.Roles;
import com.iit.eventmanagment.model.Users;
import com.iit.eventmanagment.model.db.DBConnector;
import com.iit.eventmanagment.model.db.QueryConstants;

/**
 * This class handles the verification of the user.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class LoginDAOImpl implements LoginDAO {
	private DBConnector databaseConnector;

	/**
	 * Verifies user credentials, if valid, returns the user with all the details
	 */
	@Override
	public Users verifyUser(Login login) throws Exception {
		Users loggedInUser;
		this.databaseConnector = DBConnector.getDBConnector();
		try(Connection connection = databaseConnector.getConnection();
				CallableStatement callableStatement = connection.prepareCall(QueryConstants.VERIFY_USER)) {
					callableStatement.setString(1, login.getUsername());
					callableStatement.setString(2, login.getPassword());
					callableStatement.registerOutParameter(3, java.sql.Types.VARCHAR);
					callableStatement.registerOutParameter(4, java.sql.Types.VARCHAR);
					callableStatement.registerOutParameter(5, java.sql.Types.VARCHAR);
					callableStatement.registerOutParameter(6, java.sql.Types.VARCHAR);
					callableStatement.registerOutParameter(7, java.sql.Types.VARCHAR);
					callableStatement.registerOutParameter(8, java.sql.Types.SMALLINT);
					callableStatement.registerOutParameter(9, java.sql.Types.INTEGER);
					callableStatement.executeQuery();
					loggedInUser = populateUser(callableStatement);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return loggedInUser;
	}

	/**
	 * Populates user details
	 * @param callableStatement
	 * @return user object
	 * @throws SQLException
	 */
	private Users populateUser(CallableStatement callableStatement) throws SQLException {
		Users loggedInUser = new Users();
		loggedInUser.setFirstName(callableStatement.getString(3));
		loggedInUser.setLastName(callableStatement.getString(4));
		Department dept = new Department();
		dept.setDepartmentName(callableStatement.getString(5));
		loggedInUser.setDepartment(dept);
		loggedInUser.setEmail(callableStatement.getString(6));
		loggedInUser.setPhoneNumber(callableStatement.getString(7));
		Roles role = new Roles();
		role.setId(callableStatement.getInt(8));
		loggedInUser.setRole(role);
		loggedInUser.setId(callableStatement.getInt(9));
		return loggedInUser;
	}
}