package com.iit.eventmanagment.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.iit.eventmanagment.dao.LocationDAO;
import com.iit.eventmanagment.model.Department;
import com.iit.eventmanagment.model.Location;
import com.iit.eventmanagment.model.db.DBConnector;
import com.iit.eventmanagment.model.db.QueryConstants;

/**
 * This class handles adding, deleting, updating and viewing of locations for the events
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class LocationDAOImpl implements LocationDAO {
	private DBConnector databaseConnector;

	/**
	 * Adds event location in the database.
	 */
	@Override
	public Location addLocation(Location location) throws Exception {
		this.databaseConnector = DBConnector.getDBConnector();
		try(Connection connection = databaseConnector.getConnection();
				CallableStatement callableStatement = connection.prepareCall(QueryConstants.ADD_LOCATION)) {
					callableStatement.setString(1, location.getLocationName());
					callableStatement.setString(2, location.getLocationAddress());
					callableStatement.setString(3, location.getLocationCity());
					callableStatement.setString(4, location.getLocationState());
					callableStatement.setString(5, location.getLocationCountry());
					callableStatement.setString(6, location.getLocationEmail());
					callableStatement.setString(7, location.getLocationPhone());
					callableStatement.registerOutParameter(8, java.sql.Types.INTEGER);
					callableStatement.registerOutParameter(9, java.sql.Types.BOOLEAN);
					callableStatement.executeQuery();
					boolean result =  callableStatement.getBoolean(9);
					if(result)
						location.setId(callableStatement.getInt(8));
					else
						return null;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return location;
	}

	/**
	 * Updates event location in the database.
	 */
	@Override
	public boolean updateLocation(Location location) throws Exception {
		this.databaseConnector = DBConnector.getDBConnector();
		boolean result = false;
		try(Connection connection = databaseConnector.getConnection();
				CallableStatement callableStatement = connection.prepareCall(QueryConstants.UPDATE_LOCATION)) {
					callableStatement.setInt(1, location.getId());
					callableStatement.setString(2, location.getLocationName());
					callableStatement.setString(3, location.getLocationAddress());
					callableStatement.setString(4, location.getLocationCity());
					callableStatement.setString(5, location.getLocationState());
					callableStatement.setString(6, location.getLocationCountry());
					callableStatement.setString(7, location.getLocationEmail());
					callableStatement.setString(8, location.getLocationPhone());
					callableStatement.registerOutParameter(9, java.sql.Types.BOOLEAN);
					callableStatement.executeQuery();
					result =  callableStatement.getBoolean(9);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return result;
	}

	/**
	 * Deletes event location.
	 */
	@Override
	public boolean deleteLocation(Location location) throws Exception {
		this.databaseConnector = DBConnector.getDBConnector();
		boolean result = false;
		try(Connection connection = databaseConnector.getConnection();
				CallableStatement callableStatement = connection.prepareCall(QueryConstants.DELETE_LOCATION)) {
			callableStatement.setInt(1, location.getId());
			callableStatement.registerOutParameter(2, java.sql.Types.BOOLEAN);
			callableStatement.executeQuery();
			result =  callableStatement.getBoolean(2);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return result;
	}

	/**
	 * Selects all the event location from the database.
	 */
	@Override
	public List<Location> viewAllLocation() throws Exception {
		List<Location> locationList;
		this.databaseConnector = DBConnector.getDBConnector();
		try(Connection connection = databaseConnector.getConnection();
				Statement statement = connection.createStatement()) {
			ResultSet rs = statement.executeQuery((QueryConstants.VIEW_ALL_LOCATION));
			locationList = populateLocation(rs);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return locationList;
	}

	/**
	 * Creates locations objects selected from the database.
	 */
	private List<Location> populateLocation(ResultSet rs) throws SQLException {
		List<Location> locationList = new ArrayList<Location>();
		while (rs.next()) {
			Location location = new Location(rs.getInt(1), rs.getString(2),
					rs.getString(3),  rs.getString(4), rs.getString(5), rs.getString(6),
					rs.getString(7), rs.getString(8));
			locationList.add(location);
		}
		return locationList;
	}
}
