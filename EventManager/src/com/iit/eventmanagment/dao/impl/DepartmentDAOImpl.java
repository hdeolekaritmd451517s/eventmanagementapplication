package com.iit.eventmanagment.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.iit.eventmanagment.dao.DepartmentDAO;
import com.iit.eventmanagment.model.Department;
import com.iit.eventmanagment.model.db.DBConnector;
import com.iit.eventmanagment.model.db.QueryConstants;

/**
 * This class handles adding, viewing, updating and deleting of the department in the database.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class DepartmentDAOImpl implements DepartmentDAO{
	private DBConnector databaseConnector;

	/**
	 * Adds  department to the database.
	 */
	@Override
	public Department addDepartment(Department department) throws Exception {
		boolean result = false;
		this.databaseConnector = DBConnector.getDBConnector();
		try(Connection connection = databaseConnector.getConnection();
				CallableStatement callableStatement = connection.prepareCall(QueryConstants.ADD_DEPARTMENT)) {
			callableStatement.setString(1, department.getDepartmentName());
			callableStatement.registerOutParameter(2,  java.sql.Types.SMALLINT);
			callableStatement.registerOutParameter(3, java.sql.Types.BOOLEAN);
			callableStatement.executeQuery();
			result = callableStatement.getBoolean(3);
			if(result)
				department.setId(callableStatement.getInt(2));
			else
				return null;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return department;
	}


	/**
	 * This method updates the department
	 */
	@Override
	public boolean updateDepartment(Department department) throws Exception {
		this.databaseConnector = DBConnector.getDBConnector();
		boolean result = false;
		try(Connection connection = databaseConnector.getConnection();
				CallableStatement callableStatement = connection.prepareCall(QueryConstants.UPDATE_DEPARTMENT)) {
					callableStatement.setInt(1, department.getId());
					callableStatement.setString(2, department.getDepartmentName());
					callableStatement.registerOutParameter(3, java.sql.Types.BOOLEAN);
					callableStatement.executeQuery();
					result =  callableStatement.getBoolean(3);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return result;
	}
	/**
	 * Deletes department by department ID.
	 */
	@Override
	public boolean deleteDepartment(Department department) throws Exception {
		this.databaseConnector = DBConnector.getDBConnector();
		boolean result = false;
		try(Connection connection = databaseConnector.getConnection();
				CallableStatement callableStatement = connection.prepareCall(QueryConstants.DELETE_DEPARTMENT)) {
			callableStatement.setInt(1, department.getId());
			callableStatement.registerOutParameter(2, java.sql.Types.BOOLEAN);
			callableStatement.executeQuery();
			result =  callableStatement.getBoolean(2);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return result;
	}

	/**
	 * select all departments from the database.
	 */
	@Override
	public List<Department> viewAllDepartment() throws Exception {
		List<Department> deptList;
		this.databaseConnector = DBConnector.getDBConnector();
		try(Connection connection = databaseConnector.getConnection();
				Statement statement = connection.createStatement()) {
			ResultSet rs = statement.executeQuery((QueryConstants.VIEW_ALL_DEPARTMENT));
			deptList = populateDepartment(rs);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return deptList;
	}

	/**
	 * creates  Department object.
	 */
	private List<Department> populateDepartment(ResultSet rs) throws Exception{
		List<Department> deptList = new ArrayList<Department>();
		while (rs.next()) {
			Department dept = new Department();
			dept.setId(rs.getInt(1));
			dept.setDepartmentName(rs.getString(2));
			deptList.add(dept);
		}
		return deptList;
	}
}
