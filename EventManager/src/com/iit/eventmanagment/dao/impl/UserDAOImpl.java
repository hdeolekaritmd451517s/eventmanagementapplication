package com.iit.eventmanagment.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.iit.eventmanagment.dao.UserDAO;
import com.iit.eventmanagment.model.Department;
import com.iit.eventmanagment.model.Location;
import com.iit.eventmanagment.model.Roles;
import com.iit.eventmanagment.model.Users;
import com.iit.eventmanagment.model.db.DBConnector;
import com.iit.eventmanagment.model.db.QueryConstants;

/**
 * This class handles adding, viewing, updating and deleting of users.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class UserDAOImpl implements UserDAO{
	private DBConnector databaseConnector;

	/**
	 *  Adds user to the database. Returns the user with user ID
	 */
	@Override
	public boolean addUser(Users user) throws Exception {
		boolean result = false;
		this.databaseConnector = DBConnector.getDBConnector();
		try(Connection connection = databaseConnector.getConnection();
				CallableStatement callableStatement = connection.prepareCall(QueryConstants.ADD_USERS)) {
			callableStatement.setString(1, user.getFirstName());
			callableStatement.setString(2, user.getLastName());
			callableStatement.setString(3, user.getDepartment().getDepartmentName());
			callableStatement.setString(4, user.getEmail());
			callableStatement.setString(5, user.getPhoneNumber());
			callableStatement.setString(6, user.getPassword());
			callableStatement.setString(7, user.getRole().getRoleName());
			callableStatement.registerOutParameter(8, java.sql.Types.BOOLEAN);
			callableStatement.executeQuery();
			result = callableStatement.getBoolean(8);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return result;
	}

	/**
	 *  Updates the user to the database.
	 */
	@Override
	public boolean updateUser(Users user) throws Exception {
		boolean result = false;
		this.databaseConnector = DBConnector.getDBConnector();
		try(Connection connection = databaseConnector.getConnection();
				CallableStatement callableStatement = connection.prepareCall(QueryConstants.UPDATE_USER)) {
			callableStatement.setInt(1, user.getId());
			callableStatement.setString(2, user.getFirstName());
			callableStatement.setString(3, user.getLastName());
			callableStatement.setString(4, user.getDepartment().getDepartmentName());
			callableStatement.setString(5, user.getEmail());
			callableStatement.setString(6, user.getPhoneNumber());
			callableStatement.registerOutParameter(7, java.sql.Types.BOOLEAN);
			callableStatement.executeQuery();
			result = callableStatement.getBoolean(7);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return result;
	}

	@Override
	public boolean deleteUser(Users user) {
		// TODO Auto-generated method stub
		return false;
	}


	/**
	 *  Selects all the users into list
	 */
	@Override
	public List<Users> viewAllUser() throws Exception {
		List<Users> userList;
		this.databaseConnector = DBConnector.getDBConnector();
		try(Connection connection = databaseConnector.getConnection();
				Statement statement = connection.createStatement()) {
			ResultSet rs = statement.executeQuery((QueryConstants.VIEW_ALL_USERS));
			userList = populateUser(rs);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return userList;
	}

	/**
	 *  Populates all the user with user details
	 */
	private List<Users> populateUser(ResultSet rs) throws SQLException {
		List<Users> userList = new ArrayList<Users>();
		while (rs.next()) {
			Department dept = new Department();
			dept.setDepartmentName( rs.getString(4));
			Roles role = new Roles();
			role.setRoleName(rs.getString(7));
			Users user = new Users(rs.getInt(1), rs.getString(2),
					rs.getString(3), dept, rs.getString(5), rs.getString(6), role);
			System.out.println("view all users -->"+user);
			userList.add(user);
		}
		return userList;
	}
}