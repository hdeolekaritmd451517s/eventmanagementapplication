package com.iit.eventmanagment.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.iit.eventmanagment.dao.RolesDAO;
import com.iit.eventmanagment.model.Roles;
import com.iit.eventmanagment.model.db.DBConnector;
import com.iit.eventmanagment.model.db.QueryConstants;

/**
 * This class allows adding, updating, viewing and deleting of roles in the database.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class RolesDAOImpl implements RolesDAO{
	private DBConnector databaseConnector;

	/**
	 * Adds the role into the database
	 */
	@Override
	public Roles addRole(Roles role) throws Exception {
		this.databaseConnector = DBConnector.getDBConnector();
		try(Connection connection = databaseConnector.getConnection();
				CallableStatement callableStatement = connection.prepareCall(QueryConstants.ADD_ROLE)) {
					callableStatement.setString(1, role.getRoleName());
					callableStatement.registerOutParameter(2, java.sql.Types.SMALLINT);
					callableStatement.registerOutParameter(3, java.sql.Types.BOOLEAN);
					callableStatement.executeQuery();
					boolean result =  callableStatement.getBoolean(3);
					if(result)
						role.setId(callableStatement.getInt(2));
					else
						return null;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return role;
	}

	/**
	 * Updates the role in the database
	 */
	@Override
	public boolean updateRole(Roles role) throws Exception {
		this.databaseConnector = DBConnector.getDBConnector();
		boolean result = false;
		try(Connection connection = databaseConnector.getConnection();
				CallableStatement callableStatement = connection.prepareCall(QueryConstants.UPDATE_ROLE)) {
					callableStatement.setInt(1, role.getId());
					callableStatement.setString(2, role.getRoleName());
					callableStatement.registerOutParameter(3, java.sql.Types.BOOLEAN);
					callableStatement.executeQuery();
					result =  callableStatement.getBoolean(3);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return result;
	}

	/**
	 * Deletes the role from the database
	 */
	@Override
	public boolean deleteRole(Roles role) throws Exception {
		this.databaseConnector = DBConnector.getDBConnector();
		boolean result = false;
		try(Connection connection = databaseConnector.getConnection();
				CallableStatement callableStatement = connection.prepareCall(QueryConstants.DELETE_ROLE)) {
			callableStatement.setInt(1, role.getId());
			callableStatement.registerOutParameter(2, java.sql.Types.BOOLEAN);
			callableStatement.executeQuery();
			result =  callableStatement.getBoolean(2);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return result;
	}

	/**
	 * Selects all the roles from the database.
	 */
	@Override
	public List<Roles> viewAllRoles() throws Exception {
		List<Roles> rolesList;
		this.databaseConnector = DBConnector.getDBConnector();
		try(Connection connection = databaseConnector.getConnection();
				Statement statement = connection.createStatement()) {
			ResultSet rs = statement.executeQuery((QueryConstants.VIEW_ALL_ROLES));
			rolesList = populateRoles(rs);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return rolesList;
	}

	/**
	 * Populates all the roles into a list.
	 */
	private List<Roles> populateRoles(ResultSet rs) throws Exception{
		List<Roles> rolesList = new ArrayList<Roles>();
		while (rs.next()) {
			Roles role = new Roles();
			role.setId(rs.getInt(1));
			role.setRoleName(rs.getString(2));
			//role.setRolePriority(rs.getString(3));
			rolesList.add(role);
		}
		return rolesList;
	}
}
