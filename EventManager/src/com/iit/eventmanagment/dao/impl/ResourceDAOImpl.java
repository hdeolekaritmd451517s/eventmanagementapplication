package com.iit.eventmanagment.dao.impl;

import java.sql.BatchUpdateException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.iit.eventmanagment.dao.ResourceDAO;
import com.iit.eventmanagment.model.Department;
import com.iit.eventmanagment.model.Event;
import com.iit.eventmanagment.model.Roles;
import com.iit.eventmanagment.model.Users;
import com.iit.eventmanagment.model.db.DBConnector;
import com.iit.eventmanagment.model.db.QueryConstants;

/**
 * This class handles adding, viewing, updating and deleting of resources.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class ResourceDAOImpl implements ResourceDAO {
	private DBConnector databaseConnector;


	/**
	 *  Adds user as resource to the database. Returns the user with user ID
	 */
	@Override
	public Users addResource(Users user, Users loggedInUser) throws Exception {
		this.databaseConnector = DBConnector.getDBConnector();
		try(Connection connection = databaseConnector.getConnection();
				CallableStatement callableStatement = connection.prepareCall(QueryConstants.ADD_NEW_RESOURCE)) {
			callableStatement.setString(1, user.getFirstName());
			callableStatement.setString(2, user.getLastName());
			callableStatement.setString(3, user.getDepartment().getDepartmentName());
			callableStatement.setString(4, user.getEmail());
			callableStatement.setString(5, user.getPhoneNumber());
			callableStatement.setString(6, user.getRole().getRoleName());
			callableStatement.setInt(7, loggedInUser.getId());
			callableStatement.registerOutParameter(8, java.sql.Types.INTEGER);
			callableStatement.registerOutParameter(9, java.sql.Types.BOOLEAN);
			callableStatement.executeQuery();
			boolean result = callableStatement.getBoolean(9);
			if(result)
				user.setId(callableStatement.getInt(8));
			else
				return null;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return user;
	}

	/**
	 *  Updates user as resource to the database.
	 */
	@Override
	public boolean updateAndAddResource(Users user, Users loggedInUser) throws Exception {
		boolean result = false;
		this.databaseConnector = DBConnector.getDBConnector();
		try(Connection connection = databaseConnector.getConnection();
				CallableStatement callableStatement = connection.prepareCall(QueryConstants.ADD_USER_TO_RESOURCE)) {
			callableStatement.setInt(1, user.getId());
			callableStatement.setString(2, user.getFirstName());
			callableStatement.setString(3, user.getLastName());
			callableStatement.setString(4, user.getEmail());
			callableStatement.setString(5, user.getPhoneNumber());
			callableStatement.setString(6, user.getRole().getRoleName());
			callableStatement.setInt(7, loggedInUser.getId());
			callableStatement.registerOutParameter(8, java.sql.Types.BOOLEAN);
			callableStatement.executeQuery();
			result = callableStatement.getBoolean(8);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return result;
	}

	/**
	 *  Selects all the resource based on user ID
	 */
	@Override
	public List<Users> viewAllResourcesById(Users loggedInUser) throws Exception {
		List<Users> userList;
		this.databaseConnector = DBConnector.getDBConnector();
		try(Connection connection = databaseConnector.getConnection();
				CallableStatement callableStatement = connection.prepareCall(QueryConstants.VIEW_ALL_RESOUCES_BY_ID)) {
			callableStatement.setInt(1, loggedInUser.getId());
			ResultSet rs = callableStatement.executeQuery();
			userList = populateUser(rs);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return userList;
	}

	/**
	 *  Populates all the resources into the list
	 */
	private List<Users> populateUser(ResultSet rs) throws SQLException {
		List<Users> userList = new ArrayList<Users>();
		while (rs.next()) {
			Department dept = new Department();
			dept.setDepartmentName( rs.getString(4));
			Roles role = new Roles();
			role.setRoleName(rs.getString(7));
			Users user = new Users(rs.getInt(1), rs.getString(2),
					rs.getString(3), dept, rs.getString(5), rs.getString(6), role);
			userList.add(user);
		}
		return userList;
	}

	/**
	 *  Delete resource by userID
	 */
	@Override
	public boolean deleteResourceById(Users currentResource) throws Exception {
		this.databaseConnector = DBConnector.getDBConnector();
		boolean result = false;
		try(Connection connection = databaseConnector.getConnection();
				CallableStatement callableStatement = connection.prepareCall(QueryConstants.DELETE_RESOURCE_BY_ID)) {
			callableStatement.setInt(1, currentResource.getId());
			callableStatement.registerOutParameter(2, java.sql.Types.BOOLEAN);
			callableStatement.executeQuery();
			result =  callableStatement.getBoolean(2);
			System.out.println("Resource ID["+currentResource.getId()+"]  was deleted!");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return result;
	}


	/**
	 *  Assigns resources to the event.
	 */
	@Override
	public boolean assignEventResourcesById(Users loggedInUser, Event event) throws Exception {
		boolean result = true;
		this.databaseConnector = DBConnector.getDBConnector();
		try (Connection connection = databaseConnector.getConnection()) {
			try (CallableStatement callableStatement = connection.prepareCall(QueryConstants.ASSIGN_EVENT_RESOURCES)) {
				connection.setAutoCommit(false);
				event.getEventResources().forEach(user -> {
					try {
						callableStatement.setInt(1, loggedInUser.getId());
						callableStatement.setInt(2, event.getId());
						callableStatement.setInt(3, user.getId());
						callableStatement.addBatch();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				});
				int[] updateCounts = callableStatement.executeBatch();
				checkBatchUpdateCounts(updateCounts);
				connection.commit();
			} catch (BatchUpdateException e) {
				try {
					e.printStackTrace();
					int[] updateCounts = e.getUpdateCounts();
					checkBatchUpdateCounts(updateCounts);
					connection.rollback();
					result = false;
					throw e;
				} catch (Exception e2) {
					result = false;
					e.printStackTrace();
					throw e;
				}
			}
		}catch (Exception e) {
			result = false;
			e.printStackTrace();
			throw e;
		}
		return result;
	}

	  public static void checkBatchUpdateCounts(int[] updateCounts) {
		    for (int i = 0; i < updateCounts.length; i++) {
		      if (updateCounts[i] >= 0) {
		        // Successfully executed; the number represents number of affected rows
		        System.out.println("OK: updateCount=" + updateCounts[i]);
		      } else if (updateCounts[i] == Statement.SUCCESS_NO_INFO) {
		        // Successfully executed; number of affected rows not available
		        System.out.println("OK: updateCount=Statement.SUCCESS_NO_INFO");
		      } else if (updateCounts[i] == Statement.EXECUTE_FAILED) {
		        System.out.println("updateCount=Statement.EXECUTE_FAILED");
		      }
		    }
		  }

		/**
		 *  Unassigns resources from the event.
		 */
	@Override
	public boolean unAssignEventResourcesById(Users loggedInUser, Event selectedEvent, Users selectedUser)
			throws Exception {
		this.databaseConnector = DBConnector.getDBConnector();
		boolean result = false;
		try(Connection connection = databaseConnector.getConnection();
				CallableStatement callableStatement = connection.prepareCall(QueryConstants.UNASSIGN_EVENT_RESOURCES)) {
			callableStatement.setInt(1, loggedInUser.getId());
			callableStatement.setInt(2, selectedEvent.getId());
			callableStatement.setInt(3, selectedUser.getId());
			callableStatement.registerOutParameter(4, java.sql.Types.BOOLEAN);
			callableStatement.executeQuery();
			result =  callableStatement.getBoolean(4);
			System.out.println("Resource was unassigned by userId:"+loggedInUser.getId());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return result;
	}
}
