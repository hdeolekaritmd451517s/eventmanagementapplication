package com.iit.eventmanagment.dao.impl;

import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import com.iit.eventmanagment.dao.EventDAO;
import com.iit.eventmanagment.model.Event;
import com.iit.eventmanagment.model.Users;
import com.iit.eventmanagment.model.db.DBConnector;
import com.iit.eventmanagment.model.db.QueryConstants;

/**
 * This class handles adding, deleting, updating and deleting of events.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class EventDAOImpl implements EventDAO {
	private DBConnector databaseConnector;

	/**
	 * Adds event to the database.
	 */
	@Override
	public Event addEvent(Event event, Users loggedInUser) throws Exception {
		boolean result = false;
		this.databaseConnector = DBConnector.getDBConnector();
		try(Connection connection = databaseConnector.getConnection();
				CallableStatement callableStatement = connection.prepareCall(QueryConstants.ADD_EVENT)) {
			callableStatement.setString(1, event.getName());
			callableStatement.setString(2, event.getDescription());
			callableStatement.setString(3, event.getEventAddress());
			callableStatement.setString(4, event.getEventCity());
			callableStatement.setString(5, event.getEventState());
			callableStatement.setString(6, event.getEventCountry());
			callableStatement.setInt(7, Integer.parseInt(event.getEventPostalCode()));
			callableStatement.setString(8, event.getEventEmail());
			callableStatement.setString(9, event.getEventPhone());
			callableStatement.setDate(10, Date.valueOf(event.getEventStartDate()));
			callableStatement.setTime(11, Time.valueOf(event.getEventStartTime()));
			callableStatement.setDate(12, Date.valueOf(event.getEventEndDate()));
			callableStatement.setTime(13, Time.valueOf(event.getEventEndTime()));
			 Blob b1 = connection.createBlob();
			 b1.setBytes(1, event.getEventImage());
			callableStatement.setBlob(14, b1);
			callableStatement.setInt(15, event.getNoOfSeats());
			callableStatement.setInt(16, loggedInUser.getId());
			callableStatement.registerOutParameter(17, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(18, java.sql.Types.INTEGER);
			callableStatement.registerOutParameter(19, java.sql.Types.BOOLEAN);
			callableStatement.executeQuery();
			result = callableStatement.getBoolean(19);
			if (result) {
				event.setId(callableStatement.getInt(18));
			}else{
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return event;
	}

	/**
	 * Updates event in the database.
	 */
	@Override
	public boolean updateEvent(Event event) throws Exception {
		boolean result = false;
		this.databaseConnector = DBConnector.getDBConnector();
		try(Connection connection = databaseConnector.getConnection();
				CallableStatement callableStatement = connection.prepareCall(QueryConstants.UPDATE_EVENT)) {
			callableStatement.setInt(1, event.getId());
			callableStatement.setString(2, event.getName());
			callableStatement.setString(3, event.getDescription());
			callableStatement.setString(4, event.getEventAddress());
			callableStatement.setString(5, event.getEventCity());
			callableStatement.setString(6, event.getEventState());
			callableStatement.setString(7, event.getEventCountry());
			callableStatement.setString(8, event.getEventPostalCode());
			callableStatement.setString(9, event.getEventEmail());
			callableStatement.setString(10, event.getEventPhone());
			callableStatement.setDate(11, Date.valueOf(event.getEventStartDate()));
			callableStatement.setTime(12, Time.valueOf(event.getEventStartTime()));
			callableStatement.setDate(13, Date.valueOf(event.getEventEndDate()));
			callableStatement.setTime(14, Time.valueOf(event.getEventEndTime()));
			Blob b1 = connection.createBlob();
			 b1.setBytes(1, event.getEventImage());
			callableStatement.setBlob(15, b1);
			callableStatement.setInt(16, event.getNoOfSeats());
			callableStatement.registerOutParameter(17, java.sql.Types.BOOLEAN);
			callableStatement.executeQuery();
			result = callableStatement.getBoolean(17);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return result;
	}

	/**
	 * Deletes event from the database.
	 */
	@Override
	public boolean deleteEvent(Event event) throws Exception {
		this.databaseConnector = DBConnector.getDBConnector();
		boolean result = false;
		try(Connection connection = databaseConnector.getConnection();
				CallableStatement callableStatement = connection.prepareCall(QueryConstants.DELETE_EVENT)) {
			callableStatement.setInt(1, event.getId());
			callableStatement.registerOutParameter(2, java.sql.Types.BOOLEAN);
			callableStatement.executeQuery();
			result =  callableStatement.getBoolean(2);
			System.out.println("event deleted"+result);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return result;
	}

	/**
	 * Selects all event from the database.
	 */
	@Override
	public List<Event> viewAllEvent() throws Exception {
		List<Event> eventList;
		this.databaseConnector = DBConnector.getDBConnector();
		try(Connection connection = databaseConnector.getConnection();
				Statement statement = connection.createStatement()) {
			ResultSet rs = statement.executeQuery((QueryConstants.VIEW_ALL_EVENTS));
			eventList = populateEvent(rs);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return eventList;
	}
	/**
	 * Creates list of  all the selected event
	 */
	private List<Event> populateEvent(ResultSet rs) throws SQLException {
		List<Event> eventList = new ArrayList<Event>();
		while (rs.next()) {
			Event event = new Event(rs.getInt(1), rs.getString(2),
					rs.getString(3), rs.getString(4), rs.getString(5),  rs.getString(6),  rs.getString(7),  rs.getString(8),  rs.getString(9),
					 rs.getString(10), rs.getDate(11).toLocalDate(), rs.getTime(12).toLocalTime(), rs.getDate(13).toLocalDate(),
					rs.getTime(14).toLocalTime(), rs.getInt(15), rs.getBlob(16).toString().getBytes(), rs.getString(17));
			eventList.add(event);
		}
		return eventList;
	}

	/**
	 * Selects all event from the database by userId.
	 */
	@Override
	public List<Event> viewAllEventByUserId(Users loggedInUser) throws Exception {
		List<Event> eventList;
		this.databaseConnector = DBConnector.getDBConnector();
		try(Connection connection = databaseConnector.getConnection();
				CallableStatement callableStatement = connection.prepareCall(QueryConstants.VIEW_ALL_EVENTS_BY_ID)) {
			callableStatement.setInt(1, loggedInUser.getId());
			ResultSet rs = callableStatement.executeQuery();
			eventList = populateEvent(rs);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return eventList;
	}

}
