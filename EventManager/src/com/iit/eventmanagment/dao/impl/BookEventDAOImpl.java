package com.iit.eventmanagment.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.iit.eventmanagment.dao.BookEventDAO;
import com.iit.eventmanagment.model.BookEvent;
import com.iit.eventmanagment.model.Users;
import com.iit.eventmanagment.model.db.DBConnector;
import com.iit.eventmanagment.model.db.QueryConstants;

/**
 * This class connects to database and performs add, update delete, select operations for booking an event
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public class BookEventDAOImpl implements BookEventDAO {
	private DBConnector databaseConnector;


	/**
	 * Adds event booking to the database.
	 */
	@Override
	public BookEvent addBooking(Users loggedInUser, BookEvent bookedEvent) throws Exception {
		this.databaseConnector = DBConnector.getDBConnector();
		try(Connection connection = databaseConnector.getConnection();
				CallableStatement callableStatement = connection.prepareCall(QueryConstants.ADD_NEW_BOOKING)) {
			callableStatement.setInt(1, loggedInUser.getId());
			callableStatement.setInt(2, bookedEvent.getEventId());
			callableStatement.setBoolean(3, bookedEvent.isReminder());
			callableStatement.registerOutParameter(4, java.sql.Types.TIMESTAMP);
			callableStatement.registerOutParameter(5, java.sql.Types.BOOLEAN);
			callableStatement.executeQuery();
			boolean result = callableStatement.getBoolean(5);
			if(result)
				bookedEvent.setBookingDateTime(callableStatement.getTimestamp(4).toLocalDateTime());
			else
				return null;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return bookedEvent;
	}

	/**
	 * selects all event booking by user id
	 */
	@Override
	public List<BookEvent> viewAllBookingById(Users loggedInUser) throws Exception {
		List<BookEvent> bookedEventList;
		this.databaseConnector = DBConnector.getDBConnector();
		try(Connection connection = databaseConnector.getConnection();
				CallableStatement callableStatement = connection.prepareCall(QueryConstants.VIEW_ALL_BOOKING_BY_ID)) {
			callableStatement.setInt(1, loggedInUser.getId());
			ResultSet rs = callableStatement.executeQuery();
			bookedEventList = populateBookedEvent(rs, false);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return bookedEventList;
	}



	/**
	 * creates  BookEvent object.
	 */
	private List<BookEvent> populateBookedEvent(ResultSet rs, boolean viewAll) throws SQLException {
		List<BookEvent> bookedEventList = new ArrayList<BookEvent>();
		while (rs.next()) {
			BookEvent bookedEvent = null;
			if(!viewAll){
				bookedEvent = new BookEvent();
				bookedEvent.setId(rs.getInt(1));
				bookedEvent.setEventName(rs.getString(2));
				bookedEvent.setBookingDateTime(rs.getTimestamp(3).toLocalDateTime());
			}else{
				bookedEvent = new BookEvent();
				bookedEvent.setId(rs.getInt(1));
				bookedEvent.setUserFirstName(rs.getString(2));
				bookedEvent.setUserLastName(rs.getString(3));
				bookedEvent.setUserEmail(rs.getString(4));
				bookedEvent.setEventName(rs.getString(5));
				bookedEvent.setBookingDateTime(rs.getTimestamp(6).toLocalDateTime());
			}
			bookedEventList.add(bookedEvent);
		}
		return bookedEventList;
	}


	/**
	 * select all booking from the database.
	 */
	@Override
	public List<BookEvent> viewAllBooking() throws Exception {
		List<BookEvent> bookedEventList;
		this.databaseConnector = DBConnector.getDBConnector();
		try(Connection connection = databaseConnector.getConnection();
				Statement statement = connection.createStatement()) {
			ResultSet rs = statement.executeQuery((QueryConstants.VIEW_ALL_BOOKING));
			bookedEventList = populateBookedEvent(rs, true);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return bookedEventList;
	}

	/**
	 * Deletes booking by booking ID.
	 */
	@Override
	public boolean deleteBookingById(BookEvent bookedEvent) throws Exception {
		this.databaseConnector = DBConnector.getDBConnector();
		boolean result = false;
		try(Connection connection = databaseConnector.getConnection();
				CallableStatement callableStatement = connection.prepareCall(QueryConstants.CANCEL_BOOKING_BY_ID)) {
			callableStatement.setInt(1, bookedEvent.getId());
			callableStatement.registerOutParameter(2, java.sql.Types.BOOLEAN);
			callableStatement.executeQuery();
			result =  callableStatement.getBoolean(2);
			System.out.println("Booking ID "+bookedEvent.getId()+" was canceled!");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return result;
	}

}
