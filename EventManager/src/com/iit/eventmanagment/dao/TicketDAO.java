package com.iit.eventmanagment.dao;

import java.util.List;
import com.iit.eventmanagment.model.Event;
import com.iit.eventmanagment.model.Ticket;
import com.iit.eventmanagment.model.Users;
/**
 * This is interface for tickets. It allows to add, assign, unassign, view, update and delete tickets.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public interface TicketDAO {
	public abstract Ticket addTicket(Ticket ticket, Users loggedInUser) throws Exception;
	public abstract List<Ticket> viewAllTicket() throws Exception;
	public abstract List<Ticket> viewAllTicketsById(Users loggedInUser) throws Exception;
	public abstract boolean updateTicket(Ticket ticket) throws Exception;
	public abstract boolean deleteTicket(Ticket ticket) throws Exception;
	public abstract boolean assignEventTicketsById(Users loggedInUser, Event event) throws Exception;
	public abstract boolean unAssignEventTicketsById(Users loggedInUser, Event selectedEvent, Ticket selectedTicket) throws Exception;

}
