package com.iit.eventmanagment.dao;

import java.util.List;

import com.iit.eventmanagment.model.BookEvent;
import com.iit.eventmanagment.model.Users;

/**
 * This is interface for booking event. It allows to add bookings, view bookings and delete bookings.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public interface BookEventDAO {
	public abstract BookEvent addBooking(Users loggedInUser, BookEvent bookedEvent) throws Exception;
	public abstract List<BookEvent> viewAllBookingById(Users loggedInUser) throws Exception;
	public abstract List<BookEvent> viewAllBooking() throws Exception;
	public abstract boolean deleteBookingById(BookEvent bookedEvent) throws Exception;
}
