package com.iit.eventmanagment.dao;

import java.util.List;
import com.iit.eventmanagment.model.Department;

/**
 * This is interface for department. It allows to add department, view departments, update department and delete department.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public interface DepartmentDAO {
	public abstract Department addDepartment(Department department) throws Exception;
	public abstract boolean updateDepartment(Department department) throws Exception;
	public abstract boolean deleteDepartment(Department department) throws Exception;
	public abstract List<Department> viewAllDepartment() throws Exception;
}