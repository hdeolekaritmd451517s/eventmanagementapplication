package com.iit.eventmanagment.dao;

import java.util.List;

import com.iit.eventmanagment.model.Department;
import com.iit.eventmanagment.model.Roles;
/**
 * This is interface for roles. It allows to add, view, update and delete roles.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public interface RolesDAO {
	public abstract Roles addRole(Roles role) throws Exception;
	public abstract boolean updateRole(Roles role) throws Exception;
	public abstract boolean deleteRole(Roles role) throws Exception;
	public abstract List<Roles> viewAllRoles() throws Exception;
}
