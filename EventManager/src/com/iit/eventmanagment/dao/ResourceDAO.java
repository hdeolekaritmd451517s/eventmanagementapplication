package com.iit.eventmanagment.dao;

import java.util.List;

import com.iit.eventmanagment.model.Event;
import com.iit.eventmanagment.model.Users;
import javafx.collections.ObservableList;
/**
 * This is interface for resources. It allows to add, assign, unassign resources, view resources, update and delete resources.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public interface ResourceDAO {
	public abstract Users addResource(Users user, Users loggedInUser) throws Exception;
	public abstract boolean updateAndAddResource(Users user, Users loggedInUser) throws Exception;
	public abstract List<Users> viewAllResourcesById(Users loggedInUser) throws Exception;
	public abstract boolean deleteResourceById(Users currentResource) throws Exception;
	public abstract boolean assignEventResourcesById(Users loggedInUser, Event event) throws Exception;
	public abstract boolean unAssignEventResourcesById(Users loggedInUser, Event selectedEvent, Users selectedUser) throws Exception;
}
