package com.iit.eventmanagment.dao;

import java.util.List;

import com.iit.eventmanagment.model.Users;
/**
 * This is interface for users. It allows to add, view, update and delete users.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public interface UserDAO {
	public abstract boolean addUser(Users user) throws Exception;
	public abstract boolean updateUser(Users user) throws Exception;
	public abstract boolean deleteUser(Users user) throws Exception;
	public abstract List<Users> viewAllUser() throws Exception;
}
