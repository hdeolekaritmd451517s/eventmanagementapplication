package com.iit.eventmanagment.dao;

import java.util.List;

import com.iit.eventmanagment.model.Event;
import com.iit.eventmanagment.model.Users;

/**
 * This is interface for events. It allows to add events, view events, update and delete events.
 * * @author Harshal Deolekar & Sri Chivukula
 * @version 1.0
 * @since December 2, 2016
 */
public interface EventDAO {
	public abstract Event addEvent(Event event, Users loggedInUser) throws Exception;
	public abstract List<Event> viewAllEvent() throws Exception;
	public abstract List<Event> viewAllEventByUserId(Users loggedInUser) throws Exception;
	public abstract boolean updateEvent(Event event) throws Exception;
	public abstract boolean deleteEvent(Event event) throws Exception;
}
